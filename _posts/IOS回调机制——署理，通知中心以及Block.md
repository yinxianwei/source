title: IOS回调机制——署理，通知中心以及Block
date: 2014-02-20 15:54:00
categories:
- iOS应用开发之常用方法
tags:
- block
---
##Xcode5.0正式版
IOS7和Xcode5正式版在昨天正式可以下载。IOS7不多说了，交互设计，界面风格，操作的简化程度都属于比较领先的水平。
这里来说说Xcode5正式版，和以前的Xcode5测试版来比，正式版改动不大，不过也有稍许变化。整体来讲，跟Xcode4.6比，属于换了一个时代- -(因为以前的工程换到这里编译需要改很多地方)
Xcode5给我的感觉就是：界面简洁扁平化，配置工程图形化，还有就是。。白。为什么说白，看看配置IB和配置工程就知道了。。。

![](http://www.myexception.cn/img/2013/09/21/141350218.jpg)

![](http://www.myexception.cn/img/2013/09/21/141350219.jpg)


相比4.6来讲，5在操作上还是有很多大的改进。也可以用于开发旧版本的项目，需要把arm64去掉，把XIB文件设置成之前的版本就好。不过也有一些细节需要注意。所以嫌麻烦的话可以用之前版本的- -。
言归正传，这里来认识下三种IOS常见的回调模式。
##代理模式
作为IOS中最常见的通讯模式，代理几乎无处不在。
![](http://www.myexception.cn/img/2013/09/21/141350220.jpg)

看实例这里有一个数组，我们首先通过代理的方式将数组传递到其他方法中去。
设置协议及方法
```cpp
@protocol CallBackDelegate;

@interface ViewController : UIViewController
@property (weak, nonatomic) id<CallBackDelegate> delegate;
@end

@protocol CallBackDelegate <NSObject>
- (void)showArrayWithDelegate:(NSArray *)array;
@end
```

@interface ViewController () <CallBackDelegate>

点击按钮传递数组让其显示

```cpp
- (IBAction)delegateCallBack
{
    NSDictionary *dict = @{@"array": @[@"Chelsea", @"MUFC", @"Real Madrid"]};
    NSArray *array = dict[@"array"];
    [self.delegate showArrayWithDelegate:array];
}
```

调用，显示

```cpp
- (void)showArrayWithDelegate:(NSArray *)array
{
    _outputLabel.text = array[2];
}
```

最重要也是最容易忽略的，就是一定要设置delegate的指向。完成后屏幕显示![](http://www.myexception.cn/img/2013/09/21/141350221.jpg)


##使用通知中心

通知中心的方式可以不用设置代理，但是需要设置观察者和移除观察者。
![](http://www.myexception.cn/img/2013/09/21/141350222.jpg)

代码

```cpp
- (IBAction)callBack
{
    NSDictionary *dict = @{@"array": @[@"Chelsea", @"MUFC", @"Real Madrid"]};
    NSArray *array = dict[@"array"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OutputArrayNotification" object:array];
}
```

注册和移出观察者

```cpp
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(outputWithNote:) name:@"OutputArrayNotification" object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"OutputArrayNotification" object:nil];
}
```

显示
```cpp
- (void)outputWithNote:(NSNotification *)aNotification
{
    NSArray *receiveArray = [aNotification object];
    _outputLabel.text = receiveArray[0];
}
```

![](http://www.myexception.cn/img/2013/09/21/141350223.jpg)


##Block

什么是Block：从C的声明符到Objective-C的Blocks语法块代码以闭包得形式将各种内容进行传递，可以是代码，可以是数组无所不能。块代码十分方便将不同地方的代码集中统一，使其易读性增强。

来看这里怎么进行数组传递。
typedef void (^Arr_Block)(NSArray *array);


```cpp
- (void)showArrayUsingBlock:(Arr_Block)block
{
    NSDictionary *dict = @{@"array": @[@"Chelsea", @"MUFC", @"Real Madrid"]};
    NSArray *array = dict[@"array"];
    block(array);
}
```

调用方法，显示

```cpp
- (IBAction)blockCallBack
{
    [self showArrayUsingBlock:^(NSArray *array) {
        _outputLabel.text = array[1];
    }];
}
```

![](http://www.myexception.cn/img/2013/09/21/141350224.jpg)

三种模式都很轻松~


本文转自:[http://www.myexception.cn/operating-system/1407402.html](http://www.myexception.cn/operating-system/1407402.html)

title: IOS CCTransitionScene场景切换动画
date: 2014-02-13 09:40:00
categories:
- iOS游戏开发之基础学习
tags:
- cocos2d
---
**CCTransitionScene**

**[cpp]** [view
 plain](http://blog.csdn.net/youngsblog/article/details/9857803# "view plain")[copy](http://blog.csdn.net/youngsblog/article/details/9857803# "copy")1.     CCScene *level = [levelScene scene];  
2. //  1.切入效果  
3. //    CCTransitionScene *trans = [[CCTransitionSplitCols alloc] initWithDuration:2.0f scene:level];  
4. //  2.雷达效果  
5. //    CCTransitionScene *trans = [[CCTransitionRadialCCW alloc] initWithDuration:2.0f scene:level];  
6. //  3.小格子动画  
7. //    CCTransitionScene *trans = [[CCTransitionTurnOffTiles alloc] initWithDuration:2.0f scene:level];  
8. //  4.滑动效果  
9. //    CCTransitionScene *trans = [[CCTransitionSlideInL alloc] initWithDuration:2.0f scene:level];  
10. //  5.翻转效果  
11. //    CCTransitionScene *trans = [[CCTransitionFlipX alloc] initWithDuration:2.0f scene:level];  
12. //  6.淡入效果  
13.     CCTransitionScene *trans = [[CCTransitionCrossFade alloc] initWithDuration:0.5f scene:level];  
14.     // 给一个时间，让他动画到level剧场  
15.     // trans本来也是一个剧场  
16.     [[CCDirector sharedDirector] replaceScene:trans];  
17.     [trans release];  


从代码中可以看到其基本思路，用CCTransitionScene创建一个指针并根据需要创建不同切换效果的类，并使用构建方法与想要过渡到的场景建立联系。
对象方法:
- (id) initWithDration:(ccTime)t scene:(CCScene *)scene
或者类方法：
- (id) transitionWithDration:(ccTime)t scene:(CCScene *)scene
参数scene就是想要过渡到的场景。
谨记trans是一个继承于CCScene的一个类对象，那么要运作这个CCScene就必须叫导演来换节目了。
用导演的单例 [CCDirector sharedDirector] 使用- (void) replaceScene: (CCScene *)scene 播放这个包含第二个场景的动画场景。
经过0.5秒的切换将真正切换到 level 这个场景，trans 动画场景在使用后已经没有用处，可以将其release释放内存。
 
切换动画有很多，这里找了一个总汇：
来自：[http://blog.csdn.net/qiaoshe/article/details/6838191](http://blog.csdn.net/qiaoshe/article/details/6838191)，感谢分享


CCTransitionFade, //渐隐效果
CCTransitionFadeTR,//碎片效果
CCTransitionJumpZoom, //跳动效果
CCTransitionMoveInL, //从左向右移动
CCTransitionPageTurn, //翻页效果
CCTransitionRadialCCW,//钟摆效果
CCTransitionRotoZoom,//涡轮效果
CCTransitionSceneOriented,//
CCTransitionShrinkGrow, //渐远效果
CCTransitionSlideInL, //左移
CCTransitionSplitCols,//上下移动
CCTransitionTurnOffTiles//百叶窗
CCTransitionScene : CCScene  基类

CCRotoZoomTransition 旋转缩小切换
CCJumpZoomTransition 缩小后跳跃切换
CCSlideInLTransition 从左向右切换
CCSlideInRTransition 从右向左切换
CCSlideInTTransition 从上向下切换
CCSlideInBTransition 从下向上切换
CCShrinkGrowTransition逐渐缩小切换
CCFlipXTransition 已x中间为轴平面式旋转切换
CCFlipYTransition 已y中间为轴平面式旋转切换
CCFlipAngularTransition 侧翻式旋转切换
CCZoomFlipXTransition 缩小后x为轴旋转切换
CCZoomFlipYTransition 缩小后y为轴旋转切换
CCZoomFlipAngularTransition 缩小侧翻式旋转切换
CCFadeTransition 逐渐变暗切换
CCCrossFadeTransition   逐渐变暗切换2
CCTurnOffTilesTransition 随机方块覆盖切换
CCSplitColsTransition 三条上下分开切换
CCSplitRowsTransition 三条左右分开切换
CCFadeTRTransition 小方块大方块式切换 左下到右上
CCFadeBLTransition 小方块大方块式切换 右上到左下 
CCFadeUpTransition 百叶窗从下向上
CCFadeDownTransition 百叶窗从上向下

CCTransitionRotoZoom : CCTransitionScene 旋转进入
CCTransitionJumpZoom : CCTransitionScene 跳动进入
CCTransitionMoveInL : CCTransitionScene<CCTransitionEaseScene> 从左侧进入
CCTransitionMoveInR : CCTransitionMoveInL 从右侧进入
CCTransitionMoveInT : CCTransitionMoveInL 从顶部进入
CCTransitionMoveInB : CCTransitionMoveInL 从底部进入
CCTransitionSlideInL : CCTransitionScene<CCTransitionEaseScene> 从左侧滑入
CCTransitionSlideInR : CCTransitionSlideInL  从右侧滑入
CCTransitionSlideInB : CCTransitionSlideInL 从顶部滑入
CCTransitionSlideInT : CCTransitionSlideInL  从底部滑入
CCTransitionShrinkGrow : CCTransitionScene<CCTransitionEaseScene> 交替进入
CCTransitionFlipX : CCTransitionSceneOriented x轴翻入（左右）
CCTransitionFlipY : CCTransitionSceneOriented y轴翻入（上下）
CCTransitionFlipAngular : CCTransitionSceneOriented 左上右下轴翻入
CCTransitionZoomFlipX : CCTransitionSceneOriented x轴翻入放大缩小效果（左右）
CCTransitionZoomFlipY : CCTransitionSceneOriented y轴翻入放大缩小效果（上下）
CCTransitionZoomFlipAngular :CCTransitionSceneOriented  左上右下轴翻入放大缩小效果
CCTransitionFade : CCTransitionScene   变暗变亮进入
CCTransitionCrossFade : CCTransitionScene 渐变进入
CCTransitionTurnOffTiles : CCTransitionScene<CCTransitionEaseScene> 小方格消失进入
CCTransitionSplitCols : CCTransitionScene<CCTransitionEaseScene> 竖条切换进入
CCTransitionSplitRows : CCTransitionSplitCols  横条切换进入
CCTransitionFadeTR : CCTransitionScene<CCTransitionEaseScene> 小方格右上角显示进入
CCTransitionFadeBL : CCTransitionFadeTR 小方格左下角显示进入
CCTransitionFadeUp : CCTransitionFadeTR 横条向上显示进入
CCTransitionFadeDown : CCTransitionFadeTR 横条向下显示进入


title: 关于UITextField
date: 2013-09-05 11:42:00
categories:
- iOS应用开发之UI
tags:
- ios
- iphone开发
- uitextfield
---

```objc
7.9
CGRectMake(x,y,w,h);
CGPointMake(x,y);
===========================================================================================
1.设置子视图的位置。
 view.bounds = CGRectMake(0, 0, 100, 100);
  //bounds 是以子视图的为原点，设置大小
    view.center = CGPointMake(320/2, 460/2);
  //center 是以父视图的坐标系为基准
    view.backgroundColor = [UIColor greenColor];
  //设置视图的背景色
===========================================================================================
 2. 创建一个button1 默认类型为自定义按钮 
UIButton* button1 = [[UIButton alloc] initWithFrame:CGRectMake(100, 300, 100, 40)];
===========================================================================================
3.为button1的标题设置颜色。
[button1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
===========================================================================================
4.为button1设置图片
                                                     UIControlStateNormal  = 0,                   //常态                      
                                                     UIControlStateHighlighted  = 1 << 0,         //  高亮
                             UIControlStateDisabled     = 1 << 1,        //禁用 
                                                     UIControlStateSelected     = 1 << 2,        // 选中

[button1 setBackgroundImage:[UIImage imageNamed:@"btn1.png"] forState:UIControlStateNormal];
   //向button添加一个背景图片，默认情况下显示的图片
[button1 setBackgroundImage:[UIImage imageNamed:@"btn2.png"] forState:UIControlStateHighlighted];
    //高亮情况下显示的图片
      /*
    [btn3 setImage:[UIImage imageNamed:@"btn1.png"] forState:UIControlStateNormal];
     向btn3添加一个图片，但是向btn3设置标题的时候会覆盖标题，
     所以用setBackgroundImage:UIImage添加一个图片
     [UIImage imageNamed:@"btn1.png"]返回一个图片地址，(UIImage *)
     */
[btn3 setBackgroundImage:[UIImage imageNamed:@"btn1.png"]forState: UIControlStateNormal];
===========================================================================================
5.显示控件
[self.window addSubview:button1];
//当前window下，添加一个子视图==>button1
===========================================================================================
6.为视图空间关联事件
[button1 addTarget:self action:@selector(ok) forControlEvents:UIControlEventTouchUpInside];
    //如果在button1上触发UIControlEventTouchUpInside(点击抬起)就执行当前(self)类的方法@selector(ok)
===========================================================================================
7.创建一个UITextField
UITextField *text1=[[UITextField alloc]initWithFrame:CGRectMake(x, y, w, h)];
text1.borderStyle=UITextBorderStyleRoundedRect;
    //borderStyle 设置text1的风格
  
    //弹出键盘[text1 resignFirstResponder]; 

    //设置输入框的背景颜色，此时设置为白色 如果使用了自定义的背景图片边框会被忽略掉　
    　 text1.backgroundColor = [UIColor whiteColor];
    
    //设置背景
    text1.background = [UIImage imageNamed:@"dd.png"];
    
    //设置背景 
    text1.disabledBackground = [UIImage imageNamed:@"cc.png"];

    //当输入框没有内容时，水印提示 提示内容为password
    text1.placeholder = @"请输入账号";
    
    //设置输入框内容的字体样式和大小
    text1.font = [UIFont fontWithName:@"Arial" size:20.0f];
    
    //设置字体颜色
    text1.textColor = [UIColor redColor];
    ================================================ 
    //输入框中是否有个叉号，在什么时候显示，用于一次性删除输入框中的内容
    text1.clearButtonMode = UITextFieldViewModeAlways;
    
        UITextFieldViewModeNever,　重不出现
        UITextFieldViewModeWhileEditing, 编辑时出现
        UITextFieldViewModeUnlessEditing,　除了编辑外都出现
        UITextFieldViewModeAlways 　一直出现
    ================================================ 
    //每输入一个字符就变成点 用语密码输入
    text.secureTextEntry = YES;

    //再次编辑就清空
    text.clearsOnBeginEditing = YES; 
    
    //内容对齐方式
    text.textAlignment = UITextAlignmentLeft;
    
    //内容的垂直对齐方式  UITextField继承自UIControl,此类中有一个属性contentVerticalAlignment
    text.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    //设置为YES时文本会自动缩小以适应文本窗口大小.默认是保持原来大小,而让长文本滚动　
    textFied.adjustsFontSizeToFitWidth = YES;
    
    //设置自动缩小显示的最小字体大小
    text.minimumFontSize = 20;
    =================================================
    //设置键盘的样式
    text.keyboardType = UIKeyboardTypeNumberPad;

        UIKeyboardTypeDefault,     　默认键盘，支持所有字符         
        UIKeyboardTypeASCIICapable,　支持ASCII的默认键盘
        UIKeyboardTypeNumbersAndPunctuation,　标准电话键盘，支持＋＊＃字符
        UIKeyboardTypeURL,            URL键盘，支持.com按钮 只支持URL字符
        UIKeyboardTypeNumberPad,            　数字键盘
        UIKeyboardTypePhonePad,　 　电话键盘
        UIKeyboardTypeNamePhonePad, 　电话键盘，也支持输入人名
        UIKeyboardTypeEmailAddress, 　用于输入电子 邮件地址的键盘     
        UIKeyboardTypeDecimalPad,   　数字键盘 有数字和小数点
        UIKeyboardTypeTwitter,      　优化的键盘，方便输入@、#字符

===========================================================================================
8.
-(void)insertSubview:(UIView *)view atindex:(NSinteger)index;
//交换两个控件的层（上下关系）
-(void)exchangeSubviewAtindx:(NSinteger)index1withSubviewAtindex:(NSinteger)index2;
// index1和index2控件位置调换

```



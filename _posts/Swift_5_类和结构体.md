title: Swift_5_类和结构体
date: 2014-07-21 15:42:00
categories:
- iOS应用开发之Swift
tags:
- Swift
---

```objc
import Foundation

println("Hello, World!")



class PeopleClass{

    
}

struct PeopleStruct{
    
}

struct Resolution {
    var width = 0
    var heigth = 0
}

class VideoMode {
    var resolution = Resolution()
    var interlaced = false
    var frameRate = 0.0
    var name : String?
}


let someResolution = Resolution()
let someVideoMode = VideoMode()

println("The width of someResolution is \(someResolution.width)")

someVideoMode.resolution.width = 12880
println("The width of someVideoMode is now \(someVideoMode.resolution.width)")

let vga = Resolution(width:640, heigth: 480)

println("vga is \(vga.width) \(vga.heigth)")

let hd = Resolution(width: 1920, heigth: 1080)
var cinema = hd
cinema.width = 2048


println("cinema is now  \(cinema.width) pixels wide")

```


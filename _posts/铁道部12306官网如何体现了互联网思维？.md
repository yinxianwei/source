title: 铁道部12306官网如何体现了互联网思维？
date: 2014-01-08 19:15:00
---
本文转自:[互联网的一些事](http://www.yixieshi.com/it/15563.html)
![铁道部12306官网如何体现了互联网思维？,互联网的一些事](http://www.yixieshi.com/uploads/allimg/140108/1-14010Q02412O4.jpg)　　年关将近，12306果断成为被上与被骂的最多网站。可是你们知道12306有多努力吗?
　　攥着在网吧刷了三小时才抢到的无座票，你终于可以登上回家的火车了。被挤悬空的你也许没空思考，这张小小的票根上凝聚了多少比特智慧和科技情怀。
　　现在连卖煎饼果子的、卖牛腩的、卖山寨手机的、卖避孕套的，都恨不能说自己是互联网思维的化身，可是咋整委员会要告诉你，12306才是真正运用互联网思维的天才之作!!!
　　**1、用户思维：得屌丝者得天下**
　　“得屌丝者得天下”已是互联网圈内人所皆知的真理。12306不惜花费数亿，为众多坐不起灰机的屌丝们开发了该网站，找到了屌丝用户的硬需求，狠击痛点，抓住了互联网的“长尾”。
![铁道部12306官网如何体现了互联网思维？,互联网的一些事](http://mmbiz.qpic.cn/mmbiz/884XsHGHibP5bianFoD137OSCiaUhMGoW2LQsmsIwjb4ANZCcKnia3tPrpd52zu7bebOw1esQc6R3novNm3QySuZfw/0)　　**2、极致思维：丑的极致是美**
　　极致思维，是把产品、服务和用户体验做到超越用户的预期。12306的页面设计做到了极简。复古的配色，不羁的线条，甚至一直缓冲的小logo，都大大出乎目前互联网用户的理解水准。所以，看不懂就是你格调的问题了。
![铁道部12306官网如何体现了互联网思维？,互联网的一些事](http://mmbiz.qpic.cn/mmbiz/884XsHGHibP5bianFoD137OSCiaUhMGoW2LNiamFsjYHF7MraxY7umIgiacwlphjY9tgXHePOwagSkyHhnOjQibibcHvg/0)　　**3、让用户参与其中：排排队，散散心**
　　好的用户体验应该从细节入手，让用户在使用过程中有所思考、有所感动是12306网站设计的初衷，在特别安排的排队环节中便可见一斑。
　　浮躁的社会，快节奏的生活，人们似乎忘记了内心安宁是什么状态了。而在排队等票的过程中，用户们可以安静地喝杯茶、看部电影、思考自己所理解的生活。据统计，每一位购票者在抢购春节车票期间，平均获得30分钟以上的宁静时间。
![铁道部12306官网如何体现了互联网思维？,互联网的一些事](http://mmbiz.qpic.cn/mmbiz/884XsHGHibP5bianFoD137OSCiaUhMGoW2L8zsR2eSH5w75ZhzrR6PbhZ4viboBNbhUC77RMHOic9RR78O1wlQicQm5A/0)　　**4、人机交互：跟着验证码练视力**
　　在用户参与和反馈过程中，12306的验证码环节也在不断迭代升级，从“小数值算术题”进化为“彩色动态验证码机制”，完美体现了凯文·凯利的人机交互思维——12306调动起你眼珠转动的体感，了解你的状态，调节你的视力，提供更符合你眼神儿的购票时间。
![铁道部12306官网如何体现了互联网思维？,互联网的一些事](http://mmbiz.qpic.cn/mmbiz/884XsHGHibP5bianFoD137OSCiaUhMGoW2LQclUia96ibPe5tvCvr75Qg8MDNtQpJSrib9WUNEibs4yg7PyhoRefxxqibg/0)　　**5、开放平台：拉动产业链**
　　“开放、共享、共赢”是互联网思维的基本要素，秉承这一理念的12306已成为一个巨大的孵化器，造就了多种抢票衍生品的出现：软件、浏览器、手机客户端……
　　同时，12306也鼓舞着人们不断进行众包尝试、云端协作，一个买票任务，十人分工协作，每次任务成功都是UGC的成果。
![铁道部12306官网如何体现了互联网思维？,互联网的一些事](http://mmbiz.qpic.cn/mmbiz/884XsHGHibP5bianFoD137OSCiaUhMGoW2LciaP54phccOicT6w4D4QzEiaMM9J75icZshCDBuSv9ibkHnTknesNownlkA/0)　　**6、饥饿营销：秒到就是赢，让用户尖叫起来！**
　　巧妙调动用户心理也是互联网巨头们惯用的手法。面对旺盛的用户需求，12306着重突出几个板块：开票日期、剩余票量、支付时限。
　　还有1天!!只剩9张!!还有9秒!!!不到支付的最后一步，抢票就不算成功，从来没有什么消费能做到让用户这般大声尖叫，12306才是饥饿营销的真正王者。
![铁道部12306官网如何体现了互联网思维？,互联网的一些事](http://mmbiz.qpic.cn/mmbiz/884XsHGHibP5bianFoD137OSCiaUhMGoW2LzWbZWic3Hnwd8PYibibqpeJ1EyUUCobB7yTicQWpJpdKyqDpOcKqK3fjxw/0)　　来自：咋整
 
 

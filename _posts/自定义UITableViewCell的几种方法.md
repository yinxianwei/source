title: 自定义UITableViewCell的几种方法
date: 2013-09-05 13:14:00
categories:
- iOS应用开发之UITableViewCell
tags:
- ios
- iphone开发
---


自定义cell的三种方式。

(1)

UITableviewcell的子类自定义cell
1.新建一个类继承自UITableviewcell。
2。初始化方法中添加自己的控件

```objc
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.menuImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 37, 100)];
        [self addSubview:_menuImageView];
        [_menuImageView release];
    }
    return self;
}

```

3。使用

```objc
static NSString *cellIdenifer = @"MenuTableViewCellInDentifer";
    
    LPMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifer];
    
    if (!cell) 
    {
        cell = [[[LPMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdenifer] autorelease];
        
    }
```
(2)
在tableview的代理方法中自定义cell

```objc
static NSString *cellIdenifer = @"MenuTableViewCellInDentifer";
    
    LPMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifer];
    
    if (!cell) 
    {
        cell = [[[LPMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdenifer] autorelease];

UILabel *aaa = [[UILabel alloc] init]
[cell addsubview:aaa];
aaa.tag = 111;
        
    }

UILabel *label = [cell viewWithTag:111];

```

(3)

xib自定义cell
1.新建一个类继承自uitableviewcell。
2.新建一个空的xib.
3.在xib中拖拽一个uitableviewcell，然后设置一下他的大小
4.更改刚才拖拽的uitableviewcell的类型为 第一步中新建的类的类型。
5.设置uitableviewcell的idenifier为一个固定字符串，这个字符串要跟tableview的数据源中定义的字符串吻合
static NSString *cellIdenifer = @"MenuTableViewCellInDentifer";（应该跟xib中cell设置的吻合）
    
    LPMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifer];
6.在xib的cell中添加自定义的控件
7.在自定义的cell类中声明属性，供以后连接使用
8.连接xib中的控件和类中的属性，关联的时候不是关联到xib的file's owner上，而是uitableviewcell本身。
9.使用 

```objc
static NSString *cellIdenifer = @"DishesTableViewCellInDentifer";（应该跟xib中cell设置的idenifier吻合）
    
   （自定义的cell类） LPDishTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifer];
        
    if (!cell) 
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LPDishTableViewCell" owner:nil options:nil];（此为刚才新建的xib的名字）
        
        for (id oneObject in nib) 
        {
            if ([oneObject isKindOfClass:[LPDishTableViewCell class]]) 
            {
                cell = (LPDishTableViewCell *)oneObject;
            }
        }
    }

```

                        


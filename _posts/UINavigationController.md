title: UINavigationController
date: 2013-09-05 13:10:00
categories:
- iOS应用开发之UI
tags:
- ios
- iphone开发
---
7.21
导航控制器
入口类:
1.导航的创建

UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];

2.导航栏隐藏

navController.navigationBar.hidden = YES;

3.导航栏添加到界面

 [self.window addSubview:navController.view];

4.导航栏Button的创建

//initWithBarButtonSystemItem使用系统图标绑定方法
UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPause target:self action:@selector(btnClick)];
    self.navigationItem.rightBarButtonItem=btn;


UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"进入" style:UIBarButtonItemStyleBordered target:self action:@selector(btnClick)];
    self.navigationItem.rightBarButtonItem=btn;


UIController
1.导航的title命名

//设置导航的标题，title是viewcontroller的属性，
    self.title = @"登录";
//更改默认的titleview，默认情况下，只能改title的文字，如果想要更灵活的控制title，可以这样做
    //UILabel *titleLabel = [[UILabel alloc] init];
//    titleLabel.frame = CGRectMake(0, 0, 100, 30);
//    titleLabel.textColor = [UIColor yellowColor];
//    titleLabel.text = @"登陆";
//    self.navigationItem.titleView = titleLabel;

2.导航颜色的设置

//设置导航条的颜色，因为导航条是公共的，所以需要先找到导航控制器，然后再通过导航控制器找到它的导航条，然后设置颜色
    self.navigationController.navigationBar.tintColor = [UIColor redColor];

3.导航栏的返回创建

UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"返回上一级" style:UIBarButtonItemStyleDone target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;

4.导航的传值

在第一个UIControoler上写一个传值方法,在加载之前获取,传到第二个UIControoler上的属性
返回传值
//重写父类的方法
//viewcontroller将要显示到屏幕上的时候调用此方法
- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppearviewWillAppear");               
    field1.text = content2;
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppearviewDidAppear");
}

- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"viewWillDisappear");
}

- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"viewDidDisappear");
}

6.导航栏视图的切换

//创建第二个视图控制器
    SecondViewController *viewController = [[SecondViewController alloc] init];
/把第二个视图控制器添加到导航控制器中
    [self.navigationController pushViewController:viewController animated:YES];
返回上一个导航栏

//可以通过先找到入口类，然后再找到第一个视图控制器，但是不够通用
//得到所有导航控制器当中所有的viewcontroller
    NSArray *viewcontrollers = self.navigationController.viewControllers;
//找到倒数第二个视图控制器。最后控制器的索引是 count - 1，倒数第二个就是count - 2
    FirstViewController *viewController = [viewcontrollers objectAtIndex:[viewcontrollers count] - 2];
//返回上一级(把当前的viewcontroller从导航控制器中弹出)
    [self.navigationController popViewControllerAnimated:YES];


7.导航栏按钮的优先级

 /******
    navigationbar 按钮的优先级
    1.设置viewcontroller 的leftBarButtonItem
    2.上一级设置  backBarButtonItem
    3.使用上一级的标题
    4.系统默认为back
     ********/












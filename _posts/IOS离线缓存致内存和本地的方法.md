title: IOS离线缓存致内存和本地的方法
date: 2014-02-26 14:16:00
categories:
- iOS应用开发之网络通讯
tags:
- 缓存
---
缓存由四个文件组成。
 
FlashDiskCacheManager.h
FlashDiskCacheManager.m
 
这个类是以单例的模式提供的，您可以在您想要缓存的地方来share
 
URLCacheElement.h
URLCacheElement.m
 
这个类您永远都不要使用，每一个缓存就是一个URLCacheElement的对象，然后归档到文件中，实现磁盘缓存。
 
具体见FlashDiskCacheManager类的注释部分。
 
FlashDiskCacheManager.h
 

**[cpp]** [view
 plain](http://blog.csdn.net/windows_star/article/details/6117392# "view plain")[copy](http://blog.csdn.net/windows_star/article/details/6117392# "copy")1. //  
2. //  FlashDiskCacheManager.h  
3. //    
4. //  
5. //  Created by jeffrey on 10-12-21.  
6. //  Copyright 2010 AppUFO. All rights reserved.  
7. //  
8. #import <Foundation/Foundation.h>  
9. #import "URLCacheElement.h"  
10. @interface FlashDiskCacheManager : NSObject  
11. {  
12.     /** 存储已经缓存的url **/  
13.     NSMutableDictionary* urlDictionary;  
14.       
15.     NSKeyedUnarchiver* reader;  
16. }  
17. @property (nonatomic, retain) NSMutableDictionary *urlDictionary;  
18. /** 
19.  * 
20.  * 设置保存时间 
21.  * 
22.  */  
23. + (void) setMinStoreInterval:(double)interval;  
24. + (FlashDiskCacheManager*) sharedManager;  
25. /** 
26.  * 
27.  * 获取缓存的对象 如果为nil，则缓存已经失效，或者无缓存 
28.  * 
29.  */  
30. - (URLCacheElement*) cachedForWithURL:(NSURL*)url;  
31. /** 
32.  * 
33.  * 缓存请求1 
34.  * 
35.  */  
36. - (void) cachedWithURLResponse:(NSCachedURLResponse*) cachedResponse;  
37. /** 
38.  * 
39.  * 缓存请求2 
40.  * 
41.  */  
42. - (void) cachedWithData:(NSData*)data theUrl:(NSURL*)url;  
43. /** 
44.  * 
45.  * 写入闪存 
46.  * 
47.  */  
48. - (void) storeToDisk;  
49. @end  

 
 
FlashDiskCacheManager.m
 

**[cpp]** [view
 plain](http://blog.csdn.net/windows_star/article/details/6117392# "view plain")[copy](http://blog.csdn.net/windows_star/article/details/6117392# "copy")1. //  
2. //  FlashDiskCacheManager.m  
3. //    
4. //  
5. //  Created by jeffrey on 10-12-21.  
6. //  Copyright 2010 AppUFO. All rights reserved.  
7. #import "FlashDiskCacheManager.h"  
8.   
9. @implementation FlashDiskCacheManager  
10. @synthesize urlDictionary;  
11. static FlashDiskCacheManager* defaultManager = nil;  
12. /** 存储实效时间 **/  
13. static NSTimeInterval minStoreInterval = 10 * 60 * 60;  
14. + (void) setMinStoreInterval:(double)interval  
15. {  
16.     minStoreInterval = interval;  
17. }  
18. + (FlashDiskCacheManager*) sharedManager  
19. {  
20.     @synchronized(self)  
21.     {  
22.         if (defaultManager == nil)  
23.         {  
24.             defaultManager = [[self alloc] init];  
25.         }  
26.         return defaultManager;  
27.     }  
28. }  
29. - (id) init  
30. {  
31.     self = [super init];  
32.     if (self)  
33.     {  
34.         NSArray* path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
35.         self.urlDictionary = [[NSMutableDictionary alloc] init];  
36.         [urlDictionary release];  
37.           
38.         //解档  
39.         for (NSInteger index = 0; index < 100000; ++index)  
40.         {  
41.             NSString* document = [path objectAtIndex:0];  
42.             document = [document stringByAppendingPathComponent:[NSString stringWithFormat:@"cache%d", index]];  
43.             if ([[NSFileManager defaultManager] fileExistsAtPath:document])  
44.             {  
45.                 NSMutableData* data = [[NSMutableData alloc] initWithContentsOfFile:document];  
46.                 reader = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];  
47.                 URLCacheElement* element = [reader decodeObjectForKey:[NSString stringWithFormat:@"cache%d", index]];  
48.                 [reader finishDecoding];  
49.                 NSURL* urlKey = [[NSURL alloc] initWithString:element.name];  
50.                 NSTimeInterval now = [element.date timeIntervalSinceNow];  
51.                   
52.                 if (fabs(now) - minStoreInterval > 0)  
53.                 {  
54.                     [data release];  
55.                     [urlKey release];  
56.                     [reader release];  
57.                     continue;  
58.                 }  
59.                   
60.                 [urlDictionary setObject:element forKey:urlKey];  
61.                 [data release];  
62.                 [urlKey release];  
63.                 [reader release];  
64.             }  
65.             else  
66.             {  
67.                 break;  
68.             }  
69.         }  
70.         return self;  
71.     }  
72.     return nil;  
73. }  
74. - (URLCacheElement*) cachedForWithURL:(NSURL*)url  
75. {  
76.     URLCacheElement* element =  [urlDictionary objectForKey:url];  
77.     if (element == nil)  
78.     {  
79.         return nil;  
80.     }  
81.       
82.     NSTimeInterval now = [element.date timeIntervalSinceNow];  
83.     if (fabs(now) - minStoreInterval > 0)  
84.     {  
85.         [self.urlDictionary removeObjectForKey:url];  
86.         return nil;  
87.     }  
88.     return element;  
89. }  
90. - (void) cachedWithURLResponse:(NSCachedURLResponse*) cachedResponse  
91. {  
92.     NSURLResponse* response = [cachedResponse response];  
93.       
94.     //要存储的url  
95.     NSURL* url = [response URL];  
96.       
97.     //要存储的数据  
98.     NSData* data = [cachedResponse data];  
99.       
100.     //存储的时间  
101.     NSDate* date = [NSDate date];  
102.       
103.     // 存储的用户信息  
104.     NSDictionary* user = [cachedResponse userInfo];  
105.       
106.     URLCacheElement* element = [[URLCacheElement alloc] initWithData:data :date :user];  
107.     [self.urlDictionary setObject:element forKey:url];  
108.       
109.     [element release];  
110. }  
111. - (void) cachedWithData:(NSData*)data theUrl:(NSURL*)url  
112. {  
113.     //存储的时间  
114.     NSDate* date = [NSDate date];  
115.       
116.     // 存储的用户信息  
117.     NSDictionary* user = nil;  
118.       
119.     URLCacheElement* element = [[URLCacheElement alloc] initWithData:data :date :user];  
120.     [self.urlDictionary setObject:element forKey:url];  
121.       
122.     [element release];    
123. }  
124. - (void) storeToDisk  
125. {  
126.     NSArray* path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
127.     NSArray* arrayOfKeys = [urlDictionary allKeys];  
128.       
129.     //归档  
130.     for (NSInteger index = 0; index < [arrayOfKeys count]; ++index)  
131.     {  
132.         NSURL* url = (NSURL*)[arrayOfKeys objectAtIndex:index];  
133.         NSString* name = [url absoluteString];  
134.           
135.         //初始化路径  
136.         NSString* document = [path objectAtIndex:0];  
137.         document = [document stringByAppendingPathComponent:[NSString stringWithFormat:@"cache%d", index]];  
138.           
139.         NSMutableData* tData = [[NSMutableData alloc] init];  
140.         NSKeyedArchiver* write = [[NSKeyedArchiver alloc] initForWritingWithMutableData:tData];  
141.         URLCacheElement* element = [urlDictionary objectForKey:[arrayOfKeys objectAtIndex:index]];  
142.           
143.         //记录这个缓存的名字  
144.         element.name = name;  
145.           
146.         [write encodeObject:element forKey:[NSString stringWithFormat:@"cache%d", index]];  
147.           
148.         [write finishEncoding];  
149.         [tData writeToFile:document atomically:YES];  
150.           
151.         [tData release];  
152.         [write release];  
153.     }  
154. }  
155. - (void) dealloc  
156. {  
157.     [super dealloc];  
158.     [urlDictionary release];  
159. }  
160. @end  

 
 
URLCacheElement.h
 

**[cpp]** [view
 plain](http://blog.csdn.net/windows_star/article/details/6117392# "view plain")[copy](http://blog.csdn.net/windows_star/article/details/6117392# "copy")1. //  
2. //  URLCacheElement.h  
3. //    
4. //  
5. //  Created by jeffrey on 10-12-21.  
6. //  Copyright 2010 AppUFO. All rights reserved.  
7. //  
8. #import <Foundation/Foundation.h>  
9. #define KData @"urlData"  
10. #define KUserInfo @"userInfo"  
11. #define KDate @"date"  
12. #define KName @"name"  
13. @interface URLCacheElement : NSObject<NSCoding, NSCopying>  
14. {  
15.     /** url存储的数据 **/  
16.     NSData* data;  
17.       
18.     /** 存储的用户信息 **/  
19.     NSDictionary* userInfo;  
20.       
21.     /** 存储的时间 **/  
22.     NSDate* date;  
23.       
24.     NSString* name;  
25. }  
26. - (id) initWithData:(NSData*)indata :(NSDate*)indate :(NSDictionary*)inuserInfo;  
27. @property (nonatomic, retain) NSData *data;  
28. @property (nonatomic, retain) NSDictionary *userInfo;  
29. @property (nonatomic, retain) NSDate *date;  
30. @property (nonatomic, retain) NSString *name;  
31. @end  

 
 
URLCacheElement.m
 

**[cpp]** [view
 plain](http://blog.csdn.net/windows_star/article/details/6117392# "view plain")[copy](http://blog.csdn.net/windows_star/article/details/6117392# "copy")1. //  
2. //  URLCacheElement.m  
3. //    
4. //  
5. //  Created by jeffrey on 10-12-21.  
6. //  Copyright 2010 AppUFO. All rights reserved.  
7. #import "URLCacheElement.h"  
8.   
9. @implementation URLCacheElement  
10. @synthesize data;  
11. @synthesize date;  
12. @synthesize userInfo;  
13. @synthesize name;  
14. - (id) initWithData:(NSMutableData*)indata :(NSDate*)indate :(NSMutableDictionary*)inuserInfo  
15. {  
16.     self = [super init];  
17.     if (self)  
18.     {  
19.         self.data = indata;  
20.         self.date = indate;  
21.         self.userInfo = inuserInfo;  
22.         return self;  
23.     }  
24.     return nil;  
25. }  
26.   
27. - (id) initWithCoder:(NSCoder *)aDecoder  
28. {  
29.     self = [super init];  
30.     if (self)  
31.     {  
32.         self.data = [aDecoder decodeObjectForKey:KData];  
33.         self.date = [aDecoder decodeObjectForKey:KDate];  
34.         self.userInfo = [aDecoder decodeObjectForKey:KUserInfo];  
35.         self.name = [aDecoder decodeObjectForKey:KName];  
36.         return self;  
37.     }  
38.     return nil;  
39. }  
40.   
41. - (void) encodeWithCoder:(NSCoder *)aCoder  
42. {  
43.     [aCoder encodeObject:data forKey:KData];  
44.     [aCoder encodeObject:date forKey:KDate];  
45.     [aCoder encodeObject:userInfo forKey:KUserInfo];  
46.     [aCoder encodeObject:name forKey:KName];  
47. }  
48. - (id) copyWithZone:(NSZone *)zone  
49. {  
50.     URLCacheElement *copy = [[[self class] allocWithZone:zone] init];  
51.       
52.     self.data = [data copy];  
53.     self.date = [date copy];  
54.     self.userInfo = [userInfo copy];  
55.     name = [name copy];  
56.       
57.     return copy;  
58. }  
59. - (void) dealloc  
60. {  
61.     [super dealloc];  
62. }  
63. @end  


本文转自:[http://blog.csdn.net/windows_star/article/details/6117392](http://blog.csdn.net/windows_star/article/details/6117392)

title: UIWebView加载本地pdf、doc等文件
date: 2013-11-28 10:21:00
categories:
- iOS应用开发之UI
tags:
- iphone开发
- ios
- uiwebview
---

```objc
//加载本地pdf到webview

- (void) loadDocument:(NSString *)docName
{
    NSString *path = [[NSBundle mainBundle] pathForResource:docName ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self loadRequest:request];
}
```


http://www.cnblogs.com/wengzilin/archive/2012/03/28/2420873.html

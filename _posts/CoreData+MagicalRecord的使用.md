title: CoreData+MagicalRecord的使用
date: 2014-10-15 10:54:00
categories:
- iOS应用开发之第三方框架
tags:
- MagicalRecord
---
1.首先导入文件，地址：[https://github.com/magicalpanda/MagicalRecord](https://github.com/magicalpanda/MagicalRecord)
2.然后创建xcdatamodeld和实例对象，如下图
![](http://img.blog.csdn.net/20141015103725687?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)
不会创建的话请参照我的另一份文章：[http://blog.csdn.net/yin_xianwei/article/details/20618755](http://blog.csdn.net/yin_xianwei/article/details/20618755)
3.导入头文件，大家都会


```objc
   #import "CoreData+MagicalRecord.h"
```
4.在appdelgate创建数据库


```objc
    [MagicalRecord setupCoreDataStackWithStoreNamed:[NSString stringWithFormat:@"%@.sqlite", @"Test"]];
```
5.启动应用之后你会在你的模拟器沙盒路径下的Library/Application Support/工程名/  下看到数据库文件（iOS7）
6.接下来就是操作它了
你的Entity会有这些方法
6.1查找


```objc
 //查找所有
   NSArray *ary1 = [DogEntity MR_findAll];
    
    //查找并按name升序排序
  NSArray *ary2 = [DogEntity MR_findAllSortedBy:@"name" ascending:YES]; //查找type为2的数据
   NSArray *ary3 = [DogEntity MR_findByAttribute:@"type" withValue:@"2"];
//查找第一条数据
	 DogEntity *entyty = [DogEntity MR_findFirst];
```



	
6.2增加
```objc
    DogEntity *dog = [DogEntity MR_createEntity];
    
    dog.name = @"huahua";
    dog.type = @"2";
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];

```

6.3更新

```objc
   NSArray *ary = [DogEntity MR_findAll];
   
     DogEntity *dog = [ary lastObject];
        dog.name = @"hhhhhh";
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];

```


6.4删除



```objc
   NSArray *ary = [DogEntity MR_findAll];
   
        DogEntity *dog = [ary lastObject];
        [dog MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];

```


7.那么问题来了，你会发现所有的操作都会有一个MR_前缀（反正我觉得挺cool的），想要去掉的话就这样导入头文件：


```objc
    #define MR_SHORTHAND
    #import "CoreData+MagicalRecord.h"
```

8.有一些常用的方法


```objc
    根据某个属性某个条件查询
    NSArray *dogs = [DogEntity MR_findByAttribute:@"name" withValue:@"Y.X."];
    
    根据排序取得搜索结果
    NSArray *dogs = [DogEntity MR_findAllSortedBy:@"name" ascending:YES];

```

```objc
//    查询所有记录
+ (NSArray *) MR_findAll;
//根据上下文句柄查询所有记录
+ (NSArray *) MR_findAllInContext:(NSManagedObjectContext *)context;
//根据某个属性排序查询所有记录
+ (NSArray *) MR_findAllSortedBy:(NSString *)sortTerm ascending:(BOOL)ascending;
//根据某个属性排序以及上下文操作句柄查询所有记录
+ (NSArray *) MR_findAllSortedBy:(NSString *)sortTerm ascending:(BOOL)ascending inContext:(NSManagedObjectContext *)context;
//根据某个属性排序用谓词来查询记录
+ (NSArray *) MR_findAllSortedBy:(NSString *)sortTerm ascending:(BOOL)ascending withPredicate:(NSPredicate *)searchTerm;
//根据某个属性排序以及上下文操作句柄用谓词来查询记录
+ (NSArray *) MR_findAllSortedBy:(NSString *)sortTerm ascending:(BOOL)ascending withPredicate:(NSPredicate *)searchTerm inContext:(NSManagedObjectContext *)context;
//根据谓词查询
+ (NSArray *) MR_findAllWithPredicate:(NSPredicate *)searchTerm;
//根据谓词以及上下文操作句柄来查询
+ (NSArray *) MR_findAllWithPredicate:(NSPredicate *)searchTerm inContext:(NSManagedObjectContext *)context;
//以下都是查询一个对象时的操作,与上面重复,不一一赘述
+ (instancetype) MR_findFirst;
+ (instancetype) MR_findFirstInContext:(NSManagedObjectContext *)context;
+ (instancetype) MR_findFirstWithPredicate:(NSPredicate *)searchTerm;
+ (instancetype) MR_findFirstWithPredicate:(NSPredicate *)searchTerm inContext:(NSManagedObjectContext *)context;
+ (instancetype) MR_findFirstWithPredicate:(NSPredicate *)searchterm sortedBy:(NSString *)property ascending:(BOOL)ascending;
+ (instancetype) MR_findFirstWithPredicate:(NSPredicate *)searchterm sortedBy:(NSString *)property ascending:(BOOL)ascending inContext:(NSManagedObjectContext *)context;
+ (instancetype) MR_findFirstWithPredicate:(NSPredicate *)searchTerm andRetrieveAttributes:(NSArray *)attributes;
+ (instancetype) MR_findFirstWithPredicate:(NSPredicate *)searchTerm andRetrieveAttributes:(NSArray *)attributes inContext:(NSManagedObjectContext *)context;
+ (instancetype) MR_findFirstWithPredicate:(NSPredicate *)searchTerm sortedBy:(NSString *)sortBy ascending:(BOOL)ascending andRetrieveAttributes:(id)attributes, ...;
+ (instancetype) MR_findFirstWithPredicate:(NSPredicate *)searchTerm sortedBy:(NSString *)sortBy ascending:(BOOL)ascending inContext:(NSManagedObjectContext *)context andRetrieveAttributes:(id)attributes, ...;
+ (instancetype) MR_findFirstByAttribute:(NSString *)attribute withValue:(id)searchValue;
+ (instancetype) MR_findFirstByAttribute:(NSString *)attribute withValue:(id)searchValue inContext:(NSManagedObjectContext *)context;
+ (instancetype) MR_findFirstOrderedByAttribute:(NSString *)attribute ascending:(BOOL)ascending;
+ (instancetype) MR_findFirstOrderedByAttribute:(NSString *)attribute ascending:(BOOL)ascending inContext:(NSManagedObjectContext *)context;

```
参考：[http://www.haogongju.net/art/2570005](http://www.haogongju.net/art/2570005)

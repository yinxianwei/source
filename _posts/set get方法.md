title: set get方法
date: 2013-09-05 11:38:00
categories:
- iOS应用开发之第三方框架
tags:
- ios
- iphone开发
---

set get方法的简写
========.h========
@property(nonatomic, retain)属性类型 属性名;
   描述      一个属性一旦描述之后可以利用对象点.调用

例==>@property(nonatomic, retain)NSString *name[,NSString *aname];
           @property(nonatomic, assign)int age[,woking];
    可以同时描述多个相同类型的属性.
指针类型:retain(保留)       非指针类型:assign 
指针类型是字符串还可以 copy   只有字符串才能写copy
 
========.m========
@synthesize 属性名;
实现被描述的属性自动生成的方法的方法体
具体实现功能如下:
@synthesize name;
||||||||||||||||   注:同种数值类型可以用一句实现方法
||||||||||||||||   如:   @synthesize name,aname;
-(void)setName:(NSString *)aName
{
    name = aName;
}
-(NSString *)name
{
    return name;
}
===================
People *p1=[[People alloc] init];
p1.name=@"李四";
//相当于set方法,向那么赋值
[p1 name];
相当于get方法  例: NSString *newname=[p1 name];
  ==============把name的值赋予newname

  NSString *p2=[[People allic] init];
[p2.name];

//此时p2会以实际类型变化，而不是字符类型


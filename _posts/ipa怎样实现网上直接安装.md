title: ipa怎样实现网上直接安装
date: 2013-11-22 17:39:00
categories:
- iOS之应用发布
tags:
- ios
- iphone开发
---
you have to have two file. 
yourapp.ipa
yourapp.plist
in the plist :


<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>items</key>
    <array>
        <dict>
            <key>assets</key>
            <array>
                <dict>
                    <key>kind</key>
                    <string>software-package</string>
                    <key>url</key>
                    <string>[http://urlofyourapp/appname.ipa](http://urlofyourapp/appname.ipa)</string>
                </dict>
            </array>
            <key>metadata</key>
            <dict>
                <key>bundle-identifier</key>
                <string>com.domain.appname</string>
                <key>bundle-version</key>
                <string>1.0</string>
                <key>kind</key>
                <string>software</string>
                <key>title</key>
                <string>YourAppName</string>
            </dict>
        </dict>
    </array>
</dict>
</plist>

and create a webpage with a link
<a href="itms-services://?action=download-manifest&url=http://yourappurl/yourapp.plist">click
 to install</a>

最后要注意对方的机器里要有相应的provision profile.

哦？ls的再说说，这样的安装需要itunes不？
**capsen**2011-07-09 21:10不需要，完全使用网络安装，只要对方机器有了相应的provision profile，直接访问网页按后按那个连接选择安装就可以了。
**superarts**2011-07-10 13:33profile也可以网络下载安装吗？
**capsen**2011-07-10 15:21可以，但profile必须针对对方机器的ccid.
**visen_api**2011-08-05 11:08是ccid，还是udid?
**fordmendeo**2011-08-05 11:09mark。。。
**visen_api**2011-08-05 11:09profile文件需要什么内容？
**visen_api**2011-08-09 16:41重启问题未解决，顶！
**titi007**2012-04-13 09:29重启的问题，有人解决吗？
**nouon**2012-10-24 09:14我也遇到和楼主一样的问题，也是重启后就有了，不知道楼主是这么解决的

但是看到楼主的最后登录时间是2012-09-26

不知道楼主什么时候会回复我的问题，唉。。。
**wuyingce**2012-12-19 10:52在下载的plist文件中，又这样一个属性
                 <key>bundle-identifier</key>
                <string>com.domain.appname</string>

需要根据你的app中的bundle－identifier 来进行修改！之后就不会出现安装完成后消失的情况！[http://www.cocoachina.com/applenews/devnews/2013/0501/6113.html](http://www.cocoachina.com/applenews/devnews/2013/0501/6113.html)





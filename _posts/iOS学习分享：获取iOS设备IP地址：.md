title: iOS学习分享：获取iOS设备IP地址：
date: 2013-10-25 15:59:00
categories:
- iOS之常用方法
tags:
- 获取iP地址
---

```objc
#include <arpa/inet.h>
#include <net/if.h>
#include <ifaddrs.h>

```

```objc

```

```objc
- (NSString *)getIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL)
        {
            if(temp_addr->ifa_addr->sa_family == AF_INET)
            {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"])
                {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;

}
```


[From:http://www.cocoachina.com/bbs/read.php?tid=142534](http://www.cocoachina.com/bbs/read.php?tid=142534)

title: iOS屏幕旋转，屏幕自适应方向变化
date: 2014-01-21 09:57:00
categories:
- iOS应用开发之UI
tags:
- 屏幕旋转
---
文章转自:[http://moto0421.iteye.com/blog/1586791](http://moto0421.iteye.com/blog/1586791)


1. iOS有四个方向的旋转，为了保证自己的代码能够支持旋转，我们必须首先处理一个函数：
Objective-c代码  [![收藏代码](http://moto0421.iteye.com/images/icon_star.png)]( "收藏这段代码")1. - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {  
2.   
3.     return YES;  
4. }  

 2. 这个函数时用来确定我们的应用所支持的旋转方向。如果想要支持每个方向则直接返回YES就行，还可以单独判断某一方向：

Objective-c代码  [![收藏代码](http://moto0421.iteye.com/images/icon_star.png)]( "收藏这段代码")1. - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {  
2.   
3.     if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft) {  
4.   
5.         //left  
6.   
7.     }  
8.   
9.     if (interfaceOrientation==UIInterfaceOrientationLandscapeRight) {  
10.   
11.         //right  
12.   
13.     }  
14.   
15.     if (interfaceOrientation==UIInterfaceOrientationPortrait) {  
16.   
17.         //up  
18.   
19.     }  
20.   
21.     if (interfaceOrientation==UIInterfaceOrientationPortraitUpsideDown) {  
22.   
23.         //down  
24.   
25.     }  
26.   
27.     return YES;  
28. }  

 
3.IOS6之后又不太一样


```objc
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAllButUpsideDown ;
	//支持旋转的方向
}
```

```objc
- (BOOL)shouldAutorotate{ return YES;
	//是否旋转
}
```


```objc
 UIInterfaceOrientationMaskPortrait = (1 << UIInterfaceOrientationPortrait),
    UIInterfaceOrientationMaskLandscapeLeft = (1 << UIInterfaceOrientationLandscapeLeft),
    UIInterfaceOrientationMaskLandscapeRight = (1 << UIInterfaceOrientationLandscapeRight),
    UIInterfaceOrientationMaskPortraitUpsideDown = (1 << UIInterfaceOrientationPortraitUpsideDown),
    UIInterfaceOrientationMaskLandscape = (UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight),
    UIInterfaceOrientationMaskAll = (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskPortraitUpsideDown),
    UIInterfaceOrientationMaskAllButUpsideDown = (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight),
```

```objc

```
4. 当然旋转还有一些函数可触发：

```objc
//旋转方向发生改变时  
  
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {  
}  
//视图旋转动画前一半发生之前自动调用  
  
-(void)willAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {  
}  
//视图旋转动画后一半发生之前自动调用  
  
-(void)willAnimateSecondHalfOfRotationFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation duration:(NSTimeInterval)duration {  
}  
//视图旋转之前自动调用  
  
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {  
}  
//视图旋转完成之后自动调用  
  
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {  
}  
//视图旋转动画前一半发生之后自动调用  
  
-(void)didAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {  
}  
```



```objc


```

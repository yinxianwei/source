title: Xib的创建
date: 2013-09-05 13:14:00
categories:
- iOS应用开发之UI
tags:
- iphone开发
- ios
---
1:New file->User Interface 右边Empty
2:给xib取一个名称，例如a
3:在控件集中选择一个object，托放到编辑区，在编辑区左侧出现object
4:修改object的类为AppDelegate
5:修改File's Owner的类名UIApplication
6:把AppDelegate 和File's Owner连线.连线方法：选中AppDelegate,点击右键，把New Referencing Oulet 和File's Owner上的delegate 关联。
7:在控件集中拖一个window放到编辑区
8:把window和AppDelegate的中window进行关联。确保：AppDelegate 中有IBOutlet UIWIndow* window.关联方法同上。
9:在左侧工程列表文件中，找到Info.plist文件，开始编辑该文件。在编辑区，点击右键Add Row,添加一行 Main nib file base name，内容为xib文件的名称，不带后缀。
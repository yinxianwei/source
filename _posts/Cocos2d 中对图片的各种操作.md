title: Cocos2d 中对图片的各种操作
date: 2014-02-13 09:36:00
categories:
- iOS游戏开发之基础学习
tags:
- cocos2d
- 2d
- iphone开发
- ios
- ios开发
---
关于精灵的各种操作，总结一下以便以后复习查找。
内容简要：
1、初始化 2、创建无图的精灵 3、设置精灵贴图大小  4、添加入层中
5、对精灵进行缩放  6、对精灵款或高进行缩放  7、旋转精灵
8、设置精灵透明度  9、精灵的镜像反转  10、设置精灵的颜色
11、得到图的宽高   12、按照像素设定图片大小  13、在原有的基础上加xy的坐标
14、设置图片锚点    15、从新排列z轴顺序   16、更换精灵贴图
17、设置可视区域 18、贴图无锯齿


原文地址：[http://blog.csdn.net/dingkun520wy/article/details/6976558](http://blog.csdn.net/dingkun520wy/article/details/6976558)  

-----------------------------------------------------------------------------------------------------------------------------------------------------
//初始化
CCSprite* sprite =[CCSprite spriteWithFile:@"Icon.png"];
 //创建无图的精灵
CCSprite*sprite2 =[CCSprite node];
//设置精灵贴图大小
sprite2.textureRect=CGRectMake(0, 0, 20, 20);//设置其为宽20，高20.

//添加入层中

[self addChild:sprite z:2]; //将精灵加入层中设置其z轴为2
//对精灵进行缩放
sprite.scale=2;//放大2倍
//对精灵款或高进行缩放
sprite.scaleX = 2;//宽放大2倍 
sprite.scaleY = 2;//高放大2倍
//旋转精灵
sprite.rotation=90;//旋转90度
//设置精灵透明度
sprite.opacity=255;//设置透明度为完全不透明（范围0～255）
//定义精灵位置
sprite.position=ccp(100,100);//设置精灵中心点坐标是x=100，y=100
//精灵的镜像反转
[sprite setFlipX:YES];//X轴镜像反转
[sprite setFlipY:YES];//Y轴镜像反转
//设置精灵的颜色
[sprite setColor:ccc3(255, 0, 0)];//设置颜色为红色
//得到图的宽高

float  contentSize  = sprite .contentSize.width //得到图片的宽高
//按照像素设定图片大小

sprite.scaleX=(20)/contentSize; //按照像素定制图片宽高
//在原有的基础上加xy的坐标
sprite.position = ccpAdd(sprite.position,ccp(20,20));//在原有坐标的基础上加减坐标
//设置图片锚点
[sprite setAnchorPoint:ccp(0.5,0.5) ];//设置图片的锚点
//从新排列z轴顺序
[self reorderChild:sprite z:1];//从新排列z轴顺序
//更换精灵贴图
CCTexture2D * test=[[CCTextureCache sharedTextureCache] addImage: @"test.png"];//新建贴图

[sprite setTexture:test]; 
//更换精灵贴图，加载帧缓存，这个test.plist保存了fram这张图
[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"test.plist"];
CCSpriteFrame* frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"fram.png"];
[sprite2 setDisplayFrame:frame];

//设置可视区域
CCSprite * sprite3 =[CCSprite spriteWithFile:@"icon.png" rect:CGRectMake(0, 0, 20,20)];//创建时设置

[sprite3 setTextureRect:CGRectMake(10, 10, 30, 30)];//创建后设置
//贴图无锯齿
[sprite3 .texture setAliasTexParameters];

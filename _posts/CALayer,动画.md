title: CALayer,动画
date: 2013-09-19 15:17:00
categories:
- iOS应用开发之动画
tags:
- ios
- iphone开发
- 动画
---
1.图层-CALayer


```plain
UIView *tView = [[UIView alloc] initWithFrame:CGRectMake(50, 50, 100, 100)];
	[self.view addSubview:tView];
	CALayer *layer = tView.view.layer;
    //图层的拐角半径
	layer.cornerRadius = 50;
	//边框的宽度
    layer.borderWidth = 10;
    //边框的颜色CGColorRef  不可以直接用UIColor来创建对象
    //可以用UIColor来创建对象，然后调CGColor来获得CGColorRef的内容
    layer.borderColor = [UIColor blueColor].CGColor;
  //设置阴影的偏移量(默认向上偏移并有一定的扩散程度)
	tView.layer.shadowOffset = CGSizeMake(5, 5);
    //设置阴影的半径(是阴影的模糊的半径)
	tView.layer.shadowRadius = 50;
    //阴影的默认的透明度是0，如果想要阴影显示出来 必须设置阴影的透明度为非0
	tView.layer.shadowOpacity = 1;
```



2.事物处理 - CATransaction  
 图层的动画属性的每一个修改必然是事务的一个部分。CATransaction 是核心动画里面负责协调多个动画原子更新显示操作。事务支持嵌套使用。 核心动画支持两种事务:隐式事务和显式事务。在图层的动画属性被一个线程修 改,同时该线程下次迭代的时候自动提交该修改的时候隐式事务自动创建。显式事务 发生在程序在修改动画属性之前给 CATransaction 发送了一个开始消息,在动画属性修改之后提交该消息。


```plain
[CATransaction begin];
    //设置动画的时间
    [CATransaction setAnimationDuration:10];
    //设置动画效果不可用 默认为NO
    [CATransaction setDisableActions:YES];
	//-1旋转 正数不旋转
    _nLayer.transform = CATransform3DMakeScale(1.5, 1.5, -1);
	//如果设置图层的多个属性，那么这些隐式动画会叠加到一起
    [CATransaction commit];
	 //设置图层的位置  图层中的 position  相当于视图中的 center，默认的是几何中心
    //_nLayer.position = CGPointMake(200, 200);
```

3.图层动画
     3.1.CAPropertyAnimation
         是一个抽象的子类,它支持动画的显示图层的关键路 径中指定的属性
     3.2.CABasicAnimation
        简单的为图层的属性提供修改。
    3.3.CAKeyframeAnimation
        支持关键帧动画,你可以指定的图层属性的关键路径动画,包括动画的每个阶段的价值,以及关键帧时间和计时功能的 一的系列值。在动画运行时,每个值被特定的插入值替代。


```plain
- (void)viewDidLoad {
	 button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.backgroundColor = [UIColor redColor];
	button.frame = CGRectMake(0, 230, 60, 60);
	[self.view addSubview:button];
	
	[button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
	[button setTitle:@"按钮" forState:UIControlStateNormal];
    
	button.layer.cornerRadius = 30;
	button.layer.shadowRadius = 10;
	button.layer.shadowColor = [UIColor blueColor].CGColor;
	button.layer.shadowOpacity = 0.5;
	button.layer.shadowOffset = CGSizeMake(10, 10);
	 button.layer.position = CGPointMake(0, 230);
	CAKeyframeAnimation *tAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    tAnimation.delegate = self;
    
    //动画的关键点，第一个是起始位置，目标位置...
    tAnimation.values = [NSArray arrayWithObjects:
                         [NSValue valueWithCGPoint:CGPointMake(0, 230)],
                         [NSValue valueWithCGPoint:CGPointMake(260, 230)],
                         [NSValue valueWithCGPoint:CGPointMake(100, 230)],
                         [NSValue valueWithCGPoint:CGPointMake(160, 230)],
                         nil];
    //动画的关键时间点，和values相对应，第一个是开始的时间比（总时间乘以这个比例），接下来是运动到下一个目标的时间，以此类推
    tAnimation.keyTimes = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0],
                           [NSNumber numberWithFloat:0.5],
                           [NSNumber numberWithFloat:0.8],
                           [NSNumber numberWithFloat:1],
                           nil];
    
    //结束的时候不移除
    //tAnimation.removedOnCompletion = NO;
    //结束的时候保持结束的状态
    //tAnimation.fillMode = kCAFillModeForwards;
    //设置动画的时间，如果不设置时间，会有一个默认的时间，时间是比较快的，但是得到的duration是0
    tAnimation.duration = 2;
    [button.layer addAnimation:tAnimation forKey:@"key"];
}
```


```plain
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
	button.layer.position = CGPointMake(160, 230);
	/*第二种移动方法
	// 初始化一个关键帧动画，移动位置
    CAKeyframeAnimation *positionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    
    positionAnimation.fillMode = kCAFillModeForwards;
    positionAnimation.removedOnCompletion = NO;
    positionAnimation.duration = 3;
    
    //时间的变化曲线
    positionAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
 
    //创建路径信息，开始点 － 最远点 － 最近点 － 结束点
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0, 100);
    CGPathAddLineToPoint(path, NULL, 260, 100);
    CGPathAddLineToPoint(path, NULL, 100, 100);
    CGPathAddLineToPoint(path, NULL, 160, 100);
    positionAnimation.path = path;
    CGPathRelease(path);
    [button.layer addAnimation:positionAnimation forKey:@"animation"];
	*/
	//绕着z轴旋转
    CAKeyframeAnimation *rotateAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotateAnimation.fillMode = kCAFillModeForwards;
    rotateAnimation.removedOnCompletion = NO;
    rotateAnimation.duration = 5;
    //旋转时间
    rotateAnimation.keyTimes = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.2],[NSNumber numberWithFloat:0.6], nil];
    //旋转角度
    rotateAnimation.values = [NSArray arrayWithObjects:[NSNumber numberWithFloat:M_PI/4],[NSNumber numberWithFloat:0.0f], nil];
    
    [_layer2 addAnimation:rotateAnimation forKey:@"animation2"];
}
```


```plain
-(void)buttonClick:(UIButton *)button
{
	//CABasicAnimation
	    //toValue传递的是最中的效果   byValue是初始状态的一个增量
    //如果没有设置formValue那么会在当前的基础上到达最终效果
    //1.旋转
    CABasicAnimation *theAnimation;
	theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.rotation"];
	theAnimation.duration = 1;
	theAnimation.repeatCount = 0;
	theAnimation.removedOnCompletion = FALSE;
	theAnimation.fillMode = kCAFillModeBoth;
	theAnimation.autoreverses = NO;
    theAnimation.fromValue = [NSNumber numberWithFloat:M_PI / 4];
	theAnimation.byValue = [NSNumber numberWithFloat:M_PI / 2];
	[_layer addAnimation:theAnimation forKey:@"animation3"];
    
    //2.缩放
    CABasicAnimation *theAnimation2;
	theAnimation2=[CABasicAnimation animationWithKeyPath:@"transform.scale"];
	theAnimation2.duration = 1;
	theAnimation2.repeatCount = 0;
	theAnimation2.removedOnCompletion = FALSE;
	theAnimation2.fillMode = kCAFillModeBoth;
	theAnimation2.autoreverses = NO;
	theAnimation2.toValue = [NSNumber numberWithFloat:2];
	[_layer2 addAnimation:theAnimation2 forKey:@"animation4"];
}
```

 3.4.CAAnimationGroup 允许一系列动画效果组合在一起,并行显示动画。


```plain
	CAAnimationGroup *animationgroup = [CAAnimationGroup animation];
    animationgroup.animations = [NSArray arrayWithObjects:positionAnimation, rotateAnimation, nil];
    //设置动画事件
    animationgroup.duration = 10.0f;
    
    animationgroup.removedOnCompletion = NO;
    animationgroup.fillMode = kCAFillModeForwards;
    
    animationgroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    [_label.layer addAnimation:animationgroup forKey:@"label_animation"];
```

4.View的拖动,点击
把具有相同属性的View集成到一个类中


```plain
//需要设置支持多点触摸，默认的是单点
 self.multipleTouchEnabled = YES; 
```


```plain
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self.superview bringSubviewToFront:self];
	    //获得当前触控点数
	 if ([touches count] == 1) {
        UITouch *touch = [touches anyObject];
		//在一定时间内点击的次数 用于处理双击事件
        int count = [touch tapCount];
        switch (count) {
		//把锚点和中心点移到点击的地方 只设置锚点,锚点会寻找中心点放置
            case 1:
            {
                //获得相对于小view的点
                CGPoint point1 = [touch locationInView:self];
                //获得相对于大view的点
                CGPoint point2 = [touch locationInView:self.superview];
                self.layer.anchorPoint = CGPointMake(point1.x / self.frame.size.width, point1.y / self.frame.size.height);
                self.center = point2;
            }
                break;
            case 2: 
                NSLog(@"处理双击ing...");
                break;
            default:
                break;
        }

}

```


```plain
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([touches count] == 1) {
        UITouch *touch = [touches anyObject];
        CGPoint point2 = [touch locationInView:[self superview]];
        
        self.center = point2;
    }
    else if ([touches count] == 2) {
        //获得当前点击的两个触控点
        NSArray *array = [touches allObjects];
        UITouch *touch1 = [array objectAtIndex:0];
        UITouch *touch2 = [array objectAtIndex:1];
        CGPoint point1 = [touch1 locationInView:self];
        CGPoint point2 = [touch2 locationInView:self];
        //获得x坐标移动的绝对值 abs();
        int distance = abs(point1.x - point2.x);

        //如果发生缩放
        if (_oldDistance != 0) {
            //得到缩放距离
            int newDistance = distance - _oldDistance;
            CGRect rect = self.frame;
            //把当前的View以比例缩放
            rect.origin.x -= newDistance / 2;
            rect.origin.y -= newDistance / 2;
            rect.size.width += newDistance;
            rect.size.height += newDistance;
            self.frame = rect;
        }
        //保存当前缩放距离
        _oldDistance = distance;
        
    }	
}
```


```plain
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{	
	//结束时清零
    _oldDistance = 0;
}
```

//set方法和UI的绑定 



```plain
-(void)setTitle:(NSString *)title
{
    //在set方法中 self.不可以出现在 = 号左边
    if (_title != title) {
        
        [_title release];
        _title = [title retain];
        
        _titleLabel.text = self.title;
        
    }
}
```





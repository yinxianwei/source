title: URL编码和解码
date: 2013-11-25 22:46:00
categories:
- iOS应用开发之常用方法
tags:
- URL编码
---
ios中http请求遇到汉字的时候，需要转化成UTF-8，用到的方法是：
NSString *encodingString = [urlStringstringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
2.url解码
请求后，返回的数据，如何显示的是这样的格式：%3A%2F%2F，此时需要我们进行UTF-8解码，用到的方法是：
NSString *str =[model.album_namestringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];


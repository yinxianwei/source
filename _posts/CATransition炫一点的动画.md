title: CATransition炫一点的动画
date: 2013-09-18 17:38:00
categories:
- iOS应用开发之动画
tags:
- iphone开发
- ios
- 动画
- api
---
1.导入库和头文件
QuartzCore.framework
#import <QuartzCore/QuartzCore.h>
 CATransition 提供了一个图层变化的过渡效果,它能影响图层的整个内容。 动画进行的时候淡入淡出(fade)、推(push)、显露(reveal)图层的内容。这些过渡效 果可以扩展到你自己定制的 Core Image 滤镜。

```plain
    //CAAnimation图层动画的基类  这些都是图层动画
    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    //动画时间
    animation.duration = 0.7;
    //速度类型
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    //动画效果
    animation.type = kCATransitionFade;
    //动画方向
    animation.subtype = kCATransitionFromLeft;
    //交换两个view的层
    NSUInteger green = [[self.view subviews] indexOfObject:self.greenView];
    NSUInteger blue = [[self.view subviews] indexOfObject:self.blueView];
    [self.view exchangeSubviewAtIndex:green withSubviewAtIndex:blue];
    //向当前view添加动画
     [self.view.layer addAnimation:animation forKey:@"animation"];
```

```plain
            //淡化
            animation.type = kCATransitionFade;
            //推挤
            animation.type = kCATransitionPush;
            //揭开
            animation.type = kCATransitionReveal;
            //覆盖
            animation.type = kCATransitionMoveIn;
            //立方体
            animation.type = @"cube";
            //吸收
            animation.type = @"suckEffect";
            //翻转
            animation.type = @"oglFlip";
            //波纹
            animation.type = @"rippleEffect";
            //翻页
            animation.type = @"pageCurl";
            //反翻页
            animation.type = @"pageUnCurl";
            //镜头开
            animation.type = @"cameraIrisHollowOpen";
            //镜头关
            animation.type = @"cameraIrisHollowClose";


```



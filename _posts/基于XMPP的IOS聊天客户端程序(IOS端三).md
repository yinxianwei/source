title: 基于XMPP的IOS聊天客户端程序(IOS端三)
date: 2014-01-07 15:36:00
categories:
- iOS应用开发之社交软件
tags:
- ios开发
- iphone开发
- XMPP
- ios
- 聊天
---
[ 本文转自：http://blog.csdn.net/kangx6/article/details/7750765](http://blog.csdn.net/kangx6/article/details/7750765)

前两篇介绍了如何通过XMPP来发送消息和接收消息，这一篇我们主要介绍如何来美化我们的聊天程序，看一下最终效果呢，当然源程序也会在最后放出
![](http://my.csdn.net/uploads/201207/16/1342409417_2820.png)

好了，我们来看一下我们写的程序
这里我们自定义了TableViewCell
![](http://my.csdn.net/uploads/201207/16/1342411906_4192.png)

一行是显示发布日期，一行是显示发送的消息，还有一个是背景

**[java]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7750765# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7750765# "copy")1. -(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{  
2.       
3.     self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];  
4.     if (self) {  
5.         //日期标签  
6.         senderAndTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 300, 20)];  
7.         //居中显示  
8.         senderAndTimeLabel.textAlignment = UITextAlignmentCenter;  
9.         senderAndTimeLabel.font = [UIFont systemFontOfSize:11.0];  
10.         //文字颜色  
11.         senderAndTimeLabel.textColor = [UIColor lightGrayColor];  
12.         [self.contentView addSubview:senderAndTimeLabel];  
13.           
14.         //背景图  
15.         bgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];  
16.         [self.contentView addSubview:bgImageView];  
17.           
18.         //聊天信息  
19.         messageContentView = [[UITextView alloc] init];  
20.         messageContentView.backgroundColor = [UIColor clearColor];  
21.         //不可编辑  
22.         messageContentView.editable = NO;  
23.         messageContentView.scrollEnabled = NO;  
24.         [messageContentView sizeToFit];  
25.         [self.contentView addSubview:messageContentView];  
26.   
27.     }  
28.       
29.     return self;  
30.       
31. }  

定义好，在UITableViewCell中将Cell改成我们自己定义的Cell

**[java]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7750765# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7750765# "copy")1. -(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{  
2.       
3.     static NSString *identifier = @"msgCell";  
4.       
5.     KKMessageCell *cell =(KKMessageCell *)[tableView dequeueReusableCellWithIdentifier:identifier];  
6.       
7.     if (cell == nil) {  
8.         cell = [[KKMessageCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];  
9.     }  
10.       
11.     NSMutableDictionary *dict = [messages objectAtIndex:indexPath.row];  
12.       
13.     //发送者  
14.     NSString *sender = [dict objectForKey:@"sender"];  
15.     //消息  
16.     NSString *message = [dict objectForKey:@"msg"];  
17.     //时间  
18.     NSString *time = [dict objectForKey:@"time"];  
19.       
20.     CGSize textSize = {260.0 ,10000.0};  
21.     CGSize size = [message sizeWithFont:[UIFont boldSystemFontOfSize:13] constrainedToSize:textSize lineBreakMode:UILineBreakModeWordWrap];  
22.       
23.     size.width +=(padding/2);  
24.       
25.     cell.messageContentView.text = message;  
26.     cell.accessoryType = UITableViewCellAccessoryNone;  
27.     cell.userInteractionEnabled = NO;  
28.       
29.     UIImage *bgImage = nil;  
30.       
31.     //发送消息  
32.     if ([sender isEqualToString:@"you"]) {  
33.         //背景图  
34.         bgImage = [[UIImage imageNamed:@"BlueBubble2.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:15];  
35.         [cell.messageContentView setFrame:CGRectMake(padding, padding*2, size.width, size.height)];  
36.           
37.         [cell.bgImageView setFrame:CGRectMake(cell.messageContentView.frame.origin.x - padding/2, cell.messageContentView.frame.origin.y - padding/2, size.width + padding, size.height + padding)];  
38.     }else {  
39.           
40.         bgImage = [[UIImage imageNamed:@"GreenBubble2.png"] stretchableImageWithLeftCapWidth:14 topCapHeight:15];  
41.           
42.         [cell.messageContentView setFrame:CGRectMake(320-size.width - padding, padding*2, size.width, size.height)];  
43.         [cell.bgImageView setFrame:CGRectMake(cell.messageContentView.frame.origin.x - padding/2, cell.messageContentView.frame.origin.y - padding/2, size.width + padding, size.height + padding)];  
44.     }  
45.       
46.     cell.bgImageView.image = bgImage;  
47.     cell.senderAndTimeLabel.text = [NSString stringWithFormat:@"%@ %@", sender, time];  
48.   
49.     return cell;  
50.       
51. }  

在这个Cell里设置了发送的消息的背景图和接收消息的背景图
这里在字典里有一个"time"
这是我们接收和发送消息的时间

**[java]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7750765# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7750765# "copy")1. +(NSString *)getCurrentTime{  
2.       
3.     NSDate *nowUTC = [NSDate date];  
4.       
5.     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];  
6.     [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];  
7.     [dateFormatter setDateStyle:NSDateFormatterMediumStyle];  
8.     [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];  
9.       
10.     return [dateFormatter stringFromDate:nowUTC];  
11.       
12. }  

在AppDelegate.m中
将我们收到消息的内容也做一下调整

**[java]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7750765# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7750765# "copy")1. - (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message{  
2.       
3. //   ......  
4.       
5.     NSMutableDictionary *dict = [NSMutableDictionary dictionary];  
6.     [dict setObject:msg forKey:@"msg"];  
7.     [dict setObject:from forKey:@"sender"];  
8.     //消息接收到的时间  
9.     [dict setObject:[Statics getCurrentTime] forKey:@"time"];  
10.       
11.    ......  
12.       
13. }  

最后我们再设置一下每一行显示的高度

**[java]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7750765# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7750765# "copy")1. //每一行的高度  
2. -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{  
3.       
4.     NSMutableDictionary *dict  = [messages objectAtIndex:indexPath.row];  
5.     NSString *msg = [dict objectForKey:@"msg"];  
6.       
7.     CGSize textSize = {260.0 , 10000.0};  
8.     CGSize size = [msg sizeWithFont:[UIFont boldSystemFontOfSize:13] constrainedToSize:textSize lineBreakMode:UILineBreakModeWordWrap];  
9.       
10.     size.height += padding*2;  
11.       
12.     CGFloat height = size.height < 65 ? 65 : size.height;  
13.       
14.     return height;  
15.       
16. }  

，对了，在发送消息的时候，别忘了也加上

**[java]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7750765# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7750765# "copy")1. - (IBAction)sendButton:(id)sender {  
2.       
3.     //本地输入框中的信息  
4.     ......  
5.       
6.     if (message.length > 0) {  
7.           
8.         .....  
9.           
10.         NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];  
11.           
12.         [dictionary setObject:message forKey:@"msg"];  
13.         [dictionary setObject:@"you" forKey:@"sender"];  
14.         [dictionary setObject:[Statics getCurrentTime] forKey:@"time"];  
15.   
16.         [messages addObject:dictionary];  
17.           
18.         //重新刷新tableView  
19.         [self.tView reloadData];  
20.           
21.     }  
22.       
23.       
24. }  

好了，这里关于XMPP发送消息的教程就结束了，以后我们会详细介绍其他关于XMPP的内容
[源码下载](http://download.csdn.net/detail/kangkangz4/4431399)


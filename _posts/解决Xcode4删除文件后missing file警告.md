title: 解决Xcode4删除文件后missing file警告
date: 2013-11-27 11:50:00
categories:
- iOS应用开发之Xcode配置
tags:
- ios
- iphone开发
---
在用xcode4开发的时候，删除不用的文件后， 编译的时候会有missing file的警告，原因是由于SVN或git造成的。
有几种方法可以解决。
1.命令行进入missing file目录，然后运行
1. svn delete nameOfMissingFile  


或
1. git rm nameOfMissingFile  


2.删除隐藏的.svn文件
命令行运行
	defaults write com.apple.finder AppleShowAllFiles TRUE


开启显示隐藏文件，然后到工程目录下删除.svn文件，然后再恢复
	defaults write com.apple.finder AppleShowAllFiles FALSE    


3.进入工程目录，运行下面命令删除隐藏文件
	find . -name .svn -exec rm -rf { } \;  


 
 
 
报警是因为，先在文件夹中删除工程中引用的文件，工程引用的路径还存在，删掉也还会报错，怀疑是bug
 
以上都不管用，找到报警信息，找到相应文件夹，和相应文件名称，新建一个同名文件，拖入到工程内，不要选择copy，clean后，在工程中删除此文件，一切都清净了。
 
注意：删除资源文件一定要从工程中删，否则后或自负。
 
Targets中
copy Bundle Resource  中的链接一定是惟一的，否则会报错，这个错误最好解决，直接输入重复的文件名，保留一个，其他删除
 
copy Bundle Resource  中一定不要包含info.plist  否则会报错
[WARN]Warning: The Copy Bundle Resources build phase contains this target's Info.plist file 'cocoa2dMVCDemo/Resources/Info.plist'.
这个错误也容易解决，删掉链接即可  ，方法有两种，（加深理解）
1.copy Bundle Resource 中输入  info.plist  删除
2.左边视窗找到info.plist文件，点击，右边属性视窗    TargetMemberShip  勾选取消掉    一切OK了

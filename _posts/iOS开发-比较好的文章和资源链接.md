title: iOS开发-比较好的文章和资源链接
date: 2014-02-18 13:39:00
categories:
- iOS应用开发之学习资源
tags:
- ios开发
---
看到的一些好的文章和资源分享下
有些链接需要注册登录才能正常浏览下载。
[http://www.cocoachina.com/bbs/read.php?tid=105689](http://www.cocoachina.com/bbs/read.php?tid=105689)  开发一年的经验 
[http://www.cocoachina.com/newbie/tutorial/2012/0720/4475.html](http://www.cocoachina.com/newbie/tutorial/2012/0720/4475.html) 抽屉式导航，让用户专注于核心的功能
[http://www.cocoachina.com/newbie/tutorial/2012/0719/4467.html](http://www.cocoachina.com/newbie/tutorial/2012/0719/4467.html) UITableView实现划动删除
[http://www.cocoachina.com/newbie/tutorial/2012/0607/4334.html](http://www.cocoachina.com/newbie/tutorial/2012/0607/4334.html) 使用SQLite3存储和读取数据
[http://www.cocoachina.com/newbie/basic/2012/0703/4415.html](http://www.cocoachina.com/newbie/basic/2012/0703/4415.html) 一个横版的tableView：FantasyView
[http://www.cocoachina.com/iphonedev/toolthain/2012/0828/4664.html](http://www.cocoachina.com/iphonedev/toolthain/2012/0828/4664.html) iOS高效开发必备的10款Objective-C类库
[http://www.cocoachina.com/iphonedev/toolthain/2011/1223/3778.html](http://www.cocoachina.com/iphonedev/toolthain/2011/1223/3778.html) 如何在UIAlertView中显示进度条
[http://www.cocoachina.com/bbs/read.php?tid=105203](http://www.cocoachina.com/bbs/read.php?tid=105203) iphone开发十几个实用demo合集 ([https://github.com/yuyi012](https://github.com/yuyi012))
[http://www.cocoachina.com/bbs/read.php?tid=79949](http://www.cocoachina.com/bbs/read.php?tid=79949) iphone 自定义控件，通讯录，网络，电子书，控件特效 等源码
[http://www.cocoachina.com/bbs/read.php?tid=74564](http://www.cocoachina.com/bbs/read.php?tid=74564) 10个迷惑新手的Cocoa，Objective-c开发难点和问题(回头开发完了再看)
[http://www.cocoachina.com/bbs/read.php?tid=83864](http://www.cocoachina.com/bbs/read.php?tid=83864) 完整地翻译了一份ASIHttpRequest的文档
[http://www.crifan.com/intro_ios_simulator_in_xcode_and_usage_summary](http://www.crifan.com/intro_ios_simulator_in_xcode_and_usage_summary)/ 【整理】Xcode中的iOS模拟器(iOS
 Simulator)的介绍和使用心得
[http://www.cocoachina.com/bbs/read.php?tid=73570](http://www.cocoachina.com/bbs/read.php?tid=73570) iphone开发笔记和技巧总结 （优先看，跟帖回答，全是实用性的）
[http://www.cocoachina.com/bbs/read.php?tid=38761&page=1](http://www.cocoachina.com/bbs/read.php?tid=38761&page=1) 苹果开发重要语法知识（优先看，跟帖回答，全是理论）
[http://liwpk.blog.163.com/blog/static/3632617020114794852539/](http://liwpk.blog.163.com/blog/static/3632617020114794852539/)  用法总结：NSNumber、NSString、NSDate、NSCalendarDate、NSData(待续)
  
[http://www.189works.com/article-80018-1.html](http://www.189works.com/article-80018-1.html) NSURLConnection
[http://my.oschina.net/sunqichao/blog/75011](http://my.oschina.net/sunqichao/blog/75011) IOS ASI http 框架详解

[http://blog.csdn.net/workhardupc100/article/details/6941685](http://blog.csdn.net/workhardupc100/article/details/6941685) 官方文档
[http://www.rcstc.com/2012/02/%E8%87%AA%E5%AE%9A%E4%B9%89mkannotationview%E6%A0%87%E8%AE%B0%E5%8F%8Acallout/](http://www.rcstc.com/2012/02/%E8%87%AA%E5%AE%9A%E4%B9%89mkannotationview%E6%A0%87%E8%AE%B0%E5%8F%8Acallout/) 
  自定义MKAnnotationView标记及callout
 其实说的是自定义callout，其实是通过两个annotations来实现的，假设我们在地图上面放置一个A，我们可以在相应选择A之后在插上一个B，是B的偏移位置刚刚好在A的上面，就形成了callout的效果。
主要是要实现地图的两个代理方法：
– mapView![](file:///D:/Fetion/Languages/default/Misc/EMOTIONS/smile_teeth.gif)idSelectAnnotationView:
– mapView![](file:///D:/Fetion/Languages/default/Misc/EMOTIONS/smile_teeth.gif)idDeselectAnnotationView:
在第一个代理方法中中插入B，在第二个方法中移除B。大概自定义的callout就是这样实现的， 
[http://blog.csdn.net/lonelyroamer/article/details/7665112](http://blog.csdn.net/lonelyroamer/article/details/7665112) Objective-C的setter和getter
[http://www.cnblogs.com/syxchina/archive/2012/09/17/2689787.html](http://www.cnblogs.com/syxchina/archive/2012/09/17/2689787.html) IOS之应用程序设置（在设置程序添加自己应用的设置首选项）

[http://blog.csdn.net/totogo2010/article/details/7727896](http://blog.csdn.net/totogo2010/article/details/7727896) IOS程序名称及内容国际化（本地化）

[](http://blog.sina.com.cn/s/blog_6ba069d1010149g4.html)[http://mobile.51cto.com/iphone-284116.htm](http://mobile.51cto.com/iphone-284116.htm) 
 iOS开发使用委托delegate在不同窗口之间传递数据

[http://wenku.baidu.com/view/1573410c581b6bd97f19ea9a.html](http://wenku.baidu.com/view/1573410c581b6bd97f19ea9a.html) 文库app上线流程

[http://blog.csdn.net/visualcatsharp/article/details/7180237](http://blog.csdn.net/visualcatsharp/article/details/7180237) app发布要点

[http://mobile.51cto.com/market-332164.htm](http://mobile.51cto.com/market-332164.htm)  iPhone开发入门：在App
 Store上发布程序

[http://www.cocoachina.com/bbs/read.php?tid-7923.html](http://www.cocoachina.com/bbs/read.php?tid-7923.html)  如何联机调试和发布程序。

[http://www.techolics.com/apple/20120401_197.html ](http://www.techolics.com/apple/20120401_197.html)iPhone应用提交流程：如何将App程序发布到App
 Store
[http://www.cocoachina.com/bbs/read.php?tid=2776](http://www.cocoachina.com/bbs/read.php?tid=2776) 总结个人经验，史上最完整的IDP申请直到软件上架销售流程   
[http://www.cocoachina.com/bbs/read.php?tid=68636](http://www.cocoachina.com/bbs/read.php?tid=68636) 如何把ios
 app放到app store或者真实iphone/ipad设备上？
[http://www.techolics.com/apple/20120401_197.html](http://www.techolics.com/apple/20120401_197.html)  iPhone应用提交流程：如何将App程序发布到App
 Store？
[http://developer.apple.com/library/ios/#documentation/LanguagesUtilities/Conceptual/iTunesConnect_Guide/8_AddingNewApps/AddingNewApps.html#//apple_ref/doc/uid/TP40011225-CH13-SW1](http://developer.apple.com/library/ios/#documentation/LanguagesUtilities/Conceptual/iTunesConnect_Guide/8_AddingNewApps/AddingNewApps.html#//apple_ref/doc/uid/TP40011225-CH13-SW1)  官方itunesconnect
 文档

[http://blog.csdn.net/vieri_ch/article/details/7719856](http://blog.csdn.net/vieri_ch/article/details/7719856)  UINavigationbar的背景修改方法集合

[http://www.cnblogs.com/pengyingh/articles/2350890.html](http://www.cnblogs.com/pengyingh/articles/2350890.html) ios5自定义导航条

[http://blog.csdn.net/wave_1102/article/details/4768212](http://blog.csdn.net/wave_1102/article/details/4768212) 为navigationbar设置背景图片
[http://www.cocoachina.com/bbs/read.php?tid=33050](http://www.cocoachina.com/bbs/read.php?tid=33050)  Objective
 C 中实现单例模式   

[http://www.cocoachina.com/iphonedev/sdk/2010/1125/2396.html](http://www.cocoachina.com/iphonedev/sdk/2010/1125/2396.html) 应用程序内购买
旧工程适配iOS6和iPhone5
[http://www.cocoachina.com/applenews/devnews/2012/1204/5243.html](http://www.cocoachina.com/applenews/devnews/2012/1204/5243.html) 第三方静态库
[http://www.cocoachina.com/applenews/devnews/2012/1204/5244.html](http://www.cocoachina.com/applenews/devnews/2012/1204/5244.html) UIActivityIndicatorView
[http://blog.cnrainbird.com/index.php/2012/09/28/jiu_gong_cheng_shi_pei_ios6_he_iphone5_xu_zhi_executable_permissions_problem/](http://blog.cnrainbird.com/index.php/2012/09/28/jiu_gong_cheng_shi_pei_ios6_he_iphone5_xu_zhi_executable_permissions_problem/)  executable
 permissions problem
[http://www.cocoachina.com/applenews/devnews/2012/1212/5312.html](http://www.cocoachina.com/applenews/devnews/2012/1212/5312.html) iOS推送功能解决方案
[http://www.cocoachina.com/applenews/devnews/2012/1212/5313.html](http://www.cocoachina.com/applenews/devnews/2012/1212/5313.html)  实现iOS长时间后台的两种方法：Audiosession和VOIP
[http://hi.baidu.com/feng20068123/item/dbcbaec278b5a776cfd4f85c](http://hi.baidu.com/feng20068123/item/dbcbaec278b5a776cfd4f85c)   XCode ios iphone 资源文件管理 NSBundle
 mainBundle
[http://www.cnblogs.com/easonoutlook/archive/2012/09/13/2684134.html](http://www.cnblogs.com/easonoutlook/archive/2012/09/13/2684134.html)  Xcode工程添加第三方文件的详细分析 Create
 folder references for any added folders
[http://www.devdiv.com/forum.php?mod=viewthread&tid=132999](http://www.devdiv.com/forum.php?mod=viewthread&tid=132999)    xcode4的环境变量，workspace及联编设置 
[http://www.cocoachina.com/newbie/basic/2012/0816/4603.html](http://www.cocoachina.com/newbie/basic/2012/0816/4603.html) 论坛用户mhmwadmiOS开发心得分享
[http://www.cocoachina.com/newbie/basic/2012/0831/4685.html](http://www.cocoachina.com/newbie/basic/2012/0831/4685.html) CGContext小记
[http://blog.csdn.net/startexcel/article/details/7240247](http://blog.csdn.net/startexcel/article/details/7240247)  常用UI相关的第三方开源库
[http://blog.csdn.net/startexcel/article/details/7224576](http://blog.csdn.net/startexcel/article/details/7224576)   IOS5 ARC unsafe_unretained等说明
[http://blog.sina.com.cn/s/blog_55a8a96d01012vik.html](http://blog.sina.com.cn/s/blog_55a8a96d01012vik.html)  为什么不要在init和dealloc函数中使用accessor 
[http://www.it165.net/pro/html/201209/3721.html](http://www.it165.net/pro/html/201209/3721.html)  Objective-C多线程编程之GCD
[http://blog.csdn.net/pjk1129/article/details/6561781](http://blog.csdn.net/pjk1129/article/details/6561781) IOS开发UI展示之UITableView ──分页加载

**本文转自:**[http://blog.csdn.net/u012416493/article/details/19247467](http://blog.csdn.net/u012416493/article/details/19247467)

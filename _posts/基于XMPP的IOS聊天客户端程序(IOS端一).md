title: 基于XMPP的IOS聊天客户端程序(IOS端一)
date: 2014-01-07 15:27:00
categories:
- iOS应用开发之社交软件
tags:
- ios开发
- iphone
- iphone开发
- XMPP
---
[本文转自：http://blog.csdn.net/kangx6/article/details/7740135](http://blog.csdn.net/kangx6/article/details/7740135)


介绍完了服务器，这篇我们就要介绍重点了，写我们自己的IOS客户端程序
先看一下我们完成的效果图
![](http://my.csdn.net/uploads/201207/13/1342164575_2627.png)![](http://my.csdn.net/uploads/201207/13/1342164555_1035.png)

![](http://my.csdn.net/uploads/201207/13/1342164590_1834.png)

首先下载xmppframework这个框架,[下载](https://github.com/robbiehanson/XMPPFramework)
![](http://my.csdn.net/uploads/201207/12/1342074297_5170.png)
点ZIP下载
接下来，用Xcode新建一个工程
将以下这些文件拖入新建工程中
![](http://my.csdn.net/uploads/201207/12/1342077119_4696.png)

![](http://my.csdn.net/uploads/201207/12/1342074830_1517.png)

加入framework
![](http://my.csdn.net/uploads/201207/12/1342077141_6924.png)

并设置
![](http://my.csdn.net/uploads/201207/12/1342077154_5471.png)
到这里我们就全部设好了，跑一下试试，看有没有错呢
如果没有错的话，我们的xmppframework就加入成功了。


我们设置我们的页面如下图：
![](http://my.csdn.net/uploads/201207/12/1342077701_5655.png)

我们的KKViewController.h

**[java]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7740135# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7740135# "copy")1. #import <UIKit/UIKit.h>  
2.   
3. @interface KKViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>  
4.   
5. @property (strong, nonatomic) IBOutlet UITableView *tView;  
6.   
7. - (IBAction)Account:(id)sender;  
8. @end  



KKViewController.m

**[java]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7740135# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7740135# "copy")1. #import "KKViewController.h"  
2.   
3. @interface KKViewController (){  
4.       
5.     //在线用户  
6.     NSMutableArray *onlineUsers;  
7.       
8. }  
9.   
10. @end  
11.   
12. @implementation KKViewController  
13. @synthesize tView;  
14.   
15. - (void)viewDidLoad  
16. {  
17.     [super viewDidLoad];  
18.     self.tView.delegate = self;  
19.     self.tView.dataSource = self;  
20.       
21.     onlineUsers = [NSMutableArray array];  
22.     // Do any additional setup after loading the view, typically from a nib.  
23. }  
24.   
25. - (void)viewDidUnload  
26. {  
27.     [self setTView:nil];  
28.     [super viewDidUnload];  
29.     // Release any retained subviews of the main view.  
30. }  
31.   
32. - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation  
33. {  
34.     return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);  
35. }  
36.   
37. - (IBAction)Account:(id)sender {  
38. }  
39.   
40. #pragma mark UITableViewDataSource  
41.   
42. -(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{  
43.       
44.     return [onlineUsers count];  
45. }  
46.   
47. -(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{  
48.       
49.     static NSString *identifier = @"userCell";  
50.     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];  
51.     if (cell == nil) {  
52.         cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];  
53.     }  
54.       
55.       
56.     return cell;  
57.       
58.       
59. }  
60.   
61. - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{  
62.       
63.     return 1;  
64. }  
65.   
66. #pragma mark UITableViewDelegate  
67. -(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{  
68.       
69.       
70. }  
71.   
72.   
73. @end  

这里的代码相信大家学过UITableView的话应该很熟悉了，如果不知道的话，就查一下UITableView的简单应用学习一下吧
接下来是登录的页面
![](http://my.csdn.net/uploads/201207/13/1342164193_4863.png)

KKLoginController.m

**[java]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7740135# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7740135# "copy")1. - (IBAction)LoginButton:(id)sender {  
2.       
3.     if ([self validateWithUser:userTextField.text andPass:passTextField.text andServer:serverTextField.text]) {  
4.           
5.         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];  
6.         [defaults setObject:self.userTextField.text forKey:USERID];  
7.         [defaults setObject:self.passTextField.text forKey:PASS];  
8.         [defaults setObject:self.serverTextField.text forKey:SERVER];  
9.         //保存  
10.         [defaults synchronize];  
11.           
12.         [self dismissModalViewControllerAnimated:YES];  
13.     }else {  
14.         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入用户名，密码和服务器" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];  
15.         [alert show];  
16.     }  
17.   
18. }  
19.   
20. - (IBAction)closeButton:(id)sender {  
21.       
22.     [self dismissModalViewControllerAnimated:YES];  
23. }  
24.   
25. -(BOOL)validateWithUser:(NSString *)userText andPass:(NSString *)passText andServer:(NSString *)serverText{  
26.       
27.     if (userText.length > 0 && passText.length > 0 && serverText.length > 0) {  
28.         return YES;  
29.     }  
30.       
31.     return NO;  
32. }  

下面是聊天的页面
![](http://my.csdn.net/uploads/201207/13/1342164714_9508.png)
这里着重的还是UITableView
KKChatController.m

**[java]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7740135# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7740135# "copy")1. -(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{  
2.       
3.     return 1;  
4. }  
5.   
6. -(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{  
7.     return [messages count];  
8. }  
9.   
10. -(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{  
11.       
12.     static NSString *identifier = @"msgCell";  
13.       
14.     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];  
15.       
16.     if (cell == nil) {  
17.         cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];  
18.     }  
19.       
20.     NSMutableDictionary *dict = [messages objectAtIndex:indexPath.row];  
21.       
22.     cell.textLabel.text = [dict objectForKey:@"msg"];  
23.     cell.detailTextLabel.text = [dict objectForKey:@"sender"];  
24.     cell.accessoryType = UITableViewCellAccessoryNone;  
25.       
26.     return cell;  
27.       
28. }  

这些都比较简单，相信大家应该都能看得懂
把这些都设置好以后，我们就要着重介绍XMPP了，怕太长了，接下一章吧。


title: UIPickerView--UIDatePicker--UITableView
date: 2013-09-05 13:12:00
categories:
- iOS应用开发之UI
tags:
- ios
- iphone开发
---
7.25
1>UIPickerView   拾取器
1.创建UIPickerView 

UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.showsSelectionIndicator = YES;
    [self.view addSubview:picker];
    [picker release];
2.设置picker的列行.

//有几列
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    //NSLog(@"numberOfComponentsInPickerView");
    return 2;
}

//每一列有多少行
- (NSInteger):(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    //NSLog(@"numberOfRowsInComponent  %d",component);
    if (component == 0)
    {
        return [_provinceArray count];
    }
    else if(component == 1)
    {
        int selectedProvinceRow = [pickerView selectedRowInComponent:0];
        
        return [[_cityArray objectAtIndex:selectedProvinceRow] count];
    }
    
    return 0;
}
3.调用picker的协议方法
4.设置picker的行列显示的内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSLog(@"titleForRow  %d   component  %d",row ,component);
    if (component == 0)
    {
        return [_provinceArray objectAtIndex:row];
    }
    else if(component == 1)
    {
        int selectedProvinceRow = [pickerView selectedRowInComponent:0];
        return [[_cityArray objectAtIndex:selectedProvinceRow] objectAtIndex:row];
    }
    return @"";
}
5.当列和行发生改变时刷新行列

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"row    %d    component   %d",row,component);
    //第0列变化的时候，需要更新第1列的值
    if (component == 0)
    {
        [pickerView reloadComponent:1];
        //手动的制定选择某一列，某一行
        [pickerView selectRow:0 inComponent:1 animated:YES];
        //[pickerView reloadAllComponents];
    }
}

2>UIDatePicker      时间拾取器

//1.创建UIDatePicker及绑定方法
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
    [datePicker addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventValueChanged];

//2.获取自定义格式的UIDatePicker的时间
    NSDate *now = datePicker.date;
    
    //日期格式器
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设置日期格式器的样式
    //yyyy 年  yy 两位数的年   year
    //MM 月  month
    //dd  日  day
    //HH 24小时制  ，hh 12小时制，hour
    //mm  分 ，minute
    //ss 秒   second
    //a 上午，下午
    
    [dateFormatter setDateFormat:@"yyyy年MM月dd日 hh:mm:ss a  eeee"];
    [dateFormatter setAMSymbol:@"上午"];
    [dateFormatter setPMSymbol:@"下午"];
    [dateFormatter setWeekdaySymbols:[NSArray arrayWithObjects:@"星期日",@"星期一",@"星期二",@"星期三",@"星期四",@"星期五",@"星期六",nil]];
    //按照设定的样式来格式化日期，返回格式化后的字符串
    NSString *dateString = [dateFormatter stringFromDate:now];


3>UITableView        

    //1.UITableView的创建和基本设置
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 300) style:UITableViewStyleGrouped];
    //tableView.separatorColor = [UIColor redColor];
    //设置分割线风格
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.dataSource = self;
    tableView.delegate = self;
    //设置全局每行高度
    tableView.rowHeight = 80;
    [self.view addSubview:tableView];
    [tableView release];


//2.UITableView的 dataSource和delegate协议方法

//返回表中有多少个“区”，默认为一个，此方法不是必须的，不实现默认为一个区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

//返回表中每个“区”的行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

//返回每一行的内容  indexPath  返回行和列,  row   section
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CellIdentifier";
    
    //queue 队列 Reusable 可重用的
    //从tableview当中取得一个可以重用的cell，如果取不到可以重用的cell。该方法返回空nil
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //如果没有就创建
    if (!cell)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        NSLog(@"创建新的cell");
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"sdfsdf";
        label.backgroundColor = [UIColor clearColor];
        label.frame = CGRectMake(200, 0, 50, 40);
        [cell addSubview:label];
        [label release];
        
        //系统分割线被隐藏,so,需要手动添加一个分割线
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 79, 320, 1)];
        separatorView.backgroundColor = [UIColor lightGrayColor];
        [cell addSubview:separatorView];
        [separatorView release];
        //设置选中状态颜色,默认为蓝色
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.imageView.image = [UIImage imageNamed:@"fleet8.png"];
        //设置右边显示内容,btn或..
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    //配置cell上的文字,配置cell上可变的内容
    cell.textLabel.text = [NSString stringWithFormat:@"%d",indexPath.row];
    
    //cell.detailTextLabel.text = @"sdfsdfsdf";
    
    return cell;
}

//只有当每一行的高度不一样的时候，才使用代理方法设置行高，如果每一行的高度都一样，最好使用tableview.rowHeight属性来统一的设置行高
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"dddddd");
//    return indexPath.row * 2 + 10;
//}
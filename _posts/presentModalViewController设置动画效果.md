title: "presentModalViewController设置动画效果"
date: 2015-04-02 15:23:15
categories: iOS应用开发之UI
tags: 动画
---

共有以下四种动画效果：  

	{% raw %}typedef NS_ENUM{% endraw %}(NSInteger, UIModalTransitionStyle) {    
		    UIModalTransitionStyleCoverVertical = 0,//默认   
		    UIModalTransitionStyleFlipHorizontal,//旋转   
		    UIModalTransitionStyleCrossDissolve,//淡入   
		    UIModalTransitionStylePartialCurl NS_ENUM_AVAILABLE_IOS(3_2),//翻页   
		};
	
<!--more-->
	
自定义 重写动画//要导入QuartzCore这个库

	adressBook *adressPage=[[adressBook alloc] init];  
	CATransition *transition=[CATransition animation];  
	transition.timingFunction=UIViewAnimationCurveEaseInOut;  
	transition.duration=0.4;  
	transition.type=kCATransitionMoveIn;  
	transition.type=kCATransitionPush;  
	transition.subtype=kCATransitionFromRight;  
	[adressPage.view.layer addAnimation:transition forKey:nil];  
	[self presentViewController:adressPage animated:NO completion:^{}];  

来自：[http://blog.csdn.net/klyz1314/article/details/8906037](http://blog.csdn.net/klyz1314/article/details/8906037)
title: iOS7应用国际化
date: 2014-02-24 13:35:00
categories:
- iOS应用开发之常用方法
tags:
- ios国际化
- iphone开发
- 国际化
---
一 应用名字国际化:1. 新建一个工程test.
2. 首先修改应用名字
3. test->PROJECT->Info->Localizations->+号->Simplified![](http://img.blog.csdn.net/20140224131250015?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

4. 在(English)添加CFBundleDisplayName="English";
5. 在(Simplified)添加CFBundleDisplayName="中文";![](http://img.blog.csdn.net/20140224131753703?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
6. 启动应用就会按照本地语言加载应用的名称.

二 应用内容国际化1. New File->String Flies,文件名字必须写Localizable,否则iOS识别不了,选择Localizable.strings,右边点击Localize...选择语言.![]()
2. 添加之后右边的Licalization会列出你支持的语言,全部勾上.
3. Localizable.strings会有你选择的两种语言的Strings文件.

4. 在(English)里添加"key" = "English";
5. 在(Simplified)里添加"key" = "中文";![](http://img.blog.csdn.net/20140224132942609?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
6. 添加一个Label就可以看到效果了.![](http://img.blog.csdn.net/20140224133121703?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
7. 当然还有获取当前语言的方法    

```objc
NSArray *languages = [NSLocale preferredLanguages];
    NSString *language = [languages objectAtIndex:0];
    NSLog ( @"%@" , language);
```





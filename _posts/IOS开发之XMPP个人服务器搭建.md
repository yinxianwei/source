title: IOS开发之XMPP个人服务器搭建
date: 2014-01-07 15:25:00
categories:
- iOS应用开发之社交软件
tags:
- ios
- ios开发
- iphone开发
- xmpp
---
本文转自：[http://blog.csdn.net/kangx6/article/details/7739828](http://blog.csdn.net/kangx6/article/details/7739828)
最近看了关于XMPP的框架，以文本聊天为例，需要发送的消息为：

**[html]** [view
 plain](http://blog.csdn.net/kangx6/article/details/7739828# "view plain")[copy](http://blog.csdn.net/kangx6/article/details/7739828# "copy")1. <message type="chat" from="kang@server.com" to="test@server.com">  
2.     <body>helloWord</body>  
3. </message>  


基中from是从哪个用户发送的消息，to是发给谁的消息，XMPP的用户都是以邮箱形式。body就是我们发送的消息文本。
好了，说到这里，我们就来开发一个基于XMPP的IOS聊天客户端程序，首先我们需要XMPP服务器，这里，我就拿本机做服务器，首先从[xmpp Server](http://xmpp.org/xmpp-software/servers/)下载ejabberd这个服务器，ejabberd支持Linux
 / Mac OS X / Solaris / Windows，所以任何操作系统都可以做我们的聊天服务器。好了，下载完后，一步一步安装就可以了，这里我们要注意一下
![](http://my.csdn.net/uploads/201207/12/1342070037_2898.png)

这里我们的服务器就是dpc1338a（一般就是机器名，默认就可以了，不需要改），每台机器的用户名都不一样，这里的服务器域名就是机器名，这个我们需要记住哦
接着一步一步，还要设置管理员密码，密码当然也需要记住了，不然我们没办法登录管理员页面去。
好了，安装完后启动，显示如下：
![](http://my.csdn.net/uploads/201207/12/1342070399_4001.png)
我们点击admin interface，会要求我们输入用户名和密码：
![](http://my.csdn.net/uploads/201207/12/1342070515_1408.png)

这里用户名是前面我们安装的时候有一个管理员名，将管理员名跟我们的服务器组合就可以了，我这里是admin@dpc1338a，每一台机器都不一样，不要照抄哦，这样你是登录不了的，密码就是安装的时候设置的密码
登录成功后就会显示如下页面：
![](http://my.csdn.net/uploads/201207/12/1342070689_8102.png)

这里我们需要解释的就是<访问控制列表>，这里是设置管理员的，我们可以在这里创建其他管理员，这个不是我们的重点，我们的重点是<虚拟主机>
点开<虚拟主机>，下面有一个<dpc1338a>，也点开

![](http://my.csdn.net/uploads/201207/12/1342071704_5702.png)

这里有一个<用户>，我们需要创建几个用户来进行数据交互。
我创建了kang@dpc1338a，test@dpc1338a, abc@dpc1338a这几个用户，过一会我们就用这几个用户进行聊天
![](http://my.csdn.net/uploads/201207/12/1342071856_8163.png)

好了，服务器装好了以后，我们就需要下载个客户端来进行聊天，这里有一些客户端工具
[http://xmpp.org/xmpp-software/clients/](http://xmpp.org/xmpp-software/clients/)，这里我们主要推荐MAC用Adium,Windows用Citron，下一章我们要介绍IOS的xmpp
 framework。

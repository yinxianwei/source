title: iOS开发的一些奇技淫巧二
date: 2014-12-29 16:34:46
description: iOS开发的一些小技巧
categories:
tags: 奇技淫巧
---
#iOS的一些小技巧
本文转自：[http://www.jianshu.com/p/08f194e9904c](http://www.jianshu.com/p/08f194e9904c)转载请注明出处。
##能不能只用一个pan手势来代替UISwipegesture的各个方向?

typedef NS_ENUM(NSUInteger, UIPanGestureRecognizerDirection) {   
	UIPanGestureRecognizerDirectionUndefined,   
    UIPanGestureRecognizerDirectionUp,   
    UIPanGestureRecognizerDirectionDown,   
    UIPanGestureRecognizerDirectionLeft,   
    UIPanGestureRecognizerDirectionRight   
};


		
	- (void)pan:(UIPanGestureRecognizer *)sender
	{

	static UIPanGestureRecognizerDirection direction = UIPanGestureRecognizerDirectionUndefined;
	
	switch (sender.state) {
	
		case UIGestureRecognizerStateBegan: {
	
			if (direction == UIPanGestureRecognizerDirectionUndefined) {
	
			CGPoint velocity = [sender velocityInView:recognizer.view];
	
			BOOL isVerticalGesture = fabs(velocity.y) > fabs(velocity.x);
	
				if (isVerticalGesture) {
					if (velocity.y > 0) {
	                    direction = UIPanGestureRecognizerDirectionDown;
	                } else {
	                    direction = UIPanGestureRecognizerDirectionUp;
	                }
	            }
	
	            else {
	                if (velocity.x > 0) {
	                    direction = UIPanGestureRecognizerDirectionRight;
	                } else {
	                    direction = UIPanGestureRecognizerDirectionLeft;
	                }
	            }
	        }
		break;
	    }
	
	    case UIGestureRecognizerStateChanged: {
	        switch (direction) {
	            case UIPanGestureRecognizerDirectionUp: {
	                [self handleUpwardsGesture:sender];
	                break;
	            }
	            case UIPanGestureRecognizerDirectionDown:{
	                [self handleDownwardsGesture:sender];
	                break;
	            }
	            case UIPanGestureRecognizerDirectionLeft: {
	                [self handleLeftGesture:sender];
	                break;
	            }
	            case UIPanGestureRecognizerDirectionRight: {
	                [self handleRightGesture:sender];
	                break;
	            }
	            default: {
	                break;
	            }
	        }
	        break;
	    }

	    case UIGestureRecognizerStateEnded: {
	        direction = UIPanGestureRecognizerDirectionUndefined;   
	        break;
	    }
	
	    default:
	        break;
	     }
	}    
##拉伸图片的时候怎么才能让图片不变形？
1. UIImage *image = [[UIImage imageNamed:@"xxx"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
(刚才有人提醒这个已经deprecated了哈,现在的方法叫resizableImageWithCapInsets).

2. ![](http://m3.img.srcdd.com/farm5/d/2014/1228/12/647444DE2635CA3F2A9951440C592A2A_ORIG_662_757.gif)

##怎么播放GIF的时候这么卡，有没有好点的库？
FlipBoard出品的太适合你了。[https://github.com/Flipboard/FLAnimatedImage](https://github.com/Flipboard/FLAnimatedImage)
##怎么一句话添加上拉刷新？
[https://github.com/samvermette/SVPullToRefresh](https://github.com/samvermette/SVPullToRefresh)

	[tableView addPullToRefreshWithActionHandler:^{
	// prepend data to dataSource, insert cells at top of table view
	// call [tableView.pullToRefreshView stopAnimating] when done
	} position:SVPullToRefreshPositionBottom];
##怎么把tableview里cell的小对勾的颜色改成别的颜色？
`_mTableView.tintColor = [UIColor redColor];`
![http://m3.img.srcdd.com/farm4/d/2014/1228/12/85E2955C50F62956F9158276B071925C_B1280_1280_754_98.png](http://m3.img.srcdd.com/farm4/d/2014/1228/12/85E2955C50F62956F9158276B071925C_B1280_1280_754_98.png)
##本来我的statusbar是lightcontent的，结果用UIImagePickerController会导致我的statusbar的样式变成黑色，怎么办？
	- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
	{
    	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
   	}

##怎么把我的navigationbar弄成透明的而不是带模糊的效果？
	[self.navigationBar setBackgroundImage:[UIImage new]
                         forBarMetrics:UIBarMetricsDefault];
	self.navigationBar.shadowImage = [UIImage new];
	self.navigationBar.translucent = YES;
##怎么改变uitextfield placeholder的颜色和位置？
继承uitextfield，重写这个方法

	- (void) drawPlaceholderInRect:(CGRect)rect {
    [[UIColor blueColor] setFill];
    [self.placeholder drawInRect:rect withFont:self.font lineBreakMode:UILineBreakModeTailTruncation alignment:self.textAlignment];
}
##你为什么知道这么多奇怪的花招？
去stackoverflow刷问题啊，少年！



title: iPhone之调用系统提示音教程
date: 2013-12-30 16:02:00
categories:
- iOS应用开发之常用方法
tags:
- iphone
- iphone开发
- ios
- ios开发
---
本文转自：[http://www.1000phone.net/thread-8587-1-1.html](http://www.1000phone.net/thread-8587-1-1.html)


首先要在工程里加入Audio Toolbox framework这个库，然后在需要调用的文件里#import <AudioToolbox/AudioToolbox.h>
最后在需要播放提示音的地方写上
AudioServicesPlaySystemSound(1106);
注：括号中为系统声音的id，详见最下面的列表。
为了方便大家测试系统声音，我写了一个demo供大家使用下载。
另外，如果想用自己的音频文件创建系统声音来播放的同学可以参考如下代码。

1. //Get the filename of the sound file:

2. NSString *path = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], @"/jad0007a.wav"];

3. //declare a system sound

4. id SystemSoundID soundID;

5. //Get a URL for the sound file

6. NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];

7. //Use audio sevices to create the sound

8. AudioServicesCreateSystemSoundID((CFURLRef)filePath, &soundID);

9. //Use audio services to play the sound

10. AudioServicesPlaySystemSound(soundID);

复制代码
提示音列表：

Sound ID   [![](http://iphonedevwiki.net/skins/common/images/sort_none.gif)](http://iphonedevwiki.net/index.php/AudioServices#)File name (iPhone)   [![](http://iphonedevwiki.net/skins/common/images/sort_none.gif)](http://iphonedevwiki.net/index.php/AudioServices#)File name (iPod Touch)   [![](http://iphonedevwiki.net/skins/common/images/sort_none.gif)](http://iphonedevwiki.net/index.php/AudioServices#)Category   [![](http://iphonedevwiki.net/skins/common/images/sort_none.gif)](http://iphonedevwiki.net/index.php/AudioServices#)Note   [![](http://iphonedevwiki.net/skins/common/images/sort_none.gif)](http://iphonedevwiki.net/index.php/AudioServices#)1000new-mail.cafnew-mail.cafMailReceived
1001mail-sent.cafmail-sent.cafMailSent
1002Voicemail.cafVoicemail.cafVoicemailReceived
1003ReceivedMessage.cafReceivedMessage.cafSMSReceived
1004SentMessage.cafSentMessage.cafSMSSent
1005alarm.cafsq_alarm.cafCalendarAlert
1006low_power.caflow_power.cafLowPower
1007sms-received1.cafsms-received1.cafSMSReceived_Alert
1008sms-received2.cafsms-received2.cafSMSReceived_Alert
1009sms-received3.cafsms-received3.cafSMSReceived_Alert
1010sms-received4.cafsms-received4.cafSMSReceived_Alert
1011--SMSReceived_Vibrate
1012sms-received1.cafsms-received1.cafSMSReceived_Alert
1013sms-received5.cafsms-received5.cafSMSReceived_Alert
1014sms-received6.cafsms-received6.cafSMSReceived_Alert
1015Voicemail.cafVoicemail.caf-Available since 2.11016tweet_sent.caftweet_sent.cafSMSSentAvailable since 5.01020Anticipate.cafAnticipate.cafSMSReceived_AlertAvailable since 4.21021Bloom.cafBloom.cafSMSReceived_AlertAvailable since 4.21022Calypso.cafCalypso.cafSMSReceived_AlertAvailable since 4.21023Choo_Choo.cafChoo_Choo.cafSMSReceived_AlertAvailable since 4.21024Descent.cafDescent.cafSMSReceived_AlertAvailable since 4.21025Fanfare.cafFanfare.cafSMSReceived_AlertAvailable since 4.21026Ladder.cafLadder.cafSMSReceived_AlertAvailable since 4.21027Minuet.cafMinuet.cafSMSReceived_AlertAvailable since 4.21028News_Flash.cafNews_Flash.cafSMSReceived_AlertAvailable since 4.21029Noir.cafNoir.cafSMSReceived_AlertAvailable since 4.21030Sherwood_Forest.cafSherwood_Forest.cafSMSReceived_AlertAvailable since 4.21031Spell.cafSpell.cafSMSReceived_AlertAvailable since 4.21032Suspense.cafSuspense.cafSMSReceived_AlertAvailable since 4.21033Telegraph.cafTelegraph.cafSMSReceived_AlertAvailable since 4.21034Tiptoes.cafTiptoes.cafSMSReceived_AlertAvailable since 4.21035Typewriters.cafTypewriters.cafSMSReceived_AlertAvailable since 4.21036Update.cafUpdate.cafSMSReceived_AlertAvailable since 4.21050ussd.cafussd.cafUSSDAlert
1051SIMToolkitCallDropped.cafSIMToolkitCallDropped.cafSIMToolkitTone
1052SIMToolkitGeneralBeep.cafSIMToolkitGeneralBeep.cafSIMToolkitTone
1053SIMToolkitNegativeACK.cafSIMToolkitNegativeACK.cafSIMToolkitTone
1054SIMToolkitPositiveACK.cafSIMToolkitPositiveACK.cafSIMToolkitTone
1055SIMToolkitSMS.cafSIMToolkitSMS.cafSIMToolkitTone
1057Tink.cafTink.cafPINKeyPressed
1070ct-busy.cafct-busy.cafAudioToneBusyThere was no category for this sound before 4.0.1071ct-congestion.cafct-congestion.cafAudioToneCongestionThere was no category for this sound before 4.0.1072ct-path-ack.cafct-path-ack.cafAudioTonePathAcknowledgeThere was no category for this sound before 4.0.1073ct-error.cafct-error.cafAudioToneErrorThere was no category for this sound before 4.0.1074ct-call-waiting.cafct-call-waiting.cafAudioToneCallWaitingThere was no category for this sound before 4.0.1075ct-keytone2.cafct-keytone2.cafAudioToneKey2There was no category for this sound before 4.0.1100lock.cafsq_lock.cafScreenLocked
1101unlock.cafsq_lock.cafScreenUnlocked
1102--FailedUnlock
1103Tink.cafsq_tock.cafKeyPressed
1104Tock.cafsq_tock.cafKeyPressed
1105Tock.cafsq_tock.cafKeyPressed
1106beep-beep.cafsq_beep-beep.cafConnectedToPower
1107RingerChanged.cafRingerChanged.cafRingerSwitchIndication
1108photoShutter.cafphotoShutter.cafCameraShutter
1109shake.cafshake.cafShakeToShuffleAvailable since 3.01110jbl_begin.cafjbl_begin.cafJBL_BeginAvailable since 3.01111jbl_confirm.cafjbl_confirm.cafJBL_ConfirmAvailable since 3.01112jbl_cancel.cafjbl_cancel.cafJBL_CancelAvailable since 3.01113begin_record.cafbegin_record.cafBeginRecordingAvailable since 3.01114end_record.cafend_record.cafEndRecordingAvailable since 3.01115jbl_ambiguous.cafjbl_ambiguous.cafJBL_AmbiguousAvailable since 3.01116jbl_no_match.cafjbl_no_match.cafJBL_NoMatchAvailable since 3.01117begin_video_record.cafbegin_video_record.cafBeginVideoRecordingAvailable since 3.01118end_video_record.cafend_video_record.cafEndVideoRecordingAvailable since 3.01150vc~invitation-accepted.cafvc~invitation-accepted.cafVCInvitationAcceptedAvailable since 4.01151vc~ringing.cafvc~ringing.cafVCRingingAvailable since 4.01152vc~ended.cafvc~ended.cafVCEndedAvailable since 4.01153ct-call-waiting.cafct-call-waiting.cafVCCallWaitingAvailable since 4.11154vc~ringing.cafvc~ringing.cafVCCallUpgradeAvailable since 4.11200dtmf-0.cafdtmf-0.cafTouchTone
1201dtmf-1.cafdtmf-1.cafTouchTone
1202dtmf-2.cafdtmf-2.cafTouchTone
1203dtmf-3.cafdtmf-3.cafTouchTone
1204dtmf-4.cafdtmf-4.cafTouchTone
1205dtmf-5.cafdtmf-5.cafTouchTone
1206dtmf-6.cafdtmf-6.cafTouchTone
1207dtmf-7.cafdtmf-7.cafTouchTone
1208dtmf-8.cafdtmf-8.cafTouchTone
1209dtmf-9.cafdtmf-9.cafTouchTone
1210dtmf-star.cafdtmf-star.cafTouchTone
1211dtmf-pound.cafdtmf-pound.cafTouchTone
1254long_low_short_high.caflong_low_short_high.cafHeadset_StartCall
1255short_double_high.cafshort_double_high.cafHeadset_Redial
1256short_low_high.cafshort_low_high.cafHeadset_AnswerCall
1257short_double_low.cafshort_double_low.cafHeadset_EndCall
1258short_double_low.cafshort_double_low.cafHeadset_CallWaitingActions
1259middle_9_short_double_low.cafmiddle_9_short_double_low.cafHeadset_TransitionEnd
1300Voicemail.cafVoicemail.cafSystemSoundPreview
1301ReceivedMessage.cafReceivedMessage.cafSystemSoundPreview
1302new-mail.cafnew-mail.cafSystemSoundPreview
1303mail-sent.cafmail-sent.cafSystemSoundPreview
1304alarm.cafsq_alarm.cafSystemSoundPreview
1305lock.cafsq_lock.cafSystemSoundPreview
1306Tock.cafsq_tock.cafKeyPressClickPreviewThe category was SystemSoundPreview before 3.2.1307sms-received1.cafsms-received1.cafSMSReceived_Selection
1308sms-received2.cafsms-received2.cafSMSReceived_Selection
1309sms-received3.cafsms-received3.cafSMSReceived_Selection
1310sms-received4.cafsms-received4.cafSMSReceived_Selection
1311--SMSReceived_Vibrate
1312sms-received1.cafsms-received1.cafSMSReceived_Selection
1313sms-received5.cafsms-received5.cafSMSReceived_Selection
1314sms-received6.cafsms-received6.cafSMSReceived_Selection
1315Voicemail.cafVoicemail.cafSystemSoundPreviewAvailable since 2.11320Anticipate.cafAnticipate.cafSMSReceived_SelectionAvailable since 4.21321Bloom.cafBloom.cafSMSReceived_SelectionAvailable since 4.21322Calypso.cafCalypso.cafSMSReceived_SelectionAvailable since 4.21323Choo_Choo.cafChoo_Choo.cafSMSReceived_SelectionAvailable since 4.21324Descent.cafDescent.cafSMSReceived_SelectionAvailable since 4.21325Fanfare.cafFanfare.cafSMSReceived_SelectionAvailable since 4.21326Ladder.cafLadder.cafSMSReceived_SelectionAvailable since 4.21327Minuet.cafMinuet.cafSMSReceived_SelectionAvailable since 4.21328News_Flash.cafNews_Flash.cafSMSReceived_SelectionAvailable since 4.21329Noir.cafNoir.cafSMSReceived_SelectionAvailable since 4.21330Sherwood_Forest.cafSherwood_Forest.cafSMSReceived_SelectionAvailable since 4.21331Spell.cafSpell.cafSMSReceived_SelectionAvailable since 4.21332Suspense.cafSuspense.cafSMSReceived_SelectionAvailable since 4.21333Telegraph.cafTelegraph.cafSMSReceived_SelectionAvailable since 4.21334Tiptoes.cafTiptoes.cafSMSReceived_SelectionAvailable since 4.21335Typewriters.cafTypewriters.cafSMSReceived_SelectionAvailable since 4.21336Update.cafUpdate.cafSMSReceived_SelectionAvailable since 4.21350--RingerVibeChanged
1351--SilentVibeChanged

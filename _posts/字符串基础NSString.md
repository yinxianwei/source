title: 字符串基础NSString
date: 2013-09-05 11:41:00
categories:
- iOS应用开发之常用方法
tags:
- ios
- iphone开发
---
7.5

字符转换。字符串拼接
NSString *text=label.text;
float newValue=[text floatValue];
获取label的text值  转换为float 赋值于newValue
=============================================
if ([label.text isEqualToString:@"Error"]) {
        label.text=@"";
    }
判断 label.text是否等价于Error
=============================================
-(IBAction)btnClick:(UIButton *)butt{
//和按钮关联方法，如果有参数 点击按钮的时候会把当前点击按钮对象传递过来
//能且只能传对象    
NSString *title=[butt titleForState:UIControlStateNormal];
//获取这个按钮的title值。
[button setTitle:@"Button" forState:UIControlStateNormal];
//更改按钮的title值。
}
title: 动画和NSTimer
date: 2013-09-05 13:04:00
categories:
- iOS应用开发之动画
tags:
- iphone开发
- ios
---


UIViewController只是容器不能显示，内有view可供调用

【导入头文件】

/Implement 实现
//additional 额外的
//setup   设置
//loading 加载
//typically 典型的

//视图被加载后调用方法
1. UIViewController的执行
- (void)viewDidLoad {
    [super viewDidLoad];
    //这里可以继续设置view的属性，或布局view，譬如背景色等
}

2.view的切换和获得
    //这种方式是生成了一个新的window，不符合要求
    //UIWindow* window = [[UIWindow alloc]init];
    
    //我们要找的是AppDelegate的window，要找到AppDelegate的window需要有一个AppDelegate的对象。
    //要获取AppDelegate的对象，只要获取UIApplication的对象通过打点调用就可以获取AppDelegate的对象
    //通过调用[UIApplication sharedApplication]可以获取UIApplication的对象
    
    //share共享的
    入口类方法名* delegate =[UIApplication sharedApplication].delegate;
    //此时window指向AppDelegate的window  //然后就可以为所欲为了
    UIWindow* window = delegate.window;

    //懒加载（延迟加载视图），生成对象的时候不会把视图加载进来，只有在添加到视图层次体系中时，才真正的把视图加载进来
    // 把即将加进来来的UIViewController初始化
    新的UIViewController* rvc = [[新的UIViewController alloc]init];


3.动画的实现
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:window cache:YES];
    
    //对于基本动画，譬如frame，bounds，center
    
     //alpha这些要放到动画里面  
    
    [UIView commitAnimations];
    
    //对于视图切换的动画，一般放到动画块外面
    //移除当前view    
    [self.view removeFromSuperview];
     //向获得的window添加view
    [window addSubview:rvc.view];

4. // 获取屏幕的大小返回frame
    [[UIScreen mainScreen] bounds];

    //隐藏状态栏
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    //[[UIScreen mainScreen] applicationFrame] 获取不算上状态栏的位置，如果状态栏显示，y坐标从20开始，如果态栏隐藏，y坐标从0开始
    NSLog(@"%@",NSStringFromCGRect([[UIScreen mainScreen] applicationFrame]));
    
    //手动创建一个UIWindow
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    
    //通过xib创建ViewController，init会默认调用initWithNibName
    //NibName默认是和类名称一致，不带后缀
    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    
    //相当与先把原来的remove，然后再addSubView，系统内部帮你做
    self.window.rootViewController = self.viewController;
5.setAnimationCurve
    setAnimationCurve:动画的运动曲线属性

    //UIViewAnimationCurveEaseInOut 动画开始和结束的时候慢，中间快
    //UIViewAnimationCurveEaseIn 动画开始慢，逐渐加速
    //UIViewAnimationCurveEaseOut 开始快，结束时慢
    //UIViewAnimationCurveLinear  匀速




6.计时器
    //不得不提计时器
    [NSTimer scheduledTimerWithTimeInterval:0.15 target:self selector:@selector(执行方法) userInfo:nil repeats:YES];

    //用c的%02d占位符来填充图片名字后缀01，02前的0   
    NSString* strImageName = [NSString stringWithFormat:@"campFire%02d.gif",count];
    UIImage* image = [UIImage imageNamed:strImageName];
    imageView.image = image;
    if(count == 17)
    {
        
        count = 0;
    }

7.九宫格的实现
    //首先宏定义大小
    #define BTN_WIDTH  70
    #define BTN_HEIGHT 70
    -(void)layoutMouse
    {
        //计算得出坐标
        float xSpace = (320-BTN_WIDTH*3)/4;
        float ySpace = (480-BTN_HEIGHT*3)/4;
        
        int count = 1;
        //双层嵌套，先做行，再做列
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) 
            {
                float xPos = (j+1)*xSpace+j*BTN_WIDTH;
                float yPos = (i+1)*ySpace+i*BTN_HEIGHT;
                //
                UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                //button的tag值从1开始
                button.tag = count;
                count++;
                
                button.frame = CGRectMake(xPos, yPos, BTN_WIDTH, BTN_HEIGHT);
                //关联事件hitMouse
                [button addTarget:self action:@selector (hitMouse:)forControlEvents:UIControlEventTouchUpInside];
                
                [self.view addSubview:button];
            }
        }
    }
    
8. UIImageView的动画实现
    //1首先定义一个成员变量UIImageView *view;并初始化获取frame值
    view=[[UIImageView alloc]initWithFrame:self.view.frame];
    //在界面上显示view
    [self.view addSubview:view];
    //定义一个可变数组，初始化为20个存储单位
    NSMutableArray*image=[[NSMutableArray alloc] initWithCapacity:20];
    //用for循环(c%02d占位符)向数组添加UIImage 对象的名字 
    for(int i=1;i<=17;i++)
    {  
        NSString*name=[NSString stringWithFormat:@"campFire%02d.gif",i];
        NSLog(@"%@",name);
        [image addObject:[UIImage imageNamed:name]];
    }
    //数组添加
    view.animationImages=image;
    //每次遍历时间
    view.animationDuration=5;
    //动画遍历次数，0为无限循环
    view.animationRepeatCount=0;
    //动画开始  stopAnimating结束 isAnimating 正在动画
    [view startAnimating];


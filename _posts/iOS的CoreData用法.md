title: iOS的CoreData用法
date: 2014-03-06 13:09:00
categories:
- iOS应用开发之数据库
tags:
- 数据库
- CoreData
- iOS开发
---


CoreData以前没用过，最近试用了一下，感觉省了好多的代码，很方便。当然，也只是会使用。

- 首先来创建一个工程

![](http://img.blog.csdn.net/20140306123438156?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
系统默认会给你增加一些东西，至于这些东西具体干嘛使的，自己去谷歌找找，说了，我只会使用。- 在工程目录下有一个.xcdatamodeld的文件，这就是CoreData的重点了。可以在这里面添加一些Modeld和配置Modeld的属性。


在xcdatamodeld的左下角Add Entity相当于创建一个数据模型也相当于在数据库创建了一张表。![](http://img.blog.csdn.net/20140306123850140?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
现在我们创建一个名Dog的Modeld。同时，这个名字的首字母要大写。- 接下来我们来配置这个Dog的一些属性。点击+号添加属性，Type可以选择属性的类型。

![](http://img.blog.csdn.net/20140306124258921?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
- 然后在Editor里点击CreateNSManagedObject SubClass...选择Dog，然后Create就会多出一个Dog类。


![](http://img.blog.csdn.net/20140306124821421?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)当然，我们完没有必要在意这些东西。 
-  现在就是使用这个东西了，好吧，我承认这一块不太会描述，因为我的数据库学的不太好。

在创建工程的时候，他会自动在AppDelegate里描述一个属性。![](http://img.blog.csdn.net/20140306125154531?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
我们就是使用NSManagedObjectContext来对文件的增删改查的操作。- 首先，我们来插入一条数据。别忘了导入Dog的头文件，虽然这是常识。


创建一个Dog对象。（@(1) == [NSNumbernumberWithInt:1];）
```objc
 Dog *dog = [NSEntityDescription insertNewObjectForEntityForName:@"Dog" inManagedObjectContext:self.managedObjectContext];
    
    [dog setName:@"花花"];
    [dog setAge:@(1)];
    [dog setSex:@(0)];
```
保存
```objc
    NSError *error = nil;
    BOOL isSave =   [self.managedObjectContext save:&error];
    if (!isSave) {
        NSLog(@"error:%@,%@",error,[error userInfo]);
    }
    else{
        NSLog(@"保存成功");
    }
```

- 然后就是查询这个表的数据，描述一个可变数组来保存他们。


```objc
@property (strong, nonatomic) NSMutableArray *dataArray;
```


```objc
  //创建取回数据请求
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    //设置要检索哪种类型的实体对象
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Dog"inManagedObjectContext:self.managedObjectContext];
    //设置请求实体
    [request setEntity:entity];
    //指定对结果的排序方式
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"ascending:NO];
    NSArray *sortDescriptions = [[NSArray alloc]initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptions];
    NSError *error = nil;
    //执行获取数据请求，返回数组
    NSMutableArray *mutableFetchResult = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResult == nil) {
        NSLog(@"Error: %@,%@",error,[error userInfo]);
    }
    self.dataArray = mutableFetchResult;
    for (Dog *dog in self.dataArray) {
        NSLog(@"age:%@---sex:%@---name:%@",dog.age,dog.sex,dog.name);
    }

```

- 现在我们给一个对象改个名字。


```objc
    [dog setName:@"哮天犬"];
    NSError *error;
    BOOL isUpdateSuccess = [self.managedObjectContext save:&error];
    if (!isUpdateSuccess) {
        NSLog(@"error:%@,%@",error,[error userInfo]);
    }
    else{
        NSLog(@"更新成功！");
    }
```

- 最后再删除这条数据


```objc
    [self.managedObjectContext deleteObject:dog];
    [self.dataArray removeObject:dog];
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error:%@,%@",error,[error userInfo]);
    }
    else{
        NSLog(@"删除成功！");
    }
```

现在我就会这么多了，希望对你有所帮助。还有，系统默认是把数据保存到Documents下面。你可以在AppDelegate的这个方法里修改。当然，这不重要。![](http://img.blog.csdn.net/20140306130753031?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)




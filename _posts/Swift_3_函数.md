title: Swift_3_函数
date: 2014-07-21 11:45:00
categories:
- iOS应用开发之Swift
tags:
- Swift
---


```objc
import Foundation

println("Hello, World!")


//声明一个函数 不带参数 没有返回值
func func1(){
    
}

//声明一个函数 传入两个String类型参数 , 没有返回值
func func2(v1:String, v2:String){
    println(v1 + "and" + v2)
}

//声明一个求和的函数
func func3(v1:Int, v2:Int) ->Int{

    return v1+v2;
}


//数组内是否有大于num的数字
func func4(list:[Int], num:Int) ->Bool{
    for s in list{
        if(func5(s,num)){
            return true;}
    }
    return false;
}


//v1 是否大于 v2
func func5(v1:Int, v2:Int) ->Bool{
    if(v1>v2){
        return true;
    }
    return false;
}

//函数嵌套函数
func func6(v1:Int) ->Int{
    func func7(v2:Int) ->Int{
        return v2+1;
    }
    return func7(v1);
}




func1();

func2("1","2");

var v1 = func3(3,4);
println("v1 is \(v1)")


var arr = [11,2,44,58,72,20];

var v2 = func4(arr, 100);

println("v2 is \(v2)");

var v3 = func6(100);

println("v3 is \(v3)");
```


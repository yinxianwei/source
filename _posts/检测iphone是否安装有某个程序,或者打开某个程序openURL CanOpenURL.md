title: 检测iphone是否安装有某个程序,或者打开某个程序openURL CanOpenURL
date: 2014-02-18 13:07:00
categories:
- iOS应用开发之Xcode配置
tags:
- ios开发
---
本文链接:http://blog.csdn.net/zltianhen/article/details/6916579
[问题描述]
1:在程序B中检测手机中是否安装A程序.
2:在程序B中打开应用程序A.

[解决办法]
步骤一:
首先在程序A的plist中添加url types
<key>CFBundleURLTypes</key>
<array>
<dict>
<key>CFBundleURLName</key>
<string>AAAAA</string>
<key>CFBundleURLSchemes</key>
<array>
<string>testA</string>
</array>
</dict>
</array>

步骤二:
在程序B中调用A程序的地方添加代码

NSURL * myURL_APP_A = [NSURL URLWithString:@"testA://AAAAA"];
if ([[UIApplication sharedApplication] canOpenURL:myURL_APP_A])
 {
NSLog(@"canOpenURL");
[[UIApplication sharedApplication] openURL:myURL_APP_A];
}


注:
这里通过canOpenURL就可以通过URL检查机器是否安装有程序A

通过openURL打开程序A


在CFBundleURLSchemes的分隔符不要使用 _    比如 abc_ddd
要使用abc.ddd

title: 通过APPID从appStore获取应用最新信息
date: 2014-03-03 16:37:00
categories:
- iOS应用开发之常用方法
tags:
---

```objc
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    
    int appid = 414478124;
    NSURLRequest *request = [NSURLRequest requestWithURL:URL(@"http://itunes.apple.com/lookup?id=%d",appid)];
    NSURLConnection *Connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    self.receiveData = [NSMutableData data];

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receiveData appendData:data];

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSString *receiveStr = [[NSString alloc]initWithData:self.receiveData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",receiveStr);
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    
}
```


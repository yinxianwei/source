title: stringByEvaluatingJavaScriptFromString的使用方法
date: 2014-02-21 16:29:00
categories:
- iOS应用开发之常用方法
tags:
- iphone开发
- ios开发
- UIWebView
---
　来源：　朱祁林 麒麟的blogstringByEvaluatingJavaScriptFromString使用stringByEvaluatingJavaScriptFromString方法，需要等UIWebView中的页面加载完成之后去调用。我们在界面上拖放一个UIWebView控件。在Load中将googlemobile加载到这个控件中，代码如下：
```objc
- (void)viewDidLoad
 {
 [super viewDidLoad];
 webview.backgroundColor = [UIColor clearColor];
 webview.scalesPageToFit =YES;
 webview.delegate =self;
 NSURL *url =[[NSURL alloc] initWithString:@"http://www.google.com.hk/m?gl=CN&hl=zh_CN&source=ihp"];

 NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
 [webview loadRequest:request];
}
```
我们在webViewDidFinishLoad方法中就可以通过javascript操作界面元素了。1、获取当前页面的url。
```objc
- (void)webViewDidFinishLoad:(UIWebView *)webView {
 NSString *currentURL = [webView stringByEvaluatingJavaScriptFromString:@"document.location.href"];
 }
```
2、获取页面title：
```objc
- (void)webViewDidFinishLoad:(UIWebView *)webView {
 NSString *currentURL = [webView stringByEvaluatingJavaScriptFromString:@"document.location.href"];

 NSString *title = [webview stringByEvaluatingJavaScriptFromString:@"document.title"];
}
```
3、修改界面元素的值。
```objc
NSString *js_result = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByName('q')[0].value='朱祁林';"];
```
4、表单提交：
```objc
NSString *js_result2 = [webView stringByEvaluatingJavaScriptFromString:@"document.forms[0].submit(); "];
```
这样就实现了在google搜索关键字：“朱祁林”的功能。5、插入js代码上面的功能我们可以封装到一个js函数中，将这个函数插入到页面上执行，代码如下：
```objc
[webView stringByEvaluatingJavaScriptFromString:@"var script = document.createElement('script');"
"script.type = 'text/javascript';"
"script.text = \"function myFunction() { "
"var field = document.getElementsByName('q')[0];"
"field.value='朱祁林';"
"document.forms[0].submit();"
"}\";"
"document.getElementsByTagName('head')[0].appendChild(script);"];
 [webView stringByEvaluatingJavaScriptFromString:@"myFunction();"];
```
看上面的代码：a、首先通过js创建一个script的标签，type为'text/javascript'。b、然后在这个标签中插入一段字符串，这段字符串就是一个函数：myFunction，这个函数实现google自动搜索关键字的功能。c、然后使用stringByEvaluatingJavaScriptFromString执行myFunction函数。演示：第一步打开google mobile网站 第二步输入关键字 第三步搜素 总结：这篇文章主要是讲解了stringByEvaluatingJavaScriptFromString的用法，它的功能非常的强大，用起来非常简单，通过它我们可以很方便的操作uiwebview中的页面元素。

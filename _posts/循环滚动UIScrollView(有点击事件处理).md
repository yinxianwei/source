title: 循环滚动UIScrollView(有点击事件处理)
date: 2013-11-25 22:47:00
categories:
- iOS应用开发之第三方框架
tags:
- api
- ios
- iphone开发
---

```objc
1.创建一个继承UIView的名为CycleScrollView的类(.h和.m文件)
2.将下面代码替换原默认代码
3.在你的类里面实例化一个CycleScrollView类的对象并用
   - (id)initWithFrame:(CGRect)frame cycleDirection:(CycleDirection)direction pictures: (NSArray*)pictureArray
进行初始化
参数介绍：
         frame 页面的位置和大小
         direction 滚动的方向，0垂直滚动，1水平滚动
         pictureArray 滚动的图片数组
比如：
        - (void)viewDidLoad
        {
           
            [super viewDidLoad];
           
            NSArray *imagesArray=[[NSArray alloc] initWithObjects: [UIImage imageNamed:@"test1.jpg"],[UIImage imageNamed:@"test2.jpg"],[UIImage imageNamed:@"test3.jpg"],nil];
           
            CycleScrollView *cycle=[[CycleScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 480) cycleDirection:0 pictures:imagesArray];
            cycle.delegate=self;
            [self.view addSubview:cycle];
            [imagesArray release];
            [cycle release];
        }

//////////////////////////////////////////////////////////////////////////////
.h文件

#import <UIKit/UIKit.h>

//枚举,用来判断滚动方向
typedef  enum _CycleDirection
{
    PortaitDirection,//纵向滚动
    LandscapeDirection//横向滚动
} CycleDirection;

@protocol CycleScrollViewDelegate

-(void)pageViewClicked:(NSInteger)pageIndex;

@end

@interface CycleScrollView : UIView <UIScrollViewDelegate>{
   
    CycleDirection scrollDirection;    // scrollView滚动的方向
    UIScrollView *scrollView;
   
    NSArray *imagesArray;   // 存放所有需要滚动的图片
   
    NSMutableArray *curImages; //存放当前滚动的三张图片
   
    int totalPageCount;//图片的总张数
   
    int curPageIndex;//当前图片的索引
   
    CGRect scrollFrame;
   
    id <CycleScrollViewDelegate> delegate;
}

@property (nonatomic,assign)id <CycleScrollViewDelegate> delegate;

- (NSArray*) getDisplayImagesWithPageindex;

- (int) validPageValue:(NSInteger)value;

- (void) refreshScrollView;

- (id)initWithFrame:(CGRect)frame cycleDirection:(CycleDirection)direction pictures:(NSArray*)pictureArray;

-(void)ButtonClicked;

@end

//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
.m文件


#import "CycleScrollView.h"


@implementation CycleScrollView

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame cycleDirection:(CycleDirection)direction pictures:(NSArray*)pictureArray
{
    self = [super initWithFrame:frame];
    if (self) {
       
        scrollFrame=frame;
       
        scrollDirection=direction;
       
        curImages=[[NSMutableArray alloc] init];
        imagesArray=[[NSArray  alloc]initWithArray:pictureArray]; 
       
        totalPageCount=[imagesArray count];
       
        curPageIndex=1;//当前显示的是图片数组里的第一张图片
       
        scrollView=[[UIScrollView alloc] initWithFrame:frame];
        scrollView.backgroundColor=[UIColor blueColor];
        scrollView.showsVerticalScrollIndicator=NO;
        scrollView.showsHorizontalScrollIndicator=NO;
        scrollView.pagingEnabled=YES;
        scrollView.delegate=self;     
       
        if (scrollDirection == PortaitDirection)
        {//在竖直方向滚动
            scrollView.contentSize=CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height*3);
        }
       
        if(scrollDirection==LandscapeDirection)           
        {//在水平方向滚动
           
            scrollView.contentSize=CGSizeMake(scrollView.frame.size.width*3, scrollView.frame.size.height);
           
        }
       
        [self  addSubview:scrollView];
       
        [self refreshScrollView];
    }
    return self;
}

- (void) refreshScrollView
{
    NSArray *subViews=[scrollView subviews];
   
    if ([subViews count]>0) {
        [subViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
   
    [self getDisplayImagesWithPageindex];
   
    for (int i = 0 ; i < 3;  i++) {
        UIButton  *imageButton=[[UIButton alloc] init];
       
        //        UIButton  *imageButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        [imageButton setImage:[curImages objectAtIndex:i] forState:UIControlStateNormal];  
        [imageButton addTarget:self action:@selector(ButtonClicked) forControlEvents:UIControlEventTouchUpInside];
       
        if(scrollDirection==PortaitDirection){
            imageButton.frame=CGRectOffset(CGRectMake(0, 0, 320, 480), 0, scrollFrame.size.height*i);
            [scrollView setContentOffset:CGPointMake(0, scrollFrame.size.height)];
        }
       
        if(scrollDirection==LandscapeDirection) { //水平滚动
            imageButton.frame=CGRectOffset(CGRectMake(0, 0, 320, 480), scrollFrame.size.width*i, 0);          
            [scrollView setContentOffset:CGPointMake(scrollFrame.size.width, 0)];
        }
       
        [scrollView addSubview:imageButton];
        [imageButton release];
    }
   
}

- (NSArray*) getDisplayImagesWithPageindex
{
    int pre=[self validPageValue:curPageIndex-1];
   
    int last=[self validPageValue:curPageIndex+1];
   
    if([curImages count]!=0)
        [curImages removeAllObjects];
   
    [curImages addObject:[imagesArray objectAtIndex:pre-1]];
   
    [curImages addObject:[imagesArray objectAtIndex:curPageIndex-1]];
   
    [curImages addObject:[imagesArray objectAtIndex:last-1]];
   
    return curImages;
}

- (int) validPageValue:(NSInteger)value
{
   
    if(value==0)
    {
        value=totalPageCount;    //value＝1为第一张，value=0为前面一张
    }
   
    if (value==totalPageCount+1) {
        value=1;
    }
   
    return value;
}

-(void)scrollViewDidScroll:(UIScrollView *)crollView
{
    int x=crollView.contentOffset.x;
   
    int y=crollView.contentOffset.y;
   
    if(scrollDirection==LandscapeDirection) //水平滚动
       
    {
       
        if(x>=2*scrollFrame.size.width) //往下翻一张
           
        {
           
            curPageIndex=[self validPageValue:curPageIndex+1];
           
            [self refreshScrollView];
           
        }
       
        if(x<=0)
           
        {
           
            curPageIndex=[self validPageValue:curPageIndex-1];
           
            [self refreshScrollView];
           
        }
       
    }
   
   
   
    //竖直滚动
   
    if(scrollDirection==PortaitDirection){
       
        if(y>=2*scrollFrame.size.height)           
        {           
            curPageIndex=[self validPageValue:curPageIndex+1];
           
            [self refreshScrollView];
           
        }
       
        if (y<=0) {
            curPageIndex=[self validPageValue:curPageIndex-1];           
            [self refreshScrollView];
        }
       
    }
}
-(void)ButtonClicked
{
    [delegate pageViewClicked:curPageIndex];
}
- (void)dealloc
{
    [imagesArray release]
    [curImages release]  
    [super dealloc];
}
@end


```


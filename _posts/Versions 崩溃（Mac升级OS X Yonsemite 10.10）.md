title: Versions 崩溃（Mac升级OS X Yonsemite 10.10）
date: 2014-10-17 17:40:00
---
今天兴冲冲的升级到了OS X Yonsemite 10.10，结果发现SVN工具不能用了，于是找到一个临时的解决办法
1.打开文件夹~/.subversion/servers
![](http://img.blog.csdn.net/20141017173827607?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)
2.在[global] 下添加http-library = serf并保存


![](http://img.blog.csdn.net/20141017173832911?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)
原文链接：[http://help.blackpixel.com/kb/versions/versions-crashing-in-os-x-yosemite-1010](http://help.blackpixel.com/kb/versions/versions-crashing-in-os-x-yosemite-1010)

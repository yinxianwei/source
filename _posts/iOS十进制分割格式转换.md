title: iOS十进制分割格式转换
date: 2014-10-10 16:45:00
categories:
- iOS应用开发之常用方法
tags:
- NSString
---
//@"123456789" 转换后 @"123,456,789"


```objc

@interface NSString (num)


- (NSString *)money;

@end

@implementation NSString (num)

- (NSString *)money{
    
    NSNumberFormatter *numFormat = [[NSNumberFormatter alloc] init];
    [numFormat setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *num = [NSNumber numberWithDouble:[self doubleValue]];
    
    return [numFormat stringFromNumber:num];
}
@end

```


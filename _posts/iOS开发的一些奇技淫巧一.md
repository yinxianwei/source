title: iOS开发的一些奇技淫巧
date: 2014-12-29 16:21:20
description: iOS的一些小技巧
categories: 
tags: 奇技淫巧
---
#iOS的一些小技巧
<br></br>
<br></br>
<br></br>
本文转自：[http://www.jianshu.com/p/50b63a221f09](http://www.jianshu.com/p/50b63a221f09)转载请注明出处。
##TableView不显示没内容的Cell怎么办?
类似这种,我不想让下面那些空的显示.

![](http://m1.img.srcdd.com/farm5/d/2014/1227/11/E3974A9B9936EF6252619A62A8981C3F_B1280_1280_380_692.png)

很简单.


`self.tableView.tableFooterView = [[UIView alloc] init];`

试过的都说好.

加完这句之后就变成了这样.

##自定义了leftBarbuttonItem左滑返回手势失效了怎么办?
	    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                         initWithImage:img
                                         style:UIBarButtonItemStylePlain
                                         target:self
                                         action:@selector(onBack:)];
                                        self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
##ScrollView莫名其妙不能在viewController划到顶怎么办?
	self.automaticallyAdjustsScrollViewInsets = NO;
##键盘事件写的好烦躁,都想摔键盘了,怎么办?
1. 买个结实的键盘.
2. 使用IQKeyboardManager(github上可搜索),用完之后腰也不疼了,腿也不酸了.

##为什么我的app老是不流畅,到底哪里出了问题?
如图

![](http://m3.img.srcdd.com/farm5/d/2014/1227/11/D4EADD8715EF7B9B813F15052FA7E51C_ORIG_385_681.gif)

这个神器叫做:KMCGeigerCounter

快去github搬运吧.

##怎么在不新建一个Cell的情况下调整separaLine的位置?
`_myTableView.separatorInset = UIEdgeInsetsMake(0, 100, 0, 0);`
##怎么点击self.view就让键盘收起,需要添加一个tapGestures么?
	- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	[self.view endEditing:YES];
	}
	
##怎么给每个ViewController设定默认的背景图片?

使用基类啊,少年.

##想在代码里改在xib里添加的layoutAttributes,但是怎么用代码找啊?
像拉button一样的拉你的约束.nslayoutattribute也是可以拉线的.

##怎么像safari一样滑动的时候隐藏navigationbar?
`navigationController.hidesBarsOnSwipe = Yes`
##导航条返回键带的title太讨厌了,怎么让它消失!
	[[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                     forBarMetrics:UIBarMetricsDefault];

##CoreData用起来好烦,语法又臭又长,怎么办?
MagicalRecord

##CollectionView 怎么实现tableview那种悬停的header?
CSStickyHeaderFlowLayout





title: iOS8上方法失效
date: 2014-11-25 14:57:51
description: 之前就发现一些方法在iOS8上会失效，此篇用来收集这些问题。
categories: iOS应用开发之iOS8
tags: iOS8
---
**之前就发现一些方法在iOS8上会失效，此篇用来收集这些问题。**

- 需要更改lab的文字的背景色，有用到NSMutableAttributedString的NSBackgroundColorAttributeName，发现在iOS8上没有效果，后来这样试了一下：

		NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithAttributedString:@"测试文字"];
		[str addAttribute:NSBackgroundColorAttributeName value:[UIColor yellowColor] range:NSMakeRange(0,4)];
		[str addAttribute:NSBackgroundColorAttributeName value:color range:NSMakeRange(0,4)];

	就发现可以了,至于原因，还不知道。

- 自定义cell的时候，子View通过调用superview找不到cell
	假如自定义cell.contentView上有一个lab
	
		iOS8之前
		UILabel *label = (UILabel *)lab.superview.superview.superview;
		iOS8之后
		UILabel *label = (UILabel *)lab.superview.superview;
至于原因，也不知道


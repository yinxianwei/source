title: iOS 向服务器提交Byte数组格式图片
date: 2014-10-09 17:37:00
categories:
- iOS应用开发之网络通讯
tags:
- uiimage
- bytearray
---

```objc
   UIImage *image = [UIImage imageNamed:@"image.png"];
    NSData *data = UIImagePNGRepresentation(image);
    NSString *byteArray = [data base64Encoding];
```


title: ios 地图在viewDidLoad里面获得经纬度
date: 2013-10-21 12:00:00
tags:
- api
- uiviewcontroller
- ios
---


```objc
#import
 
@interface RootViewController : UIViewController<</span>CLLocationManagerDelegate>
{
    NSString * currentLatitude;
    NSString * currentLongitude;
 
CLLocationManager *locManager;
 
}
@end
```




```objc
- (void)viewDidLoad
{
 
locManager =[[CLLocationManager alloc] init];
   locManager.delegate = self;
   locManager.desiredAccuracy= kCLLocationAccuracyBest;
   [locManagerstartUpdatingLocation];
   locManager.distanceFilter= 1000.0f;
   
   currentLatitude =[[NSString alloc]
                   initWithFormat:@"%g",
                   locManager.location.coordinate.latitude];
   currentLongitude =[[NSString alloc]
                    initWithFormat:@"%g",
                    locManager.location.coordinate.longitude];
}
```




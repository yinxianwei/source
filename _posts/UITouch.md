title: UITouch
date: 2013-09-05 13:08:00
categories:
- iOS应用开发之触控
tags:
- ios
- iphone开发
---

    -(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
    
    -(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
    
    -(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
    
    -(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;
    
    当手指接触屏幕时，就会调用touchesBegan:withEvent方法；
    
    当手指在屏幕上移时，动就会调用touchesMoved:withEvent方法；
    
    当手指离开屏幕时，就会调用touchesEnded:withEvent方法；
    
    当触摸被取消（比如触摸过程中被来电打断），就会调用touchesCancelled:withEvent方法。而这几个方法被调用时，正好对应了UITouch类中phase属性的4个枚举值。


    //手指在屏幕上移动时
       //首先定义
    UITouch *touch = [touches anyObject];
    //然后判断是否在view上
    if([self.view pointInside:[touch locationInView:self.view] withEvent:nil])
    {
        //用locationInView:返回触摸点的xy坐标.返回类型为CGPoint 
        CGPoint currentLocation = [touch locationInView:self.view];

        //获取子view的坐标
        CGRect frame = rollView.frame;
        //移动时为中心
        frame.origin.x=currentLocation.x-frame.size.width/2;
        frame.origin.y=currentLocation.y-frame.size.height/2;
        rollView.frame = frame;
    }
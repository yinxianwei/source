title: 隐藏UINavigationController和UITabBarController
date: 2013-11-25 22:51:00
categories:
- iOS应用开发之UI
tags:
- ios开发
---

```objc
隐藏UINavigationController：
比如A页面要隐藏UINavigationController


-(void)viewWillAppear:(BOOL)animated{
     [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];   
}

这两处都要加，如果viewdidload不加的话，会有一个明显的隐藏动画不美观。
如 果B页面需要显示UINavigationController，就需要在B页面的viewDidload或viewWillAppear中加入： [self.navigationController setNavigationBarHidden:NO animated:YES];
---------------------------------------
如果要C页面中隐藏UITabBarController
    CPage *c = [[CPage alloc]init];
    c.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:c animated:YES];

```

 

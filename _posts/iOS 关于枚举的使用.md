title: iOS 关于枚举的使用
date: 2014-04-14 10:57:00
categories:
- iOS应用开发之常用方法
tags:
- iOS开发
---
**引言: 本文转自:[http://blog.csdn.net/ysy441088327/article/details/8012677](http://blog.csdn.net/ysy441088327/article/details/8012677)**
枚举**值** 它是一个**整形(int)  **并且,它不参与内存的占用和释放**,**枚举定义变量即可直接使用,不用初始化.
在代码中使用枚举的目的只有一个,那就是增加代码的可读性.

**使用:**
**枚举**的定义如下:

**[csharp]** [view
 plain](http://blog.csdn.net/ysy441088327/article/details/8012677# "view plain")[copy](http://blog.csdn.net/ysy441088327/article/details/8012677# "copy")1. typedef enum  
2. {  
3.     //以下是枚举成员  
4.     TestA = 0,  
5.     TestB,  
6.     TestC,  
7.     TestD  
8. }Test;//枚举名称  


亦可以如下定义(推荐:结构比较清晰):

**[csharp]** [view
 plain](http://blog.csdn.net/ysy441088327/article/details/8012677# "view plain")[copy](http://blog.csdn.net/ysy441088327/article/details/8012677# "copy")1. typedef NS_ENUM(NSInteger, Test1)  
2. {  
3.     //以下是枚举成员  
4.     Test1A = 0,  
5.     Test1B = 1,  
6.     Test1C = 2,  
7.     Test1D = 3  
8. };  


枚举的定义还支持位运算的方式定义,如下:
等于号后面必须等于1

**[csharp]** [view
 plain](http://blog.csdn.net/ysy441088327/article/details/8012677# "view plain")[copy](http://blog.csdn.net/ysy441088327/article/details/8012677# "copy")1. typedef NS_ENUM(NSInteger, Test)  
2. {  
3.     TestA       = 1,      //1   1   1  
4.     TestB       = 1 << 1, //2   2   10      转换成 10进制  2  
5.     TestC       = 1 << 2, //4   3   100     转换成 10进制  4  
6.     TestD       = 1 << 3, //8   4   1000    转换成 10进制  8  
7.     TestE       = 1 << 4  //16  5   10000   转换成 10进制  16  
8. };  

什么时候要用到这种方式呢?
那就是一个枚举变量可能要代表多个枚举值的时候. 其实给一个枚举变量赋予多个枚举值的时候,原理只是把各个枚举值加起来罢了.
当加起来以后,就获取了一个新的值,那么为了保证这个值的唯一性,这个时候就体现了位运算的重要作用.
位运算可以确保枚举值组合的唯一性.
因为位运算的计算方式是将二进制转换成十进制,也就是说,枚举值里面存取的是 计算后的十进制值.
打个比方:
通过上面的位运算方式设定好枚举以后,打印出来的枚举值分别是: 1 2 4 8 16
这5个数字,无论你如何组合在一起,也不会产生两个同样的数字.
手工的去创建位运算枚举,还有稍微有点花时间的,好在Apple已经为我们准备的**uint**.所以,用下面这种方式来初始化一个位运算枚举吧:

**[csharp]** [view
 plain](http://blog.csdn.net/ysy441088327/article/details/8012677# "view plain")[copy](http://blog.csdn.net/ysy441088327/article/details/8012677# "copy")1. typedef NS_ENUM(uint, Test)  
2. {  
3.     TestA,  
4.     TestB,  
5.     TestC,  
6.     TestD,  
7.     TestE    
8. };  






多枚举值 赋值方式如下:

**[csharp]** [view
 plain](http://blog.csdn.net/ysy441088327/article/details/8012677# "view plain")[copy](http://blog.csdn.net/ysy441088327/article/details/8012677# "copy")1. Test tes = (TestA|TestB);  


判断枚举变量是否包含某个固定的枚举值,使用前需要确保枚举值以及各个组合的唯一性:

**[csharp]** [view
 plain](http://blog.csdn.net/ysy441088327/article/details/8012677# "view plain")[copy](http://blog.csdn.net/ysy441088327/article/details/8012677# "copy")1. NSLog(@"%d %d %d %d %d",TestA,TestB,TestC,TestD,TestE);  
2. Test tes = (TestA|TestB);  
3. NSLog(@"%d",tes);  
4. NSLog(@"%d",(tes & TestA));  
5.   
6. if ((tes & TestA)) {  
7.     NSLog(@"有");  
8. }else  
9. {  
10.     NSLog(@"没有");  
11. }  
12. NSLog(@"%d",(tes & TestB));  
13. if ((tes & TestA)) {  
14.     NSLog(@"有");  
15. }else  
16. {  
17.     NSLog(@"没有");  
18. }  
19.   
20. NSLog(@"%d",(tes & TestC));  
21. if ((tes & TestC)) {  
22.     NSLog(@"有");  
23. }else  
24. {  
25.     NSLog(@"没有");  
26. }  

如果 没有包含,将返回0, 0表示false NO 则进入else


也可以随时为枚举变量累加某个值,但是要自己控制不要添加已经加入过的枚举值, 枚举变量的值不会有变动,但这样将会误导阅读代码的人

**[csharp]** [view
 plain](http://blog.csdn.net/ysy441088327/article/details/8012677# "view plain")[copy](http://blog.csdn.net/ysy441088327/article/details/8012677# "copy")1. tes |=TestC;  



有累加,自然有累减了,如果累减不存在的枚举值, 那么本次累减的枚举值,会自动累加上去.
**[csharp]** [view
 plain](http://blog.csdn.net/ysy441088327/article/details/8012677# "view plain")[copy](http://blog.csdn.net/ysy441088327/article/details/8012677# "copy")1. tes^= TestE;  




以上,差不多就介绍完了, 如果有什么疑问,欢迎提问.
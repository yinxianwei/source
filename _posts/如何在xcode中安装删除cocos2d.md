title: 如何在xcode中安装/删除cocos2d
date: 2013-09-17 16:42:00
categories:
- iOS游戏开发之基础学习
tags:
- iphone开发
- ios
- xcode
---
本文转自:

[](http://blog.163.com/chester_lp/blog/static/139794082012859386718/)
#[Chester的小院](http://blog.163.com/chester_lp/blog/static/139794082012859386718/)

1、如果已经安装过其他版本的cocos2d，想要删除原来已经安装过的模板：
找到路径：/Users/xxxxxx/Library/Developer/Xcode/Templates，xxxxxx是你的名字，把模板中cocos2d的内容删去即可。如果找不到这个路径，是由于文件隐藏所致，可以在终端敲入：defaults write com.apple.finder AppleShowAllFiles -bool true来显示隐藏文件，即可找到对应的文件夹。
2、如果没有安装过或已经删除了之前的cocos2d模板，则在终端进入相应下载目录：如cd /Users/chester/Desktop/cocos2d-iphone-1.0.1/，按Return键返回cocos2d目录，输入以下命令：sudo ./install-templates.sh –f –u
最后附上cocos2d的下载地址:


[Cocos2d](http://code.google.com/p/cocos2d-iphone/downloads/list)





以下转自:[http://blog.sina.com.cn/s/blog_5c5c87d801011qcf.html](http://blog.sina.com.cn/s/blog_5c5c87d801011qcf.html)

本人也是菜菜一个，最近接触cocos2d-iphone，去社区上看到2.0的稳定版出了，果断把1.01给卸载了，在mac下面卸载xcode的模板非常的方便，直接删除模板文件夹就可以了，具体文件位置
/Users/当前用户名/Library/Developer/Xcode/Templates/cocos2d
/Users/当前用户名/Library/Developer/Xcode/Templates/File
 Templates
可以用终端或者直接用finder进去也可以，由于Library是隐藏的，用finder也只有输入路径咯，两种方法都需要当前用户的密码，删除旧的模板之后就可以我们新模板的安装了
好的，接下来继续
使用sudo ./install-templates.sh -f 命令进行安装cocos2d 2.0 模板，总是返回以下信息


cocos2d-iphone template installer  
  
Error: Do not run this script as root.
  
  
'root' is no longer supported  
  
RECOMMENDED WAY:  
 ./install-templates.sh -f 
 
由于2.0版本开始不用root权限，所以sudo没用，但是用 -f 或者 -u 参数都没反应，找了一下教程，发现两个参数一起用也没用，经过研究发现是以下代码对判断执行用户权限上有问题，总是不能认出我是使用root权限
 
 # Make sure root is not executed
  
if [[ "$(id -u)" == "0" ]]; then  
    echo ""  
    echo "Error: Do not run
 this script as root." 1>&2  
    echo ""  
    echo "'root' is no longer
 supported" 1>&2  
    echo ""  
    echo "RECOMMENDED WAY:"
 1>&2  
    echo " $0 -f" 1>&2  
    echo ""  
exit 1  
fi  


所以把这段判决root权限对脚本代码去掉（个人建议用vim删除比较好，如果不太熟悉vim那就老实用文本编辑吧！）
 
然后使用命令 sudo sh ./install-templates.sh -f 进行安装，如果出现输入password的话，就输入password，然后就可以正常安装了！


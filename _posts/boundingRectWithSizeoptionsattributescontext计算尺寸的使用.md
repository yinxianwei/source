title: boundingRectWithSize:options:attributes:context:计算尺寸的使用
date: 2014-03-03 10:57:00
categories:
- iOS应用开发之常用方法
tags:
- cell自适应高度
- ios开发
---
之前使用了NSString类的sizeWithFont:constrainedToSize:lineBreakMode:方法，但是该方法已经被iOS7 Deprecated了，而iOS7新出了一个boudingRectWithSize:options:attributes:context方法来代替。
而具体怎么使用呢，尤其那个attribute


```objc
NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:13]};
CGSize size = [@"相关NSString" boundingRectWithSize:CGSizeMake(100, 0) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
```

本文转自:[http://blog.csdn.net/tongzhitao/article/details/18221973](http://blog.csdn.net/tongzhitao/article/details/18221973)

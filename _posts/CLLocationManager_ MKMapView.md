title: CLLocationManager_ MKMapView
date: 2013-11-25 22:44:00
categories:
- iOS应用开发之地图
tags:
- fontfontios的fontfont
- fontfontiPhone开发fontfont
---

```objc
系统地图
一，用户方位
CoreLocation.framework
＃进口<CoreLocation/CoreLocation.h>
CLLocationManager * _locationManager;
 / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /
 / /创建一个管理器
    _locationManager = [[CLLocationManager的alloc]初始化];
    / /设置代理
    _locationManager.delegate =自我;
    如果（[CLLocationManager headingAvailable]）
    {
        / /指定更新方向的精度
        _locationManager.headingFilter = 5;
        / /开启电子罗盘
        [_locationManager startUpdatingHeading] ;/ /开启方向的更新
    }
/ /之前版本提供的是对象的属性，现在提供的都是静态方法
/ / _locationManager.locationServicesEnabled
    / /判断本地服务是否可用
    若（[CLLocationManager locationServicesEnabled]）{
        
        //发生事件的的最小距离间隔（缺省是不指定）也就是响应位置变化的最小距离(m)
        / / _locationManager.distanceFilter = CLLocationDistanceMax;
        _locationManager.distanceFilter = 1;
        
        / /精度（缺省是最好的）期望要求精度精确度
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        / /开启GPS
        [_locationManager startUpdatingLocation] ;/ /开启方位的更新
    }
的＃pragma标志 -  CLLocationManagerDelegate

/ /方位变化的回调函数
- （无效）locationManager：（CLLocationManager *）经理didUpdateHeading：（CLHeading *）newHeading {
    / /方向度，0度是正北方。
    CLLocationDirection方向= newHeading.magneticHeading;
    
    CLLocationDirection方向2 = newHeading.trueHeading;
    
    的NSString *信息= [NSString的stringWithFormat：@“--->地磁：％.2 F  - >地理：％.2 f”，采用方向，方向2];
    
    _headingLabel.text =信息;
    
    的NSLog（@“--->％.2 F  - >％.2 F”，方向，方向2）;
}

/ / GPS位置变化的回调
/ /定位时候调用
- （无效）locationManager：（CLLocationManager *）经理
     didUpdateLocations：（NSArray的*）的位置
{
    
    CLLocation *位置= [地点lastObject];
    
    / *
    / /取得经纬度
	CLLocationCoordinate2D坐标= location.coordinate;
	CLLocationDegrees北纬= coordinate.latitude;
	CLLocationDegrees经度= coordinate.longitude;
    
	/ /取得精度
	CLLocationAccuracy水平= location.horizo​​ntalAccuracy;
	CLLocationAccuracy垂直= location.verticalAccuracy;
    
	/ /取得高度
	CLLocationDistance高度= location.altitude;
    
	/ /取得时刻
	的NSDate *时间戳= [位置时间戳];
	
	/ /以下面的格式输出格式：<latitude>，<longitude>> + /  -  <accuracy>米@ <date-time>
	的NSLog（@“---->％@”，[位置描述]​​）;
    * /
    
    的NSDate * EVENTDATE = location.timestamp;
    / /获得与当前时间的时间间隔
    NSTimeInterval howRecent = [EVENTDATE timeIntervalSinceNow];
    / /避免处理很久之前的位置
    如果（ABS（howRecent）<15.0）{
        
        的NSLog（@“北纬％+ .6楼东经％+ .6 f \ n”，l​​ocation.coordinate.latitude，location.coordinate.longitude）;
        
        的NSString *信息= [NSString的stringWithFormat：@“（％.6 F，％F）”，location.coordinate.latitude，location.coordinate.longitude];
        _locationLabel.text =信息;
    }
}

/ /全球定位系统初始化失败
- （无效）locationManager：（CLLocationManager *）经理didFailWithError：（NSError *）错误{
    的NSLog（@“的位置管理器错误：％@”，[错误描述]）;
    
    / /关闭定位
    [经理stopUpdatingLocation];
    [经理stopUpdatingHeading];
    / /地图= [[的MKMapView的alloc]的initWithFrame：self.view.bounds];
}
/ / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /

二，系统地图的用户定位
MapKit.framework
＃进口<MapKit/MapKit.h>

静态的MKMapView *地图=零;
/ / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / /
- （无效）addMap
{
    地图= [[的MKMapView的alloc]的initWithFrame：self.view.bounds];
    / / map.delegate =自我;
    / /在地图上显示用户的当前位置
    map.showsUserLocation = YES;
    / /设置用户当前位置的标注
    map.userLocation.title = @“我的位置”;
    map.userLocation.subtitle = @“备注”;
    [self.view addSubview：地图];
    [图释];
    UIButton的*键= [UIButton的buttonWithType：UIButtonTypeRoundedRect];
    button.frame = CGRectMake（220，0，100，30）;
    [按钮的addTarget：自我行动：@选择（buttonClick :) forControlEvents：UIControlEventTouchUpInside];
    [图addSubview：按钮];
}
- （无效）buttonClick：（UIButton的*）对接
{
    
    / /移动地图
    / *
    / /坐标坐标
    CLLocationCoordinate2D mapCenter = map.userLocation.location.coordinate;
    / / convertPoint坐标点的转化把相对于FromView：后参数的这个视图的坐标点转化为图中的经纬度
    mapCenter = [地图convertPoint：
    
                 CGPointMake（0，0）
                 
             toCoordinateFromView：屁股];
    
    / /设置地图的中心点
    [图setCenterCoordinate：mapCenter动画：YES];
    * /
    
    / *
    / / ***** MKCoordinateRegion用来设置坐标显示范围
    / /获得地图当前的显示区域
    MKCoordinateRegion theRegion = map.region;
    / /中心坐标中心
    / /跨度缩放级别
    theRegion.center = map.userLocation.location.coordinate;
    / /修改的是缩放级别
    / /缩放级别越大地图显示的区域范围越广
    theRegion.span.longitudeDelta = 0.001;
    theRegion.span.latitudeDelta = 0.01;
    / /设置地图显示的范围（中心点，缩放级别）
    / /这两个一样不加动画
    / / map.region
    / / [图setRegion：theRegion];
    / /添加了动画
	[图setRegion：theRegion动画：YES];
    * /
    
    / *
    MKCoordinateRegion theRegion = map.region;
    theRegion.center = map.userLocation.location.coordinate;
    //创建一个以center为中心，上下各1000米，左右各1000米的区域，但其是一个矩形，不符合MapView的横纵比例
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance（theRegion.center，500，500）;
    / /创建出来一个符合MapView的横纵比例的区域
    MKCoordinateRegion adjustedRegion = [地图regionThatFits：viewRegion];
    [图setRegion：adjustedRegion动画：YES];
    * /
    
    / *
    CLLocationCoordinate2D位置= {34.728428,113.75217};
	MKCoordinateSpan跨度= {0.01,0.01};
	MKCoordinateRegion区= {位置，跨度};
	
    / /为了让地图成正比
	MKCoordinateRegion adjustRegion = [地图regionThatFits：区域];
	[图setRegion：adjustRegion动画：YES];
     * /
    
    / / / *
    MKCoordinateRegion theRegion;
    / /获得当前用户的纬度
    theRegion.center.latitude = map.userLocation.location.coordinate.latitude;
    / /获得当前用户的经度
    theRegion.center.longitude = map.userLocation.location.coordinate.longitude;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance（theRegion.center，500，500）;
    MKCoordinateRegion adjustedRegion = [地图regionThatFits：viewRegion];
    [图setRegion：adjustedRegion动画：YES];
    / / * /
}

```


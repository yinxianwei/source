title: IOS开发-在应用内打开AppStore
date: 2014-02-13 13:54:00
categories:
- iOS应用开发之常用方法
tags:
- 打开AppStore
---

1. 首先需要添加StoreKit.framework

2. 头文件导入<StoreKit/StoreKit.h>
3. 弹出界面


```objc
SKStoreProductViewController *storeProductViewController = [[SKStoreProductViewController alloc] init];
// Configure View Controller
[storeProductViewController setDelegate:self];
[storeProductViewController loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier :@"414478124"} completionBlock:nil];
[self presentViewController:storeProductViewController animated:YES completion:nil];
```



4. 设置代理 
```objc
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
```






title: ios基础
date: 2013-09-05 11:37:00
categories:
- iOS应用开发之常用方法
tags:
- ios
- iphone开发
---

People.h
=======


  类==>对象  创建一个People类
声明属性
NSString *name;
int age;
……
基本数据类型定义不用 *
基本数据类型:
BOOL默认值是0（NO） （YES，NO）
Byte char BOOL （1）
short(2) int(4) long(4)  long long(8)
float(4) double(8)
 NSString属于非基本类型 需加*

-(void)setName:(参数类型)参数名;//表示对象调用的方法
-(void)setName:(NSString *)aName;
-(NSString *)getName;

-(int)num1:(int)n1 num2:(int)n2;
第一个参数直接跟在方法名之后，从第二个参数起要给每一个参数添加一个别名
+(void)eating;
+表示类方法,用类名调用

People.m
======================
-(void)setName:(NSString *)aName|
{                               |
    name=aName;                 |
}                               |get set同时存在  表示赋值=>输出
-(NSString *)getName            |         
{                               |
    return name;                |
}                               |
======================
-(int)num1:(int)n1 num2:(int)n2
{
    return n1 * n2;    
}
返回n1和n2的乘积
======================
NSLog(@"");控制台输出语句.
======================
-(void)working
{
    NSLog(@"%@正在工作ing...", name);
}

+(void)eating{
    NSLog(@"eating");
    
    //self表示的不是对象  而是类本身
    
    People *people=[self alloc];
    [people setName:@"加号"];
    [people working];
}
============second===========
#import "People.h"
导入头文件
People *p1=[People alloc]  分配内存地址并返回内存地址
[p1 setName:@"张三"];       赋值
[p1 working];              调用方法

======================
[People eating];

输出: eating
    加号 正在工作ing...  


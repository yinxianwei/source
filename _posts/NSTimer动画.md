title: NSTimer动画
date: 2013-09-05 13:04:00
categories:
- iOS应用开发之动画
tags:
- iphone开发
- ios
---

h.雪花飘落动画制作

[[UIApplication sharedApplication] setStatusBarHidden:YES];//移除顶部任务栏

1.给window一个背景
    self.window.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
2.添加一个雪花
UIImageView* imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"flake.png"]];
    imgView.frame = CGRectMake(xStartPosition, -SNOW_HEIGHT, SNOW_WIDTH, SNOW_HEIGHT);
    
    //先把雪花的frame 获取
    CGRect rect = imgView.frame;
    
    [self.window addSubview:imgView];
3.用计时器引发雪花飘落事件
[NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
    
4.产生随机数给雪花坐标赋值
int xStartPosition = arc4random()%(321-SNOW_WIDTH);
    //NSLog(@"xPos is %d",xStartPos);
    int xStopPosition = arc4random()%(321-SNOW_WIDTH);

5.落下消失用动画默认调用函数获取产生的对象
    UIImageView* imgView = (UIImageView*)context;
    
    //把imgView 从父视图上去除
6.移除对象
    [imgView removeFromSuperview];
======================================================================================================================================================================================
7.雪花飘落的实现：

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.
    
    [self.window makeKeyAndVisible];
    //去掉window的顶部任务栏
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    //用图片填充window的背景 colorWithPatternImage:
    window.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(flakeShow) userInfo:nil repeats:YES];
    return YES;
}
-(void)flakeShow
{
    int x=arc4random()%321;//随机一个雪花的x初始值
    int xEnd=arc4random()%521;//随机一个雪花结束的x值
    int h=arc4random()%16+10;//随机一个雪花长度，因为雪花为正方形，所以都要用h
    //初始化一个雪花
    UIImageView *flake=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"flake"]];
    //雪花大小位置取值
    flake.frame=CGRectMake(x, -30, h, h);
    //保存雪花的地址
    CGRect flakeWz=flake.frame;
    [self.window addSubview:flake];
    
    //开始动画，beginAnimations  动画名字
    //context:环境参数，当设置这个值的时候，会在动画结束函数中再次回送
    [UIView beginAnimations:@"down" context:flake];
    [UIView setAnimationDuration:4];
    [UIView setAnimationDelegate:self];
    //动画结束坐标
    flakeWz.origin.y=500;
    flakeWz.origin.x=xEnd-100;
    //把坐标重新赋值frame
    flake.frame=flakeWz;
    [UIView commitAnimations];
    
}
-(void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    //context会保存一个参数，强制转换为UIImageView类型获取动画的对象
    UIImageView *flake=(UIImageView *)context;
    //判断动画名字
    if ([animationID isEqualToString:@"down"]) {
        //创建一个动画 名字为up
        [UIView beginAnimations:@"up" context:flake];
        [UIView setAnimationDuration:1];
        [UIView setAnimationDelegate:self];
        //透明度 alpha
        flake.alpha=0;
        
        [UIView commitAnimations];
    }
    if ([animationID isEqualToString:@"up"]) {
        //移除flake对象
        [flake removeFromSuperview];
    }
}

======================================================================================================================================================================================
8).数组NSArray
    //NSArray 不可变数组
    NSArray* array = [[NSArray alloc] initWithObjects:@"A",@"B",@"C",nil];
    
    //从0 到 99 执行100次
    for (int x= 0; x < 100; x++) {
        NSLog(@"aaaa");
    }
    //普通遍历
    for(int index = 0; index < [array count];index++)
    {
        NSString* str = [array objectAtIndex:index];
            //获取数组第index个对象
        NSLog(@"%@",str);
    }

   
    
    NSLog(@"泛型遍历开始");
    //泛型遍历
    for (NSString* str in array) {
        NSLog(@"%@",str);
    }
    
    //可变数组
    //initWithCapacity 使用一个容量来初始化可变数组
    NSMutableArray* mutableArray = [[NSMutableArray alloc]initWithCapacity:0];
    for(int i = 0;i < 10; i++)
    {
        //addObject只能添加对象，所以把i转成NSNumber对象
        [mutableArray addObject:[[NSNumber alloc]initWithInt:i]];
    }
    NSLog(@"可变数组开始泛型遍历");
    for (NSNumber* number in mutableArray) {
        //把NSNumber对象转成int
        int x = [number intValue];
        NSLog(@"%d",x);
    }


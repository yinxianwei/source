title: Swift_2_控制语句
date: 2014-07-21 11:41:00
categories:
- iOS应用开发之Swift
tags:
- Swift
---


```objc
import Foundation

println("Hello, World!")

/*
for  do While  While Switch
*/

var arr = [1,2,3,4,5,7];

for i in arr{
    println(" i is \(i)");
}

var count = arr.count;

for (var i = 0 ; i<count; i++){
    var index = arr[i];
    println("index is \(index)");
}

var index = 0;

do{
    var j = arr[index];
    println("j is \(j)");
}while(++index < count);


var x = 0;
while(x<count){
    println("arr num is \(arr[x]) index = \(x++)");
};

var type = "iOS"

///没有break默认不执行下面语句, 如果想要执行(穿透)就加上fallthrough
switch type{
    case "iOS":
        println("------")
//        fallthrough;
    case "22":
        println("=======")
    default:
        println("三三三三三")
}
```


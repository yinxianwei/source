title: View旋转动画
date: 2013-09-05 13:06:00
categories:
- iOS应用开发之动画
tags:
- iphone开发
- ios
---
首先导入头文件#import <QuartzCore/QuartzCore.h>
layer
动画基于layer进行
旋转动画和复杂动画主要在layer实现

//重置view的位置
View.transform=CGAffineTransformIdentity;

//View的旋转中心点   从左上角(0, 0)到右下角(1.0, 1.0)
View.layer.anchorPoint=CGPointMake(1.0, 1.0);

//旋转默认为顺时针旋转，角度为弧度，(角度*M_PI/180)得到旋转角度
View.transform=CGAffineTransformMakeRotation(-60*M_PI/180);

//动画实现
[UIView beginAnimations:@"score" context:(void*)imageView];
//动画返回名字为animationDidStop:score  对象为 imageView
            [UIView setAnimationDuration:1];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
//系统默认动画结束后调用animationDidStop:finished:context:方法。
            [UIView commitAnimations];
-(void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{

    //传回context 为动画对象 animationID 为动画名字
}


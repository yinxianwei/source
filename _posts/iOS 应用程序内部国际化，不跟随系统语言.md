title: iOS 应用程序内部国际化，不跟随系统语言
date: 2014-02-27 10:12:00
categories:
- iOS应用开发之常用方法
tags:
- iOS国际化
---
前言：网络上关于iOS国际化的文章很多，但基本上都是基于跟随系统语言的国际化，笔者就不赘述了-0 –
今天要讲的是不跟随系统的切换语言版本方案，即程序内部的切换语言版本方案。


**一、总则：**
       应用内部语言不跟随系统语言，由应用自己进行控制，通过配置多个语言文件，根据用户的选择动态获取不同文件夹下的语言文件，显示在界面上。
       最后把用户选择的语言持久化到本地，下次运行时读取。


**二、应用内的国际化**
**1、新建工程**
      新建Single View Application工程，取名为MyInternational。
                                                      ![](http://img.blog.csdn.net/20130925230149234?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWFuZzg0NTYyMTE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

**2、添加多语言文件**
      我们选择使用自己的语言文件，而不是系统的localizable.string，因为在项目开发使用svn时，多人操作同一文件可能会造成冲突。
在Resource中添加新的Strings File：
                                                            ![](http://img.blog.csdn.net/20130925230247031?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWFuZzg0NTYyMTE=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

       
      取名为hello（记得Targets要勾选项目）
                                                                 ![](http://img.blog.csdn.net/20130925230328453)

      新建完成之后会生成hello.string文件，把它拖进Supporting Files里。


**3、配置hello.string国际化**
      首先配置刚才的hello.string文件。
      点击hello.strings文件，点击圈中的Make localized（本地化）。
                                                                             ![](http://img.blog.csdn.net/20130925230418656)

    
      只有English，没关系点击确定。
                                                                ![](http://img.blog.csdn.net/20130925230550453)

**4、配置项目国际化**
      网络上很多配置方法不靠谱，因为新版的Xcode位置“+”号的位置换了，这里教大家正确的方法，点击项目—>选择Project—>选择Info—>点击“+”号。
                                                     ![](http://img.blog.csdn.net/20130925230623281)


      点击“+”号后，添加中文，当然也可以添加其他的语言，弹出对话框，infoPlist是配置系统的国际化文件，可以配置图标之类的多语言环境，不属于本文范围，nib文件看需要配，这里选择不配。
                                                      ![](http://img.blog.csdn.net/20130925230628156)
      
      配置完成之后，再看hello.string便分成了两个文件。
                                                                                 ![](http://img.blog.csdn.net/20130925230701593)
      完成上面的四步便完成了项目内的多语言环境配置，下面来看看代码怎么写。

**5、添加需要的字符串**
      分别在hello.strings（English）与hello.strings（Chinese）里面添加对应语言的字符串。
      字符串的格式为“key”=“value”;键值对（别忘记分号！）
      接下来配置项目需要的字符串，一个用于lable的显示，一个用于button的显示。
 
    hello.strings（English）
    "invite" ="hello atany,come to join in us!";
   "buttonInfo" ="change Language";
 
    hello.strings（Chinese）
    "invite" ="你好 atany,快来加入我们吧！";
    "buttonInfo" ="切换语言";
 
**6、看看流程**
                                                     ![](http://img.blog.csdn.net/20130925230801343)

【实现思路:】
      在初始化首个Controller的时候，进行应用语言的加载。首次加载因为userLanguage（应用内语言）不存在，则使用系统当前的语言，保存到userlanguage，下次直接读。读出来之后，获取到对应文件路径，得到文件的索引，储存到工具类InternationalControl的静态变量bundle中。
其他地方需要用字符串时，用工具类获得bundle即可读取相应文件中的字符串。

**7、新建工具类InternationalControl**
                           ![](http://img.blog.csdn.net/20130925230849375)

**InternationalControl.h**
**[cpp]** [view
 plain](http://blog.csdn.net/yang8456211/article/details/12031667# "view plain")[copy](http://blog.csdn.net/yang8456211/article/details/12031667# "copy")1. #import <Foundation/Foundation.h>  
2.   
3. #import <UIKit/UIKit.h>  
4.   
5. @interface InternationalControl : NSObject  
6.   
7. +(NSBundle *)bundle;//获取当前资源文件  
8.   
9. +(void)initUserLanguage;//初始化语言文件  
10.   
11. +(NSString *)userLanguage;//获取应用当前语言  
12.   
13. +(void)setUserlanguage:(NSString *)language;//设置当前语言  
14.   
15. @end  


**InternationalControl.m**
1）创建静态变量bundle，以及获取方法bundle（注：此处不要使用getBundle）。

**[cpp]** [view
 plain](http://blog.csdn.net/yang8456211/article/details/12031667# "view plain")[copy](http://blog.csdn.net/yang8456211/article/details/12031667# "copy")1. static NSBundle *bundle = nil;  
2.   
3. + ( NSBundle * )bundle{  
4.       
5.     return bundle;  
6.   
7. }  



2）初始化方法：
 userLanguage储存在NSUserDefaults中，首次加载时要检测是否存在，如果不存在的话读AppleLanguages，并赋值给userLanguage。
**[cpp]** [view
 plain](http://blog.csdn.net/yang8456211/article/details/12031667# "view plain")[copy](http://blog.csdn.net/yang8456211/article/details/12031667# "copy")1. +(void)initUserLanguage{  
2.       
3.     NSUserDefaults *def = [NSUserDefaults standardUserDefaults];  
4.       
5.     NSString *string = [def valueForKey:@"userLanguage"];  
6.        
7.     if(string.length == 0){  
8.           
9.         //获取系统当前语言版本(中文zh-Hans,英文en)  
10.   
11.         NSArray* languages = [def objectForKey:@"AppleLanguages"];  
12.           
13. <pre class="cpp" name="code">        NSString *current = [languages objectAtIndex:0];  
14.           
15.         string = current;  
16.           
17.         [def setValue:current forKey:@"userLanguage"];  
18.           
19.         [def synchronize];//持久化，不加的话不会保存  
20. }  
21.   
22.     //获取文件路径  
23.     NSString *path = [[NSBundle mainBundle] pathForResource:string ofType:@"lproj"];  
24.       
25.     bundle = [NSBundle bundleWithPath:path];//生成bundle  
26. }  
27. </pre>  


3）获得当前语言方法
**[cpp]** [view
 plain](http://blog.csdn.net/yang8456211/article/details/12031667# "view plain")[copy](http://blog.csdn.net/yang8456211/article/details/12031667# "copy")1. +(NSString *)userLanguage{  
2.       
3.     NSUserDefaults *def = [NSUserDefaults standardUserDefaults];  
4.       
5.     NSString *language = [def valueForKey:@"userLanguage"];  
6.       
7.     return language;  
8. }  


4）设置语言方法
**[cpp]** [view
 plain](http://blog.csdn.net/yang8456211/article/details/12031667# "view plain")[copy](http://blog.csdn.net/yang8456211/article/details/12031667# "copy")1. +(void)setUserlanguage:(NSString *)language{  
2.       
3.     NSUserDefaults *def = [NSUserDefaults standardUserDefaults];  
4.       
5.     //1.第一步改变bundle的值  
6.     NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj" ];  
7.       
8.     bundle = [NSBundle bundleWithPath:path];  
9.       
10.     //2.持久化  
11.     [def setValue:language forKey:@"userLanguage"];  
12.       
13.     [def synchronize];  
14. }  


**8、拖nib，配置点击事件**
一个button，一个label，button用于切换语言，label用于显示信息。
                                                                        ![](http://img.blog.csdn.net/20130925231125046)

**9、配置加载的第一个Controller，这里是YGViewController**
**YGViewController.h**
**[cpp]** [view
 plain](http://blog.csdn.net/yang8456211/article/details/12031667# "view plain")[copy](http://blog.csdn.net/yang8456211/article/details/12031667# "copy")1. #import <UIKit/UIKit.h>  
2.   
3. @interface YGViewController : UIViewController  
4.   
5. @property (retain, nonatomic) IBOutlet UILabel *inviteLabel;//label  
6.   
7. - (IBAction)changeLanguage:(id)sender;//点击事件  
8.   
9. @property (retain, nonatomic) IBOutlet UIButton *btChange;//button  
10.   
11. @end  

**
**
**YGViewController.m**
1）加载：
**[cpp]** [view
 plain](http://blog.csdn.net/yang8456211/article/details/12031667# "view plain")[copy](http://blog.csdn.net/yang8456211/article/details/12031667# "copy")1. - (void)viewDidLoad  
2. {  
3.     //注册通知，用于接收改变语言的通知  
4.     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage) name:@"changeLanguage" object:nil];  
5.   
6.     [InternationalControl initUserLanguage];//初始化应用语言  
7.       
8.     NSBundle *bundle = [InternationalControl bundle];  
9.       
10.     NSString *inviteMsg = [bundle localizedStringForKey:@"invite" value:nil table:@"hello"];  
11.   
12.     NSString *buttonInfo = [bundle localizedStringForKey:@"buttonInfo" value:nil table:@"hello"];//table为hello.string的文件名  
13.       
14.     [_btChange setTitle:buttonInfo forState:UIControlStateNormal];  
15.       
16.     _inviteLabel.text = inviteMsg;  
17.       
18.     [super viewDidLoad];  
19. }  


2）点击修改语言方法
**[cpp]** [view
 plain](http://blog.csdn.net/yang8456211/article/details/12031667# "view plain")[copy](http://blog.csdn.net/yang8456211/article/details/12031667# "copy")1. - (IBAction)changeLanguage:(id)sender {  
2.       
3.     NSString *lan = [InternationalControl userLanguage];  
4.       
5.     if([lan isEqualToString:@"en"]){//判断当前的语言，进行改变  
6.       
7.         [InternationalControl setUserlanguage:@"zh-Hans"];  
8.       
9.     }else{  
10.       
11.         [InternationalControl setUserlanguage:@"en"];  
12. }  
13.       
14.     //改变完成之后发送通知，告诉其他页面修改完成，提示刷新界面  
15.     [[NSNotificationCenter defaultCenter] postNotificationName:@"changeLanguage" object:nil];  
16. }  


3）接收到通知执行方法，刷新界面
**[cpp]** [view
 plain](http://blog.csdn.net/yang8456211/article/details/12031667# "view plain")[copy](http://blog.csdn.net/yang8456211/article/details/12031667# "copy")1. -(void)changeLanguage{  
2.       
3.     [_btChange setTitle:[[InternationalControl bundle] localizedStringForKey:@"buttonInfo" value:nil table:@"hello"] forState:UIControlStateNormal];  
4.       
5.     _inviteLabel.text =[[InternationalControl bundle] localizedStringForKey:@"invite" value:nil table:@"hello"];  
6. }  


如果有多个界面的话，需要每个界面都配置接收通知，用于修改界面。

**10、运行界面**
                                                                             ![](http://img.blog.csdn.net/20130925231323250)

                                                                             ![](http://img.blog.csdn.net/20130925231423796)

可以看到点击切换语言之后，语言切换成功。
退出程序再次进入，保留了上次选择的语言。
 
atany原创，转载请注明博主与博文链接,3Q![吐舌头](http://static.blog.csdn.net/xheditor/xheditor_emot/default/tongue.gif)
 [http://blog.csdn.net/yang8456211/article/details/12031667](http://blog.csdn.net/yang8456211/article/details/12031667)[
](http://blog.csdn.net/yang8456211/article/details/11783823)
 —— by atany

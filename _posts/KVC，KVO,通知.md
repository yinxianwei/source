title: KVC，KVO,通知
date: 2013-09-07 08:43:00
categories:
- iOS应用开发之常用方法
tags:
- ios
- iphone开发
---
KVC，KVO,通知

    
    1、KVC
    KVC(KeyValueCoding)　“键-值-编码”是一种可以直接通过字符串的名字（key）来访问类实例变量的机制，是通过setter、getter方法访问。
    属性的访问和设置
    KVC可以用来访问和设置实例变量的值。key是属性名称
    设置方式：[self setValue:aName forKey:@"name"]
    等同于　self.name = aName;
    访问方式： aString　=　[self valueForKey:@"name"]
    等同于　aString = self.name;
    2、KVO 观察者
    KVO(KeyValueObserver)　“键-值-监听”定义了这样一种机制，当对象的属性值发生变化的时候，我们能收到一个“通知”。观察者更准确
    NSObject提供了监听机制。所有子类也就全都能进行监听
    KVO是基于KVC来实现的。
    实现监听步骤
    （1）注册监听对象。anObserver指监听者，keyPath就是要监听的属性值，而context方便传输你需要的数据，它是个指针类型。
    -(void)addObserver:(NSObject *)anObserver
    　　　　forKeyPath:(NSString *)keyPath
    　　　　　　options:(NSKeyValueObservingOptions)options            
    　　　　　　context:(void *)context//（void*）是任何指针类型
    其中， options是监听的选项，也就是说明监听返回的字典包含什么值。有两个常用的选项：
    NSKeyValueObservingOptionNew　指返回的字典包含新值。
    NSKeyValueObservingOptionOld    指返回的字典包含旧值。
    （2）实现监听方法。监听方法在Value（属性的值）发生变化的时候自动调用。
    -(void) observeValueForKeyPath:(NSString *)keyPath
                          ofObject:(id)object
                            change:(NSDictionary *)change
                           context:(void *)context
    其中，object指被监听的对象。change里存储了一些变化的数据，比如变化前的数据，变化后的数据。
    3、通知
    通知是iOS开发框架中的一种设计模式，内部的实现机制由Cocoa框架支持。
    通知一般用于M、V、C的间的信息传递。像我在设置页面设置App皮肤。
    M是modol模型 V是view视图 C是control控制器。
    系统通知
    //注册通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinish:) //didFinish:是方法名   self （谁的）和  didFinish:确定方法
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
     　object:nil];
    selector是方法名     class是描述类的类    SEL method=@selector（方法名）
    通知用完要移除
    //移除通知
    [[NSNotificationCenter defaultCenter] removeObserver:self
               name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:nil];




    1.vc 就是一种通过字符串去间接操作对象属性的机制，  
    访问一个对象属性我们可以 person.age  也可以通过kvc的方式   [person valueForKey:@"age"]
    
    keypath 就是属性链式访问  如 person.address.street  有点象java里面的pojo  ognl表达式子类的
    
    假如给出的字符串没有对象的属性 会访问valueForUndefineKey方法 默认实现是raise 一个异常 但你可以重写这个方法,   setValue的时候也是一样的道理
    
    kvo 就是一个在语言框架层面实现的观察者模式 通过kvc的方式修改属性时，会主动通知观察者
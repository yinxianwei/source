title: iOS8定位
date: 2014-10-14 10:02:00
categories:
- iOS应用开发之地图
tags:
- iOS8定位
---

```objc
CLLocationManager  *_locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest; //add by zhangzhenqiang
    _locationManager.activityType = CLActivityTypeFitness;
    _locationManager.distanceFilter = 1000.0;
    _locationManager.pausesLocationUpdatesAutomatically = YES;
    if (CurrentSystemVersion >= 8.0)
    {
        [appDelegate.locationManager requestAlwaysAuthorization];
        [appDelegate.locationManager startUpdatingLocation];
    }
    else
    {
        [appDelegate.locationManager startUpdatingLocation];
    }
```

```objc
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    
    switch (status)
    {  case kCLAuthorizationStatusNotDetermined:
        if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        {
            [_locationManager requestWhenInUseAuthorization];
        }
        break;
        default:
        break;
    }
}
```

![](http://img.blog.csdn.net/20141014100229799?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

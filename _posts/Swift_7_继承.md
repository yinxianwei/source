title: Swift_7_继承
date: 2014-07-21 15:44:00
categories:
- iOS应用开发之Swift
tags:
- Swift
---


```objc
import Foundation

println("Hello, World!")


class People {
    
    var name:String?
    
    init(){
        self.name="李四"
    }
    
    //防止重写关键字 @final
    @final var age = 15
}

class Man : People{
    init(){
        super.init()
    }
}

let p = Man()

p.name = "张三"

println("p name is \(p.name)")

let p2 = Man()

println("p2 name is \(p2.name)")
```


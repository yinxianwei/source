title: iOS开发的一些奇巧淫技三
date: 2015-02-11 23:02:36
categories: 	
tags:
---
#iOS的一些小技巧
本文转自：[http://www.jianshu.com/p/f547eb0368c4](http://www.jianshu.com/p/f547eb0368c4)转载请注明出处。

##CGfloat和float的区别?
现在上架的app都要求支持64位系统,那么CGFloat和float的区别就在这里.command+左键点击CGFloat.
	
	typedef CGFLOAT_TYPE CGFloat;

这里可以看到CGFloat是CGFLOAT_TYPE的宏定义,那么这个又是什么?

	#if defined(__LP64__) && __LP64__
	# define CGFLOAT_TYPE double
	# define CGFLOAT_IS_DOUBLE 1
	# define CGFLOAT_MIN DBL_MIN
	# define CGFLOAT_MAX DBL_MAX
	#else
	# define CGFLOAT_TYPE float
	# define CGFLOAT_IS_DOUBLE 0
	# define CGFLOAT_MIN FLT_MIN
	# define CGFLOAT_MAX FLT_MAX
	#endif
	
这段话的意思就是,64位系统下,CGFLOAT是double类型,32位系统下是float类型.CGFloat能够保证你的代码在64位系统下也不容易出错,所以你的代码应该尽量使用CGFloat.尽管他可能造成一些多余的消耗.不过能保证安全.
<!-- more -->
##应该使用FOUNDATION_EXPORT还是#define来定义常量?


一般iOS我们定义常量的方法有两种,来看下面例子

我的.h文件
	
	FOUNDATION_EXPORT NSString * const kMyConstantString;  
	FOUNDATION_EXPORT NSString * const kMyConstantString2;
	
.m文件是这样定义的

	NSString * const kMyConstantString = @"Hello";
	NSString * const kMyConstantString2 = @"World";
	
还有一种是常用的#define方法了

\#define kMyConstantString @"Hello"

有什么区别呢?
使用第一种方法在检测字符串的值是否相等的时候更快.对于第一种你可以直接使用`(stringInstance == MyFirstConstant)`来比较,而define则使用的是这种.`([stringInstance isEqualToString:MyFirstConstant])`
哪个效率高,显而易见了.第一种直接比较的是指针地址,而第二个则是一一比较字符串的每一个字符是否相等.

##static inline function是干嘛的?

如果你的.m文件需要频繁调用一个函数,可以用static inline来声明,这相当于把函数体当做一个大号的宏定义.不过这也不是百分之百有效,到底能不能把函数体转换为大号宏定义来用要看编译器心情,它要是觉得你的方法太复杂,他就不转了.他直接调用函数.

类似这种简单函数他肯定是乐意的.

	static inline CGRect ScaleRect(CGRect rect, float n)
这到底是什么鬼?static void *CapturingStillImageContext = &CapturingStillImageContext;

这种声明方式常用于kvo,用来当做contenxt的key来添加.例如

	[self addObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:CapturingStillImageContext];

这种声明方式可以导致\<mark>a method to create a unique pointer at compile time.</mark>在编译的时候创建一个唯一的指针.因为kvo的时候context如果不小心重复了,会发生奇怪的事情.用这种方式可以避免.

##如何快速定位crash的位置?

![](http://m2.img.srcdd.com/farm5/d/2015/0209/13/A6CB8DC4D75605DCF9D1F7503831B5E1_B1280_1280_320_153.png)

![](http://m1.img.srcdd.com/farm5/d/2015/0209/13/ED2098DC3D3F5BABA8B32340DEA6B6A7_B1280_1280_490_91.png)

这样如果你的app再crash就会自动定位到那句话.

##最快速的提升流畅度的方法?

用instrument找出所有不需要透明但是透明的view,layer.全部弄成不透明的.


![](http://m3.img.srcdd.com/farm4/d/2015/0209/13/CDB838DCE182D8FF866028C74E7903D3_B1280_1280_209_330.png)

![](http://m2.img.srcdd.com/farm4/d/2015/0209/13/65FABFCD208E238EAF149E7943BE7335_B1280_1280_750_433.png)
![](http://m2.img.srcdd.com/farm5/d/2015/0209/13/AEAE236B4BB3AC1AC9FCEE5A652467E1_B1280_1280_249_278.png)
![](http://m1.img.srcdd.com/farm4/d/2015/0209/13/1486EFAAFD3D116D9AE60D2AF49FD536_B1280_1280_373_78.png)

然后你会看见这些东西.

![](http://m3.img.srcdd.com/farm5/d/2015/0209/13/F2B2EBA00399D5B58FDF65A184BE2F2F_B800_2400_720_960.jpeg)

![](http://m3.img.srcdd.com/farm5/d/2015/0209/13/EA881797CFCEBB31C7EA3D0F25D6F4F4_B800_2400_720_960.jpeg)

![](http://m2.img.srcdd.com/farm5/d/2015/0209/13/8D13B5DE31FE249721BF965A588EDA96_B800_2400_720_960.jpeg)

红色或者深红色的就是透明的层和view,他们就是拖慢你fps的罪魁祸首,如果不需要透明的就改掉.


##一个神奇的工具,Accessorizer.

到底叼不叼,看图就知道了.

![](http://m1.img.srcdd.com/farm5/d/2015/0209/13/37BFD4775C9091931B1C3D3277DD5CF5_ORIG_750_720.gif)

##我安装的软件

![](http://m2.img.srcdd.com/farm4/d/2015/0209/14/D3701504E896BF73825EB22744B42864_B1280_1280_1280_687.png)
![](http://m1.img.srcdd.com/farm5/d/2015/0209/14/141CD5EAF181AB7C0B13CA7B84793D53_B1280_1280_1280_720.png)
![](http://m2.img.srcdd.com/farm4/d/2015/0209/14/226499E656007F91A5653381AE6BA772_B1280_1280_1280_763.png)

title: Swift_6_方法
date: 2014-07-21 15:43:00
categories:
- iOS应用开发之Swift
tags:
- Swift
---

```objc
import Foundation

println("Hello, World!")

class Dog {
    func age() -> String {
    return "2"
    }
    
    var name : String?
    
    func getName(name1:String) -> String{
        self.name = name1;
        return self.name!
    }
}

let dog = Dog()

var age = dog.age();

//dog.name = "小黑"

var dogName = dog.name;

var getName = dog.getName("123")

var name2 = dog.name;

println("dog age is \(age) name is \(dogName) getName is \(getName)  name2 is \(name2)")
```


title: Xcode5.1制作formework
date: 2014-08-11 17:26:00
categories:
- iOS应用开发之Xcode配置
tags:
- framework
---
1. 新建一个工程，选择framework & library - Cocoa Touch Static Library,命名为TestKit。![](http://img.blog.csdn.net/20140811170140857?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
2. 删除不必要的文件。TestKitTests 和TestKit target。
![](http://img.blog.csdn.net/20140811170014011?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
3. 新建一个Bundle 。new-file-target；选择OS X目录下FrameWork&Library中的Bundle,取名为TestiOSKit。![](http://img.blog.csdn.net/20140811165819234?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
4. 删除TestiOSKit中TestiOSKit-Prefix.pch中的#import<Cocoa/Cocoa.h>。![](http://img.blog.csdn.net/20140811170026211?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

5. 修改target设置Build Settings选项卡：
  Base SDK选择Latest iOS(iOS 7.1)
  Build ActiveArchitecture Only选择NO
  Dead Code Stripping选择NO
  Mach-O Type选择Relocatable Object File
         Link With Standard Libraries选择NO
   Wrapper Extension修改为framework 
 Info选项卡：
 Bundle OS Type code 修改为FMWK 
 Build Phases选项卡:
首先选择Editor-Add Build Phase-Add Copy Haeders Build Phase,在Build Phases下会多出一行Copy Headers;![](http://img.blog.csdn.net/20140811170030844?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
点击右下角的Add Build Phases,选择AddCopy Headers. 添加头文件和源文件。![](http://img.blog.csdn.net/20140811165835281?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
删除系统的Frameworks。(Remove References)

         ![](http://img.blog.csdn.net/20140811170100328?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

6. 在TestKit.h和TestMarkKit.m里面写入你的函数声明和实现
![](http://img.blog.csdn.net/20140811165840218?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)![](http://img.blog.csdn.net/20140811165845062?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)


7. 选择Run Scheme
![](http://img.blog.csdn.net/20140811165849625?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)


8. 查看framework build路径
点击Xcode右上角Organizer,选择Projects选项卡如图，点击箭头可以进入bulid路径。![](http://img.blog.csdn.net/20140811170055570?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
或者选择iOS Device运行一下，然后在工程的Products-TestiOSKit.framework右键，show in finder。文件夹下en.lproj和Info.plist都可以直接删除。
![](http://img.blog.csdn.net/20140811170603171?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)


9. 使用framework
只需要在新的工程导入并使用就行啦。![](http://img.blog.csdn.net/20140811170859077?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

10. 但是我们会遇到一个问题，就是这个framework的包分两个，一个是模拟器，一个是真机，所以我们要把它合并成一个通用的包。

在finder下是这样的：
![](http://img.blog.csdn.net/20140811170913218?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
一个是真机，一个是模拟器，下面开始合并。
1.先把其中一个framework包放到桌面。
2.打开终端运行> lipo -create/Users/Jerry/Library/Developer/Xcode/DerivedData/TestKit-dmdcaxednjyebxgulialinjfjzho/Build/Products/Debug-iphoneos/TestiOSKit.framework/TestiOSKit/Users/Jerry/Library/Developer/Xcode/DerivedData/TestKit-dmdcaxednjyebxgulialinjfjzho/Build/Products/Debug-iphonesimulator/TestiOSKit.framework/TestiOSKit-output /Users/Jerry/Desktop/TestiOSKit
> ![](http://img.blog.csdn.net/20140811172349714?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

    说明一下这段代码，红色为固有字段，黑色部分均为路径，前两个是模拟器路径和真机的路径，后面的为输出路径。
    3.把输出的文件TestiOSKit替换第一步放在桌面上的framework里的TestiOSKit，然后这个framework就是通用的了。
此方法同样适用.a文件的合成。
              ![](http://img.blog.csdn.net/20140811172415984?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
11. 我把常用的宏定义和类别做成的framework地址：[https://code.csdn.net/yinxianwei88/utilitytools](https://code.csdn.net/yinxianwei88/utilitytools)
12. 本贴参考：
[http://blog.csdn.net/smking/article/details/24434819](http://blog.csdn.net/smking/article/details/24434819)
[http://blog.csdn.net/mark_creative/article/details/9450887](http://blog.csdn.net/mark_creative/article/details/9450887)



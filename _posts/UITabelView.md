title: UITabelView
date: 2013-09-07 08:45:00
categories:
- iOS应用开发之UITableViewCell
tags:
- ios
- iphone开发
---

1.单行打√的实现


```plain
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

   //获得当前点击的行
    int newRow = indexPath.row;
    //如果上一次点击变量为空返回-1 否则返回上次点击行
    int oldRow = (indexPh==nil) ? -1 :indexPh.row;
    if (newRow!=oldRow)
    {
        //把当前cell打√
        UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        //之前cell取消√
        UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:indexPh];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        //保存当前点击indexPath
        self.indexPh = indexPath;
    }
    //取消选定当前点击indexPath  否则会出现cell背景蓝色
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}
```


2.cell上子控件读取数据

```plain
-(void)btnClick:(UIButton *)btn
{
    UIButton *button = (UIButton *)btn;
    //通过button找到父视图cell
    UITableViewCell *cell = (UITableViewCell *)[button superview];
    //得到cell的indexPath 返回row
    int row = [[_tableView indexPathForCell:cell] row];
    NSString *str = [NSString stringWithFormat:@"You tapper the button for %@",[dataArray objectAtIndex:row]] ;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You tapped the button" message:str delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}
```



3. UITableiVIew的Move的实现


```plain
//此方法添加  编辑状态cell可以拖动,不写此方法,编辑状态不能拖动
- (void)tableView:(UITableView *)tableView
moveRowAtIndexPath:(NSIndexPath *)fromIndexPath
      toIndexPath:(NSIndexPath *)toIndexPath
{
    int oldRow = fromIndexPath.row;
    int newRow = toIndexPath.row;
    //对数据源操作后不用刷新页面 否则出现异常现象
    NSString *newStr = [dataArray objectAtIndex:newRow];
    [(NSMutableArray *)dataArray removeObjectAtIndex:oldRow];
    [(NSMutableArray *)dataArray insertObject:newStr atIndex:newRow];
}

```


4.Delete的实现

```plain
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //删除,首先更新数据源,再在tableview上以动画形式删除cell
    int row = indexPath.row;
    [(NSMutableArray *)dataArray removeObjectAtIndex:row];
    
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
}

//删除键的标题.
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}



```

5.程序内部文件的读取和保存本地数据的实现

```plain
    //******************************
    //首先获取本地存储文件路径
    NSString *strPath = [self returnPath];
    // 创建文件管理器
    NSFileManager *manager = [NSFileManager defaultManager];

    //检查strPath所代表的文件是否存在 存在就读取 否则就读取程序内部文件
    if ([manager fileExistsAtPath:strPath])
    {
        NSData *data = [NSData dataWithContentsOfFile:strPath];
      dataArray = [[NSKeyedUnarchiver unarchiveObjectWithData:data] retain];
    }

    else
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"presidents" ofType:@"plist"];
        NSData *data = [NSData dataWithContentsOfFile:path];
        dataArray = [[NSKeyedUnarchiver unarchiveObjectWithData:data] retain];
    }

    //接受程序Home键发送的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savePath) name:UIApplicationDidEnterBackgroundNotification object:nil];

-(void)savePath
{
    //保存文件
    [NSKeyedArchiver archiveRootObject:dataArray  toFile:[self returnPath]];
    
}

-(NSString *)returnPath
{
    NSArray *array = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [array objectAtIndex:0];

   return [path stringByAppendingPathComponent:@"tempPresident.plist"];
}

```



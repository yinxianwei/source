title: Swift秘籍
date: 2015-04-14 17:03:42
categories:
- iOS应用开发之Swift
tags:
- Swift
---

1. ##字符串是否包含另一字符串

		  if "字符串".componentsSeparatedByString("字").count > 1 {
		  	println("包含");
		  }
		  
		//不区分大小写的判断写法
		  
		   if let match = "zifuchuan".rangeOfString("Z", options: NSStringCompareOptions.CaseInsensitiveSearch){
		  	println("包含");
                        }
		  
	<!--more-->
2. ##== 和 ===

		"=="是比较两个变量，相同就返回true
		"==="是比较两个变量的内存地址，当然还有"!=="

3. ##Swift使用MagicalRecord

    - 添加拓展
	
			extension NSManagedObject {
		    class func entityName() -> String {
		        return NSStringFromClass(self).componentsSeparatedByString(".").last!
		    	}
			}

	- 在你的Entity上添加`@objc(SearchEntity) ` 如：
	
			@objc(SearchEntity)

			class SearchEntity: NSManagedObject {
				
			    @NSManaged var info: String
			    @NSManaged var tagid: NSNumber
			}

	- 参考链接
	
	 	[http://stackoverflow.com/questions/26613971/swift-coredata-warning-unable-to-load-class-named](http://stackoverflow.com/questions/26613971/swift-coredata-warning-unable-to-load-class-named)
		[http://blog.csdn.net/tounaobun/article/details/42219495](http://blog.csdn.net/tounaobun/article/details/42219495)
		[http://stackoverflow.com/questions/25746621/magicalrecord-createentity-error-in-swift](http://stackoverflow.com/questions/25746621/magicalrecord-createentity-error-in-swift)
		
4. ##Storyboard怎么设置代理给Appdelegate

		假如把UITabBarController的delegate设置给Appdelegate， 只需要在TabBarController中新增一个Object，并继承于Appdelegate，然后把delegate的关系连接在这个Object(Appdelegate)上就行了。
		
5. ##快速预览xib在模拟器中的样式

	- 打开故事板或者xib

	- 选择Assistant editor

	- 在出现的右侧窗口上方点击Automatic选择Preview，选择需要预览的xib
	
	- 参考链接：[http://hhgz9527.sinaapp.com/快速预览xib在模拟器中的样式](http://hhgz9527.sinaapp.com/%E5%BF%AB%E9%80%9F%E9%A2%84%E8%A7%88xib%E5%9C%A8%E6%A8%A1%E6%8B%9F%E5%99%A8%E4%B8%AD%E7%9A%84%E6%A0%B7%E5%BC%8F/)
	
6. ##Swift & the Objective-C Runtime

	简单的使用 objc_get/setAssociatedObject()来填充其 get 和 set 块：
		
		extension UIViewController {     
	    private struct AssociatedKeys {         
	        static var DescriptiveName = "nsh_DescriptiveName"     
	    }     
	     
	    var descriptiveName: String? {         
	        get {             
	            return objc_getAssociatedObject(self, &AssociatedKeys.DescriptiveName) as? String         
	        }         
	        set {             
	            if let newValue = newValue {                 
	                objc_setAssociatedObject(                     
	                    self,                     
	                    &AssociatedKeys.DescriptiveName,                     
	                    newValue as NSString?,                     
	                    UInt(OBJC_ASSOCIATION_RETAIN_NONATOMIC)                 
	                )             
	            }         
	        }     
	      } 
	    }
	    
   方法重载
	
		extension UIViewController {     
	    public override class func initialize() {         
	        struct Static {             
	            static var token: dispatch_once_t = 0         
	        }         
	         
	        // make sure this isn't a subclass                 
	        if self !== UIViewController.self {             
	            return         
	        }         
	         
	        dispatch_once(&Static.token) {             
	            let originalSelector = Selector("viewWillAppear:")             
	            let swizzledSelector = Selector("nsh_viewWillAppear:")  
	                        
	            let originalMethod = class_getInstanceMethod(self, originalSelector)             
	            let swizzledMethod = class_getInstanceMethod(self, swizzledSelector)
	                          
	            let didAddMethod = class_addMethod(self, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))             
	             
	            if didAddMethod {                 
	                class_replaceMethod(self, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))            
	            } else {                 
	                method_exchangeImplementations(originalMethod, swizzledMethod);             
	            }         
	        }     
	    }     
	     
	    // MARK: - Method Swizzling     
	     
	    func nsh_viewWillAppear(animated: Bool) {         
	        self.nsh_viewWillAppear(animated)         
	        if let name = self.descriptiveName {             
	            println("viewWillAppear: \(name)")         
	        } else {             
	            println("viewWillAppear: \(self)")         
	        }     
	      } 
	    }
	    

   参考：[Swift & the Objective-C Runtime](http://nshipster.cn/swift-objc-runtime/)
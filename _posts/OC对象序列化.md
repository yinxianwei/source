title: OC对象序列化
date: 2013-09-15 12:22:00
categories:
- iOS应用开发之常用方法
tags:
- ios
- iphone开发
---
本文来自:[感悟](http://liuyujie.lofter.com/post/83b14_ff6fb)


OC可以程序用到的各种对象序
列话到文件，在任何需要的情况下，从文件中重新读取数据重新构造对象，下面说一下对象的序列化和反序列化。
利用NSKeyedArchiver把对象序列化到文件中：


```html
//=================NSKeyedArchiver========================   
NSString *saveStr1 = @"NSKeyedArchiver1";  
NSString *saveStr2 = @"NSKeyedArchiver2";  
NSArray *array = [NSArray arrayWithObjects:saveStr1, saveStr2, nil];   
//—-Save  
//这一句是将路径和文件名合成文件完整路径  
NSString *Path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];   
NSString *filename = [Path stringByAppendingPathComponent:@"saveDatatest"];  
[NSKeyedArchiver archiveRootObject:array toFile:filename]; 
```


利用NSKeyedUnarchiver从文件中反序列化成对象：


```cpp
array = [NSKeyedUnarchiver unarchiveObjectWithFile: filename];    
   NSLog(@">>%@",array);
```





title: "Masonry的使用"
date: 2015-03-31 10:52:07
categories: iOS应用开发之UI
tags: Masonry
---
### 常用方法
#### 设置view的位置

	- (void)initViews{
	    
	    UIView *view = [UIView new];
	    [self.view addSubview:view];
	    
	    [view mas_makeConstraints:^(MASConstraintMaker *make) {
	        make.edges.equalTo(view.superview).with.insets(UIEdgeInsetsMake(10, 10, 10, 10));
	        
	        /* 等价于
	         make.top.equalTo(sv).with.offset(10);
	         make.left.equalTo(sv).with.offset(10);
	         make.bottom.equalTo(sv).with.offset(-10);
	         make.right.equalTo(sv).with.offset(-10);
	         */
	        
	        /* 也等价于
	         make.top.left.bottom.and.right.equalTo(sv).with.insets(UIEdgeInsetsMake(10, 10, 10, 10));
	         */
	    }];
	}

<!--more-->
#### 设置view的size
    make.size.mas_equalTo(CGSizeMake(40, 40));

#### 更新删除view的约束

	- (NSArray *)mas_makeConstraints:(void(^)(MASConstraintMaker *make))block;
	- (NSArray *)mas_updateConstraints:(void(^)(MASConstraintMaker *make))block;
	- (NSArray *)mas_remakeConstraints:(void(^)(MASConstraintMaker *make))block;
	
	/*
		mas_makeConstraints 只负责新增约束 Autolayout不能同时存在两条针对于同一对象的约束 否则会报错 
		mas_updateConstraints 针对上面的情况 会更新在block中出现的约束 不会导致出现两个相同约束的情况
		mas_remakeConstraints 则会清除之前的所有约束 仅保留最新的约束
		
		三种函数善加利用 就可以应对各种情况了
	*/

#### mas_equalTo()和equalTo()的区别
>`mas_equalTo支持的类型更多`

	#define mas_equalTo(...)                 equalTo(MASBoxValue((__VA_ARGS__)))
	#define mas_greaterThanOrEqualTo(...)    greaterThanOrEqualTo(MASBoxValue((__VA_ARGS__)))
	#define mas_lessThanOrEqualTo(...)       lessThanOrEqualTo(MASBoxValue((__VA_ARGS__)))
	
	#define mas_offset(...)                  valueOffset(MASBoxValue((__VA_ARGS__)))
		

### 一个view
#### 居中
	///* 居中
	make.size.mas_equalTo(CGSizeMake(100, 60));
	
	make.centerY.equalTo(view.superview.mas_centerY);
	make.centerX.equalTo(view.superview.mas_centerX);
	
	// */

### 多个view
#### 大小相同
	make.width.equalTo(v1.mas_width);
	make.height.equalTo(v1.mas_height);
#### 首尾相连
	//与v2上下间距为10
	make.bottom.mas_equalTo(v2.mas_top).offset(-10);

___

	//与v1左右间距为10
	make.left.equalTo(v1.mas_right).offset(10);

#### 同一垂直中线
	//与v1的x中心点在同一直线
	make.centerX.equalTo(v1.mas_centerX);
	
#### 示例
	- (void)towView{
	
	    UIView *v1 = [UIView new];
	    v1.backgroundColor = [UIColor redColor];
	    [self.view addSubview:v1];
	    
	    UIView *v2 = [UIView new];
	    v2.backgroundColor = [UIColor blackColor];
	    [self.view addSubview:v2];
	
	    [v1 mas_makeConstraints:^(MASConstraintMaker *make) {
	        make.size.mas_equalTo(CGSizeMake(200, 120));
	        make.top.mas_equalTo(v1.superview).offset(74);
	        //与v2上下间距为10
	        make.bottom.mas_equalTo(v2.mas_top).offset(-10);
	        make.centerX.equalTo(v1.superview.mas_centerX);
	    }];
	    
	    [v2 mas_makeConstraints:^(MASConstraintMaker *make) {
	        make.size.mas_equalTo(CGSizeMake(250, 30));
	       
	        make.centerX.equalTo(v1.mas_centerX);
	        /*和v1的宽高相等
	        make.width.equalTo(v1.mas_width);
	        make.height.equalTo(v1.mas_height);
	        //*/
	        make.top.mas_equalTo(v1.mas_bottom).offset(10);
	        make.bottom.mas_equalTo(v2.superview).offset(-10);
	    }];
	}
![](http://7teblm.com1.z0.glb.clouddn.com/1030?imageMogr2/thumbnail/!50p)
### 九宫格排序
	- (void)squaredUp{
	    NSMutableArray *array  =  [NSMutableArray array];
	    for (int i = 0; i<14; i++) {
	        [array addObject:[NSString stringWithFormat:@"%d",i]];
	    }
	    
	    __block UIView *lastV = nil;
	    int list = 4;
	    int gap = 10;
	    
	    for (int i = 0; i<array.count; i++) {
	        
	        UILabel *label = [UILabel new];
	        label.text = array[i];
	        [self.view addSubview:label];
	        label.textAlignment = NSTextAlignmentCenter;
	        label.backgroundColor = [UIColor magentaColor];
	        [label sizeToFit];
	        
	        [label mas_makeConstraints:^(MASConstraintMaker *make) {
	            if (lastV) {
	                make.width.equalTo(lastV.mas_width);
	            }else
	                make.size.mas_equalTo(CGSizeMake((self.view.frame.size.width - gap*(list + 1))/list, 20));
	            
	            if (i%list == 0) {
	                make.left.equalTo(label.superview).offset(gap);
	            }
	            else{
	                make.left.equalTo(lastV.mas_right).offset(gap);
	            }
	            if (i%list == (list -1)) {
	                make.right.equalTo(label.superview).offset(-gap);
	            }
	            int top = 64+(i/list+1)*gap+(i/list*label.frame.size.height);
	            make.top.equalTo(label.superview).offset(top);
	            
	            lastV = label;
	        }];
	    }
	}
	
![](http://7teblm.com1.z0.glb.clouddn.com/1036.png?imageMogr2/thumbnail/!50p)

### 不规则标签排序


参考：[http://adad184.com/2014/09/28/use-masonry-to-quick-solve-autolayout/](http://adad184.com/2014/09/28/use-masonry-to-quick-solve-autolayout/)
title: 关于UIView 的autoresizingMask属性，即UIViewAutoresizing
date: 2014-01-21 17:12:00
categories:
- iOS应用开发之UI
tags:
- iphone
- ios开发
- ios
- iphone开发
- uiview
---
**作者：舍得333
 主页：**[http://blog.sina.com.cn/u/1509658847](http://blog.sina.com.cn/u/1509658847)
**版权声明**：原创作品，允许转载，转载时请务必以超链接形式标明文章原始出版、作者信息和本声明，否则将追究法律责任。

enum
 {
   UIViewAutoresizingNone                 =
 0,
   UIViewAutoresizingFlexibleLeftMargin   =
 1 << 0,
   UIViewAutoresizingFlexibleWidth        =
 1 << 1,
   UIViewAutoresizingFlexibleRightMargin  =
 1 << 2,
   UIViewAutoresizingFlexibleTopMargin    =
 1 << 3,
   UIViewAutoresizingFlexibleHeight       =
 1 << 4,
   UIViewAutoresizingFlexibleBottomMargin
 = 1 << 5
};
typedef NSUInteger UIViewAutoresizing;
这六个值与xib文件里面的图形显示怎么对应起来，这个说起来很坑爹的。
代码设置与xib图形设置很不一至。

根据UIView的API文档说明，autoresizingMask的默认值是UIViewAutoresizingNone
那么再xib文件里面对应的图形是：



即四周选中，中间2个没有选中。这个真是有点变态啊，我刚开始以为是全都不选呢。

再举个例子：
我想要view一直与左下角对齐，高度自由伸缩，xib设置如图：



那么代码应该怎么写呢？一定要注意看清每个单词哦！！！，如下：
subView.autoresizingMask =
    UIViewAutoresizingFlexibleTopMargin
 |
    UIViewAutoresizingFlexibleRightMargin
 |
    UIViewAutoresizingFlexibleHeight;


下面的文章参考自：http://www.cnblogs.com/kiao295338444/articles/2308903.html
在 UIView 中有一个autoresizingMask的属性，它对应的是一个枚举的值（如下），属性的意思就是自动调整子控件与父控件中间的位置，宽高。
enum {
   UIViewAutoresizingNone                 = 0,
   UIViewAutoresizingFlexibleLeftMargin   = 1 << 0,
   UIViewAutoresizingFlexibleWidth        = 1 << 1,
   UIViewAutoresizingFlexibleRightMargin  = 1 << 2,
   UIViewAutoresizingFlexibleTopMargin    = 1 << 3,
   UIViewAutoresizingFlexibleHeight       = 1 << 4,
   UIViewAutoresizingFlexibleBottomMargin = 1 << 5
};
typedef NSUInteger UIViewAutoresizing;


分别解释以上意思。
UIViewAutoresizingNone就是不自动调整。
UIViewAutoresizingFlexibleLeftMargin就是自动调整与superView左边的距离，也就是说，与superView右边的距离不变。
UIViewAutoresizingFlexibleRightMargin就是自动调整与superView的右边距离，也就是说，与superView左边的距离不变。
UIViewAutoresizingFlexibleTopMargin

UIViewAutoresizingFlexibleBottomMargin
UIViewAutoresizingFlexibleWidth
UIViewAutoresizingFlexibleHeight
以上就不多解释了，参照上面的。
也可以多个枚举同时设置。如下：
subView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin  |UIViewAutoresizingFlexibleRightMargin;
如果有多个，就用“|”关联。
还有一个属性就是autoresizesSubviews，此属性的意思就是，是否可以让其subviews自动进行调整，默认状态是YES，就是允许，如果设置成NO，那么subView的autoresizingMask属性失效。


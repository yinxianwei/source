title: Mac上本地新建SVN步骤
date: 2015-01-09 17:27:28
categories:
tags: SVN
---

![](http://7teblm.com1.z0.glb.clouddn.com/1.png)
<!-- more -->
![](http://7teblm.com1.z0.glb.clouddn.com/2.png)
![](http://7teblm.com1.z0.glb.clouddn.com/3.png)
![](http://7teblm.com1.z0.glb.clouddn.com/4.png)
![](http://7teblm.com1.z0.glb.clouddn.com/5.png)
![](http://7teblm.com1.z0.glb.clouddn.com/6.png)
![](http://7teblm.com1.z0.glb.clouddn.com/7.png)
![](http://7teblm.com1.z0.glb.clouddn.com/8.png)
![](http://7teblm.com1.z0.glb.clouddn.com/9.png)
![](http://7teblm.com1.z0.glb.clouddn.com/10.png)

4. 启动SVN服务
svnserve -d -r  /Users/pengfeishi/Desktop/svn  
特别注意，路径一定是SVN的目录，不是其中一个版本库的目录，不然，能正常启动，就是访问有问题
没有任何输出，则启动成功

5. 关闭 killall -9 svnserve

6. 连接  svn://127.0.0.1/res

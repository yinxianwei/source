title: iOS自定义UITabBar
date: 2014-03-28 18:55:00
categories:
- iOS应用开发之UI
tags:
- iOS自定义Tabbar
- iOS开发
- UITabBarController
---
push页面时，可调用hidesBottomBarWhenPushed进行隐藏。
第一步，我们需要一些图片：
![](http://img.blog.csdn.net/20140328183512937?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
各个选项的图标和tabbar的背景图片，最后还要一个透明的1x1像素的图片。
第二步，新建一个工程，在工程内建一个继承于UITabBarController的类。
![](http://img.blog.csdn.net/20140328183813281?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

第三步，首先写一个方法，返回一个UINavigationController


```objc
-(UINavigationController*) viewControllerWithTitle:(NSString*) title image:(UIImage*)image
{
    UIViewController* viewController = [[UIViewController alloc] init];
    viewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:image tag:0];
    viewController.title = title;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:viewController];
    return nav;
}
```

然后在viewDidLoad里面创建TabbarController的viewControllers


```objc
 self.viewControllers = [NSArray arrayWithObjects:
                            [self viewControllerWithTitle:@"1" image:IMG(@"1")],
                            [self viewControllerWithTitle:@"2" image:IMG(@"2")],
                            [self viewControllerWithTitle:nil image:nil],
                            [self viewControllerWithTitle:@"3" image:IMG(@"3")],
                            [self viewControllerWithTitle:@"4" image:IMG(@"4")], nil];
```

看到没有，比较猥琐的就是第三个ViewController什么都没设置。
因为我们要在那个位置放一个自己的按钮，继续在viewDidLoad写：


```objc
    UIButton* button = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 0.0, 65, 65)];
    button.center = CGPointMake(160, 20);
    [button setBackgroundImage:IMG(@"add") forState:UIControlStateNormal];
    [button addTarget:self action:@selector(add:) forControlEvents:UIControlEventTouchUpInside];
    [self.tabBar addSubview:button];
```

然后设置背景图片：


```objc
    [self.tabBar setBackgroundImage:IMG(@"tabbarbg")];
```

运行之后是这样的：![](http://img.blog.csdn.net/20140328184528437?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)


会发现按钮上面有一条横线，然后再设置这个阴影运行后就没有人发现你猥琐的行径。

```objc
    [self.tabBar setShadowImage:IMG(@"transparent")];
```
最后效果图（iOS7和iOS6）：

![](http://img.blog.csdn.net/20140328184922531?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)![](http://img.blog.csdn.net/20140328184926250?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveWluX3hpYW53ZWk=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)


这样做的好处：
在ViewController里push页面的时候可调用hidesBottomBarWhenPushed的属性进行隐藏。


[源码下载](http://download.csdn.net/detail/yinxianwei88/7113863)





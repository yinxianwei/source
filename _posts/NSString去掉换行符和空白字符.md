title: NSString去掉换行符和空白字符
date: 2013-11-29 14:41:00
categories:
- iOS应用开发之常用方法
tags:
- iphone开发
- ios开发
---

```objc
NSString* headerData=***;
headerData = [headerData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
       headerData = [headerData stringByReplacingOccurrencesOfString:@"\r" withString:@""];
       headerData = [headerData stringByReplacingOccurrencesOfString:@"\n" withString:@""];
如此即可。
```



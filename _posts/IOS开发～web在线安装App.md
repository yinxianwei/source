title: IOS开发～web在线安装App
date: 2014-04-30 09:39:00
categories:
- iOS应用开发之Xcode配置
tags:
- iOS开发
---
经测试，在越狱设备上可以安装，如果设备不越狱也想安装，就要企业发布权限了，可惜我没有，所以不能测试了！

1、创建ipa及plist文件：
XCode菜单栏->Product->Archive->Distribute->Save for Enterprise or Ad-Hod Deploymemnt
点击Next
选择你的Code signing Identity，我选的是Team profile
点击Next
选择Save for Enterprise Distribution，并且填写信息
![](http://img.blog.csdn.net/20130812172507484?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbGl6aG9uZ2Z1MjAxMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

说明：Large Image URL 与 Small Image URL 对应你的App安装过程中的icon


点击保存后，会生成ipa文件和plist文件。
plist文件如下：
![](http://img.blog.csdn.net/20130812173125406?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvbGl6aG9uZ2Z1MjAxMw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

2、创建web下载页面

**[html]** [view
 plain](http://blog.csdn.net/lizhongfu2013/article/details/9930409# "view plain")[copy](http://blog.csdn.net/lizhongfu2013/article/details/9930409# "copy")[print](http://blog.csdn.net/lizhongfu2013/article/details/9930409# "print")[?](http://blog.csdn.net/lizhongfu2013/article/details/9930409# "?")1. <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">  
2. <html xmlns="http://www.w3.org/1999/xhtml">  
3. <head>  
4. <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
5. </head>  
6. <body>  
7.     <div align='center'>  
8.         <a href="itms-services://?action=download-manifest&url=http://yiwenxue.cn/game/game-demo.plist">在线安装</a>  
9.     <div>  
10. </body>  
11. </html>  


3、部署与测试
将项目ipa文件、plist文件、Large Image、Small Image、第（2）部创建的web页面部署到服务器目录中，IOS设备输入http://yiwenxue.cn/game，然后点击安装，安装URL根据你实际情况而定。另，有想自己亲手测试一下的，可以把文件都发给我，我帮忙部署（如果和商业有关，那就免了）！仅限空闲时间！


转载出处:[http://blog.csdn.net/lizhongfu2013/article/details/9930409](http://blog.csdn.net/lizhongfu2013/article/details/9930409)

title: Swift_1_基本数据类型
date: 2014-07-21 11:40:00
categories:
- iOS应用开发之Swift
tags:
- Swift
---

```objc
import Foundation

println("Hello, World!");

var v1 = 1;
var v2 = 2;

println(" v1 is \(v1)  v2 is \(v2)");

var v3 = v1 + v2;

println("v3 is \(v3)");

//字符转换
var v4 : Double = Double(v3) + 3.14;

println("v4 is \(v4)");

var v5 = "hello"

let v6 = "world"

println(v5 + " " + v6);

//声明只读类型变量
let v7 = 3.1415;

println("v7 is " + String(v7));

var arr = [1,2,4,6];

println("arr index 1 is \(arr[1])");

//声明字典
var airports: Dictionary<String, String> = ["TYO": "Tokyo", "DUB": "Dublin"];

var v8 = "10";

//字符串是否为空
if v8.isEmpty{
    println("v8 is true");
}

//遍历字符串
for character in "ddd1ew?"{
    println("char is " + character)
}

//字符串长度计算
let unusualMenagerie = "Koala ????, Snail ????, Penguin ????, Dromedary ????"
println("unusualMenagerie has \(countElements(unusualMenagerie)) characters")

var var9 = countElements(unusualMenagerie)
println("var9 lenght is \(var9)")

//比较字符串
let quotation = "We're a lot alike, you and I."
let sameQuotation = "We're a lot alike, you and I."
if quotation == sameQuotation {
    println("These two strings are considered equal")
}

//大小写转换
let normal = "Could you help me, please?"
let shouty = normal.uppercaseString
// shouty 值为 "COULD YOU HELP ME, PLEASE?"
let whispered = normal.lowercaseString
// whispered 值为 "could you help me, please?"

//String 转换 int

var i :Int = "1".toInt()!
println("\(i)")
```


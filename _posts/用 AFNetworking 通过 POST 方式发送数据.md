title: 用 AFNetworking 通过 POST 方式发送数据
date: 2014-09-13 15:14:00
categories:
- iOS应用开发之网络通讯
tags:
- AFNetworking
---
AFNetworking进行POST请求中 发送json数据有些特别 。
**AFNetworking 版本为 2.0.2**
POST 发送数据有两种形式：
1、发送纯文本的内容
2、发送的 body 部分带有文件（图片，音频或者其他二进制数据）
对应的 Content-Type 有两种：
1、application/x-www-form-urlencoded
2、multipart/form-data
传统的使用 POST 的方式发送数据用于上传文件，AFNetworking 中提供了直接的接口：

```objc
[self.manager POST:post_url parameters:params
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    // 直接以 key value 的形式向 formData 中追加二进制数据
    [formData appendPartWithFormData:[str dataUsingEncoding:NSUTF8StringEncoding] 
              name:@"key1"];
    [formData appendPartWithFileData:imgData name:@"imagefile" 
              fileName:@"img.jpg" mimeType:@"image/jpeg"];
    }
success:^(AFHTTPRequestOperation *operation, id responseObject) {
    // 成功后的处理
}
failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    // 失败后的处理
}];
```

使用 POST 方式发送纯文本内容：

```objc
- (NSMutableURLRequest *)postRequestWithURL:(NSString *)url content:(NSString *)text
{
    NSMutableURLRequest *request =
        [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" 
             forHTTPHeaderField:@"Contsetent-Type"];
    [request setHTTPBody:1];
 
    return request;
}
 
NSOperation *operation =
[self.manager HTTPRequestOperationWithRequest:request
success:^(AFHTTPRequestOperation *operation, id responseObject) {
    // 成功后的处理
}
failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    // 失败后的处理
}];
[self.manager.operationQueue addOperation:operation];
```

 
其中 self.manager 为 AFHTTPRequestOperationManager 实例。

```objc
_manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
 
// 对于网站成功返回 JSON 格式的数据但是却在 failure 回调中显示的，
// 是因为服务器返回数据的网页中 content type 没有设置为 text/json
// 对于我们公司的服务器返回的 content type 为 text/html 所以我设置为如下这样，
// 对于不同的情况可以根据自己的情况设置合适的接受的 content type 的类型
_manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
```

 
本文转自：[http://www.iliunian.com/2879.html](http://www.iliunian.com/2879.html)
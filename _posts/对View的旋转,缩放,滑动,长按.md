title: 对View的旋转,缩放,滑动,长按
date: 2013-09-19 14:37:00
categories:
- iOS应用开发之触控
tags:
- fontfontios的fontfont
- fontfontiPhone开发font
- fontfont动画fontfont
---
/ / UIView的从厦门国际银行中创建成功的时候调用此方法


```plain
 - （无效）awakeFromNib中
{
    [超级awakeFromNib中];
    / /手势手势识别器识别器
    / /给三个ImageView的加上所需要的手势 
    [自我addGestureRecognizersToPiece：firstPieceView];
    [自我addGestureRecognizersToPiece：secondPieceView];
    [自我addGestureRecognizersToPiece：thirdPieceView];
}
```
/ /添加了一组手势识别来我们一块子视图1

```plain
 - （无效）addGestureRecognizersToPiece：（UIView的*）片
{
	/ / UIGestureRecognizer与手势有关的基类
	/ /手势只会响应在最后添加的那个视图上
    / /旋转手势
    UIRotationGestureRecognizer * rotationGesture = [[UIRotationGestureRecognizer的alloc] initWithTarget：自我行动：@选择（rotatePiece :)];
    [件addGestureRecognizer：rotationGesture];
	[rotationGesture释放];
    	
    / /缩放手势
    UIPinchGestureRecognizer * pinchGesture = [[UIPinchGestureRecognizer的alloc] initWithTarget：自我行动：@选择（scalePiece :)];
    [pinchGesture setDelegate：个体经营];
    [件addGestureRecognizer：pinchGesture];
    [pinchGesture释放];
    
    / /滑动手势
    UIPanGestureRecognizer * panGesture = [[UIPanGestureRecognizer的alloc] initWithTarget：自我行动：@选择（panPiece :)];
    [panGesture setMaximumNumberOfTouches：2];
    [panGesture setDelegate：个体经营];
    [件addGestureRecognizer：panGesture];
    [panGesture释放];
    
    / /长按手势
    UILongPressGestureRecognizer * longPressGesture = [[UILongPressGestureRecognizer的alloc] initWithTarget：自我行动：@选择（showResetMenu :)];
    [件addGestureRecognizer：longPressGesture];
    [longPressGesture释放];
}
```
/ / UIMenuController要求我们能成为第一个响应，否则将不显示
/ /有没有能力成为第一响应者


```plain
 - （BOOL）canBecomeFirstResponder
{
    返回YES;
}
```

/ /缩放和旋转变换应用于相对于该层的锚点
/ /此方法会将用户的手指之间的手势识别的视图的锚点


```plain
 - （无效）adjustAnchorPointForGestureRecognizer：（UIGestureRecognizer *）gestureRecognizer 
{
    / / UIGestureRecognizerStateBegan意味着手势已经被识别
    如果（gestureRecognizer.state == UIGestureRecognizerStateBegan） 
    {
        / /手势发生在哪个视图上
        UIView的*片= gestureRecognizer.view;
        / /获得当前手势在视图上的位置。
        CGPoint locationInView = [gestureRecognizer locationInView：件];
        
        piece.layer.anchorPoint = CGPointMake（locationInView.x / piece.bounds.size.width，locationInView.y / piece.bounds.size.height）;
        / /根据在视图上的位置设置锚点。
        //防止设置完锚点过后，view的位置发生变化，相当于把view的位置重新定位到原来的位置上。
        CGPoint locationInSuperview = [gestureRecognizer locationInView：piece.superview];
        piece.center = locationInSuperview;
    }
}

```

/ /旋转手势
/ /旋转件的当前旋转
/ /申请后重置手势识别的旋转为0，所以接下来的回调是从当前旋转增量


```plain
 - （无效）rotatePiece：（UIRotationGestureRecognizer *）gestureRecognizer
{
    / /调整锚点
	[自我adjustAnchorPointForGestureRecognizer：gestureRecognizer];
    / /当手势被识别出来的时候开始或改变
    如果（[gestureRecognizer状态] == UIGestureRecognizerStateBegan | | [gestureRecognizer状态] == UIGestureRecognizerStateChanged） 
    {
          / / gestureRecognizer.view在哪个视图上做的手势
         / / CGAffineTransformRotate在原来的基础上旋转多少度
        gestureRecognizer.view.transform = CGAffineTransformRotate（gestureRecognizer.view.transform，gestureRecognizer.rotation）;
        / /旋转到多少度
       / / CGAffineTransformMakeRotation（90）;
        / / CGAffineTransformRotate（[[gestureRecognizer查看]变换]，[gestureRecognizer旋转]）;
        / /在当前的旋转矩阵上再旋转多少度（在视图的当前状态下旋转多少度）
        / /因为gestureRecognizer.rotation会累加，所以我们每次都把他清0，这样我们下次旋转的时候得到的gestureRecognizer.rotation都是当前这次旋转了多少度，就可以调用，CGAffineTransformRotate，来在原来的基础上旋转多少度
        gestureRecognizer.rotation = 0;
    }
}
```
/ /缩放手势
/ /刻度片由目前的规模
应用之后，/ /重置手势识别器的旋转为0，所以接下来的回调是从目前的规模增量


```plain
 - （无效）scalePiece：（UIPinchGestureRecognizer *）gestureRecognizer
{
    [自我adjustAnchorPointForGestureRecognizer：gestureRecognizer];
    
    如果（[gestureRecognizer状态] == UIGestureRecognizerStateBegan | | [gestureRecognizer状态] == UIGestureRecognizerStateChanged）
    {
        [gestureRecognizer观点]变换= CGAffineTransformScale（[[gestureRecognizer查看]变换]，[gestureRecognizer规模]，[gestureRecognizer规模]）;
        [gestureRecognizer setScale：1];
    }
}

```

/ /滑动手势
/ /移片的中心由平移量
/ /重设手势识别的翻译{0,0}申请后，所以接下来的回调是从当前位置的增量

```plain
 - （无效）panPiece：（UIPanGestureRecognizer *）gestureRecognizer
{
    / /在当前的哪个观点上发生了这个手势。
    [自我adjustAnchorPointForGestureRecognizer：gestureRecognizer];
    如果（[gestureRecognizer状态] == UIGestureRecognizerStateBegan | | [gestureRecognizer状态] == UIGestureRecognizerStateChanged）
    {
        UIView的*片= [gestureRecognizer观点];
        / /获得手势在父视图中移动的位置
        CGPoint翻译= [gestureRecognizer translationInView：[​​件超景];
        [件可以借助于setCenter：CGPointMake（。【片中心] X + translation.x，[片中心] Y + translation.y）];
        / /重置这个手势的滑动距离。
        / / CGPointMake（0，0）== CGPointZero
        / / CGRectZero
         [gestureRecognizer setTranslation：CGPointZero inView：[​​件超景];
    }
}
```
/ /长按手势
/ /显示一个带有单个项目，使片的变换重置菜单


```plain
 - （无效）showResetMenu：（UILongPressGestureRecognizer *）gestureRecognizer
{
    如果（[gestureRecognizer状态] == UIGestureRecognizerStateBegan） 
    {
        UIMenuController * menuController = [UIMenuController sharedMenuController];
        UIMenuItem * resetMenuItem = [[UIMenuItem的alloc] initWithTitle：@“复位”的动作：@选择（resetPiece :)];
        
        [menuController setMenuItems：[NSArray的arrayWithObjects：resetMenuItem，resetMenuItem2，resetMenuItem3，零];
        
        / /成为第一响应者，如果不成为第一响应者，UIMenuController无法显示
        [自我becomeFirstResponder];
        
        CGPoint位置= [gestureRecognizer locationInView：[​​gestureRecognizer观点];

        [menuController setTargetRect：CGRectMake（location.x，location.y，0，0）inView：[​​gestureRecognizer观点];
        / /显示出来
        [menuController setMenuVisible：是动画：YES];
        
        
        pieceForReset = [gestureRecognizer观点];
        
        [resetMenuItem释放];
    }
}

```

/ /重置所有变换
/ /动画返回到默认的锚点和改造
```plain
 - （无效）resetPiece：（UIM​​enuController *）控制器
{
    / / CGRectGetMidX获得一个矩形中心点的x坐标
    / / convertPoint转换坐标系
    CGPoint locationInSuperview = [pieceForReset convertPoint：CGPointMake（CGRectGetMidX（pieceForReset.bounds），CGRectGetMidY（pieceForReset.bounds））toView：[​​pieceForReset超景];
    
    [[pieceForReset层] setAnchorPoint：CGPointMake（0.5，0.5）];
    / /防止锚点重置后，视图的位置发生变化
    [pieceForReset可以借助于setCenter：locationInSuperview];
    
    / /回到最初的单位矩阵的状态CGAffineTransformIdentity单位矩阵
    / /设置一个视图的等转换为CGAffineTransformIdentity意味着把所有的变换都去掉，包括旋转，缩放
    [UIView的和beginAnimations：无背景：无];
    [pieceForReset的setTransform：CGAffineTransformIdentity];
    [UIView的commitAnimations];
}
```
/ /判断手势之间是否可以同时作用，
/ /保证捏，平移和旋转手势识别一个特定的视图都可以同时识别
/ /防止其他手势识别的同时认识


```plain
 - （BOOL）gestureRecognizer：（UIGestureRecognizer *）gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer：（UIGestureRecognizer *）otherGestureRecognizer
{    
    / /如果手势识别的观点是不是我们的作品之一，不允许同时识别
    如果（gestureRecognizer.view！= firstPieceView && gestureRecognizer.view！= secondPieceView && gestureRecognizer.view！= thirdPieceView）
        返回NO;
    
    / /如果手势识别有不同意见，不允许同时识别
    / /如果两个手势作用的不是同一个视图，不允许同时发生
    如果（gestureRecognizer.view！= otherGestureRecognizer.view）
        返回NO;
    / /如果不是的手势识别的是长按，不允许同时识别
    
    / /如果两个都是长按，不允许同时发生
    如果（[gestureRecognizer isKindOfClass：[UILongPressGestureRecognizer类]] | | [otherGestureRecognizer isKindOfClass：[UILongPressGestureRecognizer类]）
        返回NO;
    
    返回YES;
}
```








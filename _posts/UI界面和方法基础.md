title: UI界面和方法基础
date: 2013-09-05 11:39:00
categories:
- iOS应用开发之UI
tags:
- ios
- iphone开发
- uiview
---
7.4

关于UIView界面
MainWindow.xib ===>界面文件

    UIWindow *window;
    IBOutlet UILabel *label;
    IBOutlet UIButton *butteon;

@property (nonatomic, retain) IBOutlet UIWindow *window;
可用.调用方法类。
============================================
分别创建窗口界面，标和按钮。
-(IBAction)butClick;
创建方法，此处IBAction为动作。可于能响应事件的控件绑定

实现方法：
    
-(IBAction)butClick
{
   label.text=@"好！";   //更改label的text  属性
}
     Alt+双击，显示方法细节
============================================
//initWithFormat有拼接字符串的能力，然后把拼接的字符串创建成字符串对象
//注意：
//%@  字符串占位符
//%d  int占位符   %f float占位符
    NSString *s1=@"小明";
    
    NSString *s2=@"今天";
    
    NSString *s3=@"没上课";
    int n=100;
    NSString *info=[[NSString alloc] initWithFormat:@"%@%@%@-->%d",s1, s2, s3, n];
    
    label.text=info;
 此处text输出结果为：   小明今天没上课100  
 主要用于text输出为字符串，其它类型转换为string类型。
============================================


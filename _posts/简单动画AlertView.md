title: 简单动画AlertView
date: 2013-09-05 11:45:00
categories:
- iOS应用开发之动画
tags:
- iphone开发
- ios
- 动画
---

    
    1.视图的创建
        
        UIView    //创建的不能引发事件，所以要用子类
        UIControl //来创建一个容器，用来装所有界面的子视图，便于整个界面的移除和进入操作
        
        //创建一个UIControl
        myView=[[UIControl alloc] initWithFrame:CGRectMake(0, 20, 320, 460)];
        [self.window addSubview:myView];

        //创建一个按钮。
        UIButton *btn1=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        btn1.frame=CGRectMake(90, 180, 60, 30);
        [btn1 setTitle:@"登录" forState:UIControlStateNormal];
        [myView addSubview:btn1];

        //创建一个标签
        UILabel *lab=[[UILabel alloc] initWithFrame:CGRectMake(80, 50, 70, 40)];
        lab.backgroundColor=[UIColor clearColor];
        lab.text=@"用户名:";
        [myView addSubview:lab];

         //创建一个textField
        //因为后面需要取textField.text的值，所以要先声明一个成员变量UITextField *textField;
        textField = [[UITextField alloc] initWithFrame:CGRectMake(150, 60, 100, 30)];
        textField.borderStyle=UITextBorderStyleRoundedRect;
        [myView2 addSubview:textField];

    2.消息提示框
        //创建一个消息提示框 otherButtonTitles@"A",@"B",@"C",nil;可添加多个标题，以nil结尾。
        UIAlertView *aler=[[UIAlertView alloc] initWithTitle:@"标题" message:@"提示内容" delegate:nil                    cancelButtonTitle:@"按钮标题" otherButtonTitles:nil];
        [aler show];//显示提示

    3.动画
        //创建一个系统定义的动画
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1];//动画持续时间
        [UIView setAnimationTransition:    UIViewAnimationTransitionCurlDown
                                       //动画效果
                               forView:self.window cache:YES];//动画对象和动画是否实时监视桌面窗口动态
        [UIView commitAnimations];//提交动画。和首行成对出现。
        
        //自定义一个简单的动画
                 -(void)diy{
        [UIView beginAnimations:nil context:nil];
          [UIView setAnimationDuration:2];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(back)];//当动画执行完毕就执行back方法
          view.bounds = CGRectMake(0, 0, 20, 20);//动画结束后的大小
         view.center = CGPointMake(200, 300);//动画结束后的位置
           view.alpha  = 0.2;//动画结束后的透明度
          [UIView commitAnimations];//提交动画
        }
                                                            
    4.  //移除某个控件
        [myView2 removeFromSuperview];

    5. //输入框为显示为密码状态
        textField.secureTextEntry=YES;






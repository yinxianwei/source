title: Swift_8_扩展And类别
date: 2014-07-21 15:45:00
categories:
- iOS应用开发之Swift
tags:
- Swift
---

```objc
import Foundation

println("Hello, World!")

class People{
    var name:String?
    
}
//方法扩展
extension People{
    func test() -> String{
        return "abc"

    }
    
}


let p = People();

var v1 = p.test();

println("v1 is \(v1)")
```


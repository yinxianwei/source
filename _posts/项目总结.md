title: 项目总结
date: 2013-09-07 08:45:00
categories:
- iOS应用开发之常用方法
tags:
- ios
- iphone开发
---
        


```plain
//stringByTrimmingCharactersInSet 修剪字符串
//whitespaceAndNewlineCharacterSet  空格和换行符
dish.dishKind = [[rs stringForColumn:@"iKind"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
```

UIAlertView上添加自定义视图,采用懒加载,
继承基类xib  基类加载xib的时候一定要指定xib的名称

//字符串分割  返回数组

```plain
        NSString *str = @"s|ss";
        NSArray *array1 = [str componentsSeparatedByString:@"|"];
```

1.Delete的实现


```plain
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //删除,首先更新数据源,再在tableview上以动画形式删除cell
    int row = indexPath.row;
    [(NSMutableArray *)dataArray removeObjectAtIndex:row];
    
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
}
```

2.
 //取消选定当前点击indexPath  否则会出现cell背景蓝色

```plain
[tableView deselectRowAtIndexPath:indexPath animated:YES];
```

//移除所有子控件


```plain
  for (UIView *aView in _detailView.subviews)
        {
        [aView removeFromSuperview];
        }
```
 
3.声音文件的简单播放
在.h文件的声明中

```plain
#import <AudioToolbox/AudioToolbox.h>
    SystemSoundID  soundID;

```

在.m的文件中，第一次执行的DidViewLoad方法里面加入
 
```plain
NSString *path = [[NSBundle mainBundle] pathForResource:@"happy" ofType:@"mp3"];
 
AudioServicesCreateSystemSoundID((CFURLRef)[NSURL fileURLWithPath:path], &soundID);
 
```

这两行语句就把对应的音频文件绑定到了相关的SystemSoundID上面了。
在需要调用的方法里面加入下面的语句，就可以把声音调用出来了

```plain
AudioServicesPlaySystemSound (soundEgg); //非常简单好用
                        //soundID

```

```plain
//自定义cell的创建    
if (cell == nil)
        {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryOrderCell" owner:nil options:nil];//（此为刚才新建的xib的名字）
        
        for (id oneObject in nib)
            {
            if ([oneObject isKindOfClass:[HistoryOrderCell class]])
                {
                     cell = (HistoryOrderCell *)oneObject;
                    break;
                }
            }    
        }

```

//判断是否有alertViewButtonClicked:方法


```plain
    if ([delegate respondsToSelector:@selector(alertViewButtonClicked:)])
    {
        [delegate alertViewButtonClicked:self];
    }

```

//UIImageView开启交互,默认会调用Touch的四个方法.


```plain
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];        //创建一个UITouch
    CGPoint point = [touch locationInView:self];//获得当前imageView的坐标
    downX = point.x;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    if (downX>point.x+50)
        {
        if ([_delegate respondsToSelector:@selector(dishImgViewLeft)])
            {
            [_delegate dishImgViewLeft];
            }
        }
    if (downX<point.x-50)
        {
        if ([_delegate respondsToSelector:@selector(dishImgViewRight)])
            {
            [_delegate dishImgViewRight];
            }
        }
}

```


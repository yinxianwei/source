title: iOS – Xcode升级到5.1和iOS升级到iOS7.1问题
date: 2014-03-19 13:25:00
categories:
- iOS应用开发之Xcode配置
tags:
- Xcode5.1
---
****
iOS7.1时XCode为5.0.2 无法真机调试.升级XCode到5.1.


Xcode升级到5.1了，Apple默认让所有App都通过64位编译器编译。通过下面的3步可以关闭： 
1.选中Targets—>Build Settings—>Architectures。
2.双击Architectures，选择other，删除$(ARCH_STANDARD)，然后增加armv7和armv7s。
3.clean一下再编译。


Xcode 5.1， iOS SDK 7.1 后，TableView Cell clipsToBounds 属性默认都是NO，导致以前用的把cell的height设为0来隐藏某个cell的方法没用了,cell内容会叠起来。
 解决方法就是手动把clipsToBounds改YES，或者Storyboard里Clip Subview打勾。
本文转自：[http://www.kaifazhe.com/mobile/388362.html](http://www.kaifazhe.com/mobile/388362.html)

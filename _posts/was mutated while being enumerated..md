title: was mutated while being enumerated.
date: 2015-01-01 13:04:00
categories: 
tags:
- NSMutableArray
---
错误示例：

	NSMutableArray * arrayTemp = create;

	for (NSDictionary * dic in arrayTemp) {
	if (condition){
	    [arrayTemp removeObject:dic];
	  }
	}

解决办法：

用block进行操作，如下：	

	NSMutableArray *tempArray = [[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5", nil];
	[tempArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
	if ([obj isEqualToString:@"2"]) { 
    *stop = YES;
	if (*stop == YES) {
        [tempArray removeObject:obj]; 
      }
    }
	if (*stop) {
	    NSLog(@"array ：%@",tempArray);
	  }
	}];
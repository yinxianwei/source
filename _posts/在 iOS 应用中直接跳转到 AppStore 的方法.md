title: 在 iOS 应用中直接跳转到 AppStore 的方法
date: 2013-10-29 21:56:00
categories:
tags:
---
找到应用程序的描述链接，比如：[http://itunes.apple.com/gb/app/yi-dong-cai-bian/id391945719?mt=8](http://itunes.apple.com/gb/app/yi-dong-cai-bian/id391945719?mt=8)
然后将 http:// 替换为 itms:// 或者 itms-apps://：
 itms://[itunes.apple.com/gb/app/yi-dong-cai-bian/id391945719?mt=8](http://itunes.apple.com/gb/app/yi-dong-cai-bian/id391945719?mt=8) itms-apps://[itunes.apple.com/gb/app/yi-dong-cai-bian/id391945719?mt=8](http://itunes.apple.com/gb/app/yi-dong-cai-bian/id391945719?mt=8)  然后打开这个链接地址：
```objc
[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.apple.com/gb/app/yi-dong-cai-bian/id391945719?mt=8"]];


[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps ://itunes.apple.com/gb/app/yi-dong-cai-bian/id391945719?mt=8"]];
```

[http://www.cnblogs.com/Proteas/archive/2012/04/14/2447230.html](http://www.cnblogs.com/Proteas/archive/2012/04/14/2447230.html)

title: Xcode的Architectures和Valid Architectures的区别
date: 2014-02-17 15:51:00
categories:
- iOS应用开发之Xcode配置
tags:
- iphone开发
- xcode
- ios开发
- ios
---
作者：shede333 
[主页：http://my.oschina.net/shede333](http://my.oschina.net/shede333) 
版权声明：原创文章，版权声明：自由转载-非商用-非衍生-保持署名 | [Creative
 Commons BY-NC-ND 3.0](http://creativecommons.org/licenses/by-nc-nd/3.0/deed.zh)
##Xcode的Architectures和Valid Architectures的区别
###Architectures
这代表，在这个项目里你想要Xcode编译的目标设备列表。
###Valid Architectures
还不是太明确这个设置的意图，但是一般来说是不需要更改的。
在Xcode5.0里的Valid Architectures 设置里，有2个选项：
1. 默认为`standard
 architectures (including 64-bit)(armv7,armv7s,arm64)`，这样设置，你的Deployment target最低只能设置为 6.0,(在Xcode5.0.1 之后,最低能够兼容IOS 5.1.1);
2. `standard
 architectures (armv7,armv7s)`，这样设置，你的Deployment target最低能设置为 4.3；

##原因解释如下：
使用standard architectures (including 64-bit)(armv7,armv7s,arm64)参数， 
则打的包里面有32位、64位两份代码， 
在iPhone5s（`iPhone5s的cpu是64位的`）下，会首选运行64位代码包， 
其余的iPhone（`其余iPhone都是32位的,iPhone5c也是32位`）， 
只能运行32位包， 
但是包含两种架构的代码包，只有运行在ios6，ios7系统上。 
这也就是说，这种打包方式，对手机几乎没啥要求，但是对系统有要求，即ios6以上。
而使用standard architectures (armv7,armv7s)参数， 
则打的包里只有32位代码， 
iPhone5s的cpu是64位，但是可以兼容32位代码，即可以运行32位代码。但是这会降低iPhone5s的性能，原因下面的参考有解释。 
其余的iPhone对32位代码包更没问题， 
而32位代码包，对系统也几乎也没什么限制。
所以总结如下： 
要发挥iPhone5s的64位性能，就要包含64位包，那么系统最低要求为ios6。 
如果要兼容ios5以及更低的系统，只能打32位的包，系统都能通用，但是会丧失iPhone5s的性能。
##参考1：
[所有IOS设备详情列表
 List of iOS devices - Wikipedia, the free encyclopedia](http://en.wikipedia.org/wiki/List_of_iOS_devices)
> armv6：iPhone 2G/3G，iPod 1G/2G 
> armv7：iPhone 3GS/4/4s，iPod 3G/4G，iPad 1G/2G/3G 
> armv7s：iPhone5
##参考2：
[iOS
 7: 如何为iPhone 5S编译64位应用。](http://blog.csdn.net/keyboardota/article/details/11993883)
> Xcode 5编译的iOS 7程序包含了32位和64位两套二进制代码，在32位的iOS系统上会调用32位的二进制代码，在64位系统上会调用64位的二进制代码，以此来解决向后兼容的问题。
> 同时，考虑到很多32位的程序可能在没有重新编译的情况下部署到64位系统上，64位的iOS系统中带有两套FrameWork，一套是32位的，一套是64位的。 
> 当64位的iOS系统运行原来的32位程序时，系统会调用32位的FrameWork作为底层支撑，当系统运行64位程序时，系统会调用64位的FrameWork作为底层支撑。
> 也就是说，当一个iPhone 5S上同时运行32位程序和64位程序时，系统同时将32位和64位两套FrameWork载入了内存中，所以消耗的内存也比较多。
> 如果一台64位的iOS设备上运行的所有程序都是为64位系统编译过的，iOS系统将只载入64位的FrameWork，这将节省好多内存。所以，如果大家都可以快速将程序传换成64位的，iOS将跑得更快。真的是“大家好才是真的好”。
##参考3：
1. [What's
 the difference between “Architectures” and “Valid Architectures” in Xcode Build Settings?](http://stackoverflow.com/questions/12701188/whats-the-difference-between-architectures-and-valid-architectures-in-xcode)
2. [Xcode
 5 and iOS 7: Architecture and Valid architectures](http://stackoverflow.com/questions/18913906/xcode-5-and-ios-7-architecture-and-valid-architectures)
3. [Xcode
 Build Setting Reference (苹果官方文档)](https://developer.apple.com/library/ios/documentation/DeveloperTools/Reference/XcodeBuildSettingRef/1-Build_Setting_Reference/build_setting_ref.html#//apple_ref/doc/uid/TP40003931-CH3-SW51)
4. [64-Bit
 transition Guide for Cocoa Touch (苹果官方文档)](https://developer.apple.com/library/iOS/documentation/General/Conceptual/CocoaTouch64BitGuide/Introduction/Introduction.html)


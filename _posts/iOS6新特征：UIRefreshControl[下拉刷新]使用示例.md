title: iOS6新特征：UIRefreshControl[下拉刷新]使用示例
date: 2014-05-04 20:44:00
categories:
- iOS应用开发之UI
tags:
- UITableViewControlle
---
原文出处:[http://www.devdiv.com/iOS_iPhone-iOS6%E6%96%B0%E7%89%B9%E5%BE%81%EF%BC%9AUIRefreshControl_%E4%B8%8B%E6%8B%89%E5%88%B7%E6%96%B0_%E4%BD%BF%E7%94%A8%E7%A4%BA%E4%BE%8B-thread-127741-1-1.html](http://www.devdiv.com/iOS_iPhone-iOS6%E6%96%B0%E7%89%B9%E5%BE%81%EF%BC%9AUIRefreshControl_%E4%B8%8B%E6%8B%89%E5%88%B7%E6%96%B0_%E4%BD%BF%E7%94%A8%E7%A4%BA%E4%BE%8B-thread-127741-1-1.html)
注：这里是iOS6新特征汇总贴链接 [iOS6新特征：参考资料和示例汇总](http://www.devdiv.com/iOS_iPhone-ios_-thread-127965-1-1.html)

小弟不才，今天研究了一下iOS6中的一个新控件UIRefreshControl，下面与大家分享一下。

不会模仿的公司不是好公司不会剽窃的公司不是优秀公司
不会调戏代码的不是骨灰级码工
你同意吗？
苹果估计想取代第三方的pull to refresh】

下面是效果图，这里是代码示例 ![](http://www.devdiv.com/static/image/filetype/zip.gif)[DevDiv_UIRefreshControl.zip](http://www.devdiv.com/forum.php?mod=misc&action=attachpay&aid=32936&tid=127741) (144.42
 KB, 下载次数: 1042, 售价: 1 资源分) 

![QQ20120615-2.png](http://www.devdiv.com/forum.php?mod=attachment&aid=MzI5Mzd8Mzk1NmY0NjZ8MTM5OTIwNzU1NnwwfDEyNzc0MQ%3D%3D&noupdate=yes "QQ20120615-2.png")




下面大致介绍一下UIRefreshControl的使用

1、使用范围
如果你装了xcode_4.5_developer_preview，那么在UITableViewController.h文件中你会看到，UITableViewController里面有如下声明，说明UITableViewController已经内置了UIRefreshControl控件

`1``@property
 (nonatomic,retain) UIRefreshControl *refreshControl NS_AVAILABLE_IOS(6_0);`

【注】：UIRefreshControl目前只能用于UITableViewController，如果用在其他ViewController中，运行时会得到如下错误提示：（即UIRefreshControl只能被UITableViewController管理）

`1``2012-06-15
 14:34:34.908 DevDivUIRefreshControl[722:10103] *** Terminating app due to uncaught exception ``'NSInternalInconsistencyException'``,
 reason: ``'UIRefreshControl
 may only be managed by a UITableViewController'``2``***
 First ``throw` `call
 stack:``3``(0x186fd72
 0x1066e51 0x186fb4b 0x55a559 0x57238 0x5d482 0x55ad2 0x2ebb 0xeb2a3 0xeb30e 0x10b7e9 0x10b624 0x109aef 0x10999c 0x107adc 0x1082c6 0xecf24 0xed1e0 0xee084 0x5645c 0x5cf31 0x55ad2 0x4131d 0x414f6 0x4168c 0x49871 0x10a90 0x1196a 0x222be 0x22f9f 0x153fd 0x17ccf39
 0x17ccc10 0x17e5da5 0x17e5b12 0x1816b46 0x1815ed4 0x1815dab 0x1128f 0x12e71 0x29fd 0x2925)``4``libc++abi.dylib:
 terminate called throwing an exception``5``(lldb)`

2、如何使用
    a)初始化
如何在UITableViewController 中使用UIRefreshControl呢，在上面给出的代码附件中，你可以很详细的知道，这里介绍一下关键的部分：

`1``   ``self.refreshControl
 = [[UIRefreshControl alloc]init];``2``    ``// 
   self.refreshControl.tintColor = [UIColor blueColor];``3``    ``self.refreshControl.attributedTitle
 = [[NSAttributedString alloc]initWithString:@``"下拉刷新"``];``4``    ``[self.refreshControl
 addTarget:self action:@selector(RefreshViewControlEventValueChanged) forControlEvents:UIControlEventValueChanged];`

如上面看到的代码，虽然UITableViewController已经声明了UIRefreshControl，但是貌似还没有初始化，所以需要我们自己初始化。很神奇，初始化的时候并不需要给它指定frame，UITableViewController会为我们进行管理。遗憾的时目前只看到下拉刷新功能，上拉刷新还没有，估计在最终版里面苹果会考虑加入上拉刷新功能。
我们还可以给UIRefreshControl设置tintColor和attributedTitle。

    b)下拉刷新事件监听
当用户进行下拉刷新操作时，UIRefreshControl 会触发一个UIControlEventValueChanged事件，通过监听这个事件，我们就可以进行类似数据请求的操作了。如下代码：


`1``[self.refreshControl
 addTarget:self action:@selector(RefreshViewControlEventValueChanged)`

    c)进行数据请求
在示例中，为了演示数据请求，我简单的做了一个延时处理，2秒钟后，调用handleData

`1``[self
 performSelector:@selector(handleData) withObject:nil afterDelay:2];`

在handleData里面，就表示已经请求到了数据，在此进行UI更新即可。也需要注意的是，我们调用UIRefreshControl 的endRefreshing方法，表示刷新结束，让UIRefreshControl更新显示。

`1``-
 (``void``)
 handleData``2``{``3``    ``NSLog(@``"refreshed"``);``4``    ``[self.refreshControl
 endRefreshing];``5``    ``self.refreshControl.attributedTitle
 = [[NSAttributedString alloc]initWithString:@``"下拉刷新"``];``6` `7``    ``self.count++;``8``    ``[self.tableView
 reloadData];``9``}`


3、官方头文件
下面是sdk中UIRefreshControl的声明，想必看了下面的代码，你已经知道如何使用了。
[view
 source](http://www.devdiv.com/#viewSource "view source")[print](http://www.devdiv.com/#printSource "print")[?](http://www.devdiv.com/#about "?")`01``//``02``//  UIRefreshControl.h``03``//  UIKit``04``//``05``//  Copyright
 2012 Apple Inc. All rights reserved.``06``//``07` `08``#import
 <Foundation/Foundation.h>``09``#import
 <UIKit/UIControl.h>``10``#import
 <UIKit/UIKitDefines.h>``11` `12``NS_CLASS_AVAILABLE_IOS(6_0)
 @interface UIRefreshControl : UIControl``13` `14``/*
 The designated initializer``15`` ``*
 This initializes a UIRefreshControl with a default height and width.``16`` ``*
 Once assigned to a UITableViewController, the frame of the control is managed automatically.``17`` ``*
 When a user has pulled-to-refresh, the UIRefreshControl fires its UIControlEventValueChanged event.``18`` ``*/``19``-
 (id)init;``20` `21``@property
 (nonatomic, readonly, getter=isRefreshing) ``BOOL` `refreshing;``22` `23``@property
 (nonatomic, retain) UIColor *tintColor UI_APPEARANCE_SELECTOR;``24``@property
 (nonatomic, retain) NSAttributedString *attributedTitle UI_APPEARANCE_SELECTOR;``25` `26``//
 May be used to indicate to the refreshControl that an external event has initiated the refresh action``27``-
 (``void``)beginRefreshing
 NS_AVAILABLE_IOS(6_0);``28``//
 Must be explicitly called when the refreshing has completed``29``-
 (``void``)endRefreshing
 NS_AVAILABLE_IOS(6_0);``30` `31``@end

`
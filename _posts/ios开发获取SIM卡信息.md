title: ios开发获取SIM卡信息
date: 2013-11-30 12:58:00
categories:
- iOS应用开发之MDM资料
tags:
- ios
- api
- ios开发
---

```objc
 1.加入一个Framework(CoreTelephony.framework).

 

2.引入头文件

#import

#import

 3.初始化

 

#import

#import

#import

 

@interface RootViewController : UITableViewController

{

    //声明变量

    CTTelephonyNetworkInfo *networkInfo;

}

@end

 

 

@implementation RootViewController


- (void)viewDidLoad

{

    [super viewDidLoad];

    self.navigationItem.prompt = @"CTTelephonyNetworkInfo";

    self.navigationItem.title = @"CTCarrier";

    //初始化

    networkInfo = [[CTTelephonyNetworkInfo alloc] init];

    //当sim卡更换时弹出此窗口

    networkInfo.subscriberCellularProviderDidUpdateNotifier = ^(CTCarrier *carrier){

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Sim card changed" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];

        [alert show];

    };

}



 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{

    //获取sim卡信息

    CTCarrier *carrier = networkInfo.subscriberCellularProvider;

    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) {

        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

    }

   
    switch (indexPath.row) {

        case 0://供应商名称（中国联通 中国移动）

            cell.textLabel.text = @"carrierName";

            cell.detailTextLabel.text = carrier.carrierName;

            break;

        case 1://所在国家编号

            cell.textLabel.text = @"mobileCountryCode";

            cell.detailTextLabel.text = carrier.mobileCountryCode;

            break;

        case 2://供应商网络编号

            cell.textLabel.text = @"mobileNetworkCode";

            cell.detailTextLabel.text = carrier.mobileNetworkCode;

            break;

        case 3:

            cell.textLabel.text = @"isoCountryCode";

            cell.detailTextLabel.text = carrier.isoCountryCode;

            break;

        case 4://是否允许voip

            cell.textLabel.text = @"allowsVOIP";

            cell.detailTextLabel.text = carrier.allowsVOIP?@"YES":@"NO";

            break;

           
        default:

            break;

    }

   
    return cell;

}

```

```objc

```

转自:http://blog.sina.com.cn/s/blog_74461f3201018b5x.html


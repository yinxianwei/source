title: CoreData实体的DeleteRule属性
date: 2014-12-03 10:46:50
description: 删除一个实体怎么同时删除与其关联的实体
categories: iOS应用开发之数据库
tags: CoreData
---
在使用CoreData的时候碰到这个属性，Delete Rule。

	把对应的Delete Rule属性设置为：Cascade 就可以了。

>- 如果关系的删除规则设定为Nullify（作废）。当A对象的关系指向的B对象被删除后，A对象的关系将被设为nil。对于To Many关系类型，B对象只会从A对象的关系的容器中被移除。
- 如果关系的删除规则为Cascade（级联），当B对象的关系指向的C对象被删除后，B对象也会被删除。B对象关联（以Cascade删除规则）的二级对象A也会被删除。以此类推。
- 如果关系的删除规则为Deny（拒绝），如果删除A对象时，A对象的关系指向的B对象仍存在，则删除操作会被拒绝。
- 如果关系的删除规则为NO Action，当A对象的关系指向的B对象被删除后，A对象保持不变，这意味着A对象的关系会指向一个不存在的对象。如果没有充分的理由，最好不要使用。

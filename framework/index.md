title: 常用类库
date: 2014-12-12 13:15:35
---
[UzysAssetsPickerController](#1)|[XHImageViewer](#2) |[IQKeyBoardManager](#4)|[MJNIndexView](#3) 
--|--|--|--
[IQKeyBoardManager](#4)|[RTImageAssets](#5)||
		
#<span id="1">UzysAssetsPickerController</span>
>相册相机添加图片和添加视频，支持最大添加文件个数。		
链接地址：[https://github.com/uzysjung/UzysAssetsPickerController](https://github.com/uzysjung/UzysAssetsPickerController)		
示例：		
<a href="http://https://github.com/uzysjung/UzysAssetsPickerController" target="_blank"><img src="http://code4app.qiniudn.com/photo/53f4964d933bf0187a8b498c_15.gif" alt="图片失效" title="" width="200" />
***		

#<span id="2">XHImageViewer</span>
>实现图片浏览功能，点击图片，开启图片全屏浏览模式。支持下载和显示网络图片。
链接地址：[https://github.com/JackTeam/XHImageViewer](https://github.com/JackTeam/XHImageViewer)		
示例：		
<a href="https://github.com/JackTeam/XHImageViewer" target="_blank"><img src="http://code4app.qiniudn.com/photo/53023b81cb7e84f74e8b52c8_1.gif" alt="图片失效" title="" width="200" />
***		

#<span id="3">MJNIndexView</span>

>实现多种特殊动画效果的列表目录（UITableView index）。可以自定义各种参数，多达20多种，来定义index的外观和动画效果。仅支持iOS 6.0以上		
链接地址：[https://github.com/matthewfx/MJNIndexView](https://github.com/matthewfx/MJNIndexView)				
示例：		
<a href="https://github.com/matthewfx/MJNIndexView" target="_blank"><img src="http://code4app.qiniudn.com/photo/522008c56803fafe03000002_1.gif" alt="图片失效" title="" width="200" />
***		

#<span id="4">IQKeyBoardManager</span>

>只需一句代码就能轻松解决键盘遮挡住输入框的问题。		
链接地址：[https://github.com/hackiftekhar/IQKeyboardManager](https://github.com/hackiftekhar/IQKeyboardManager)				
示例：		
<a href="https://github.com/hackiftekhar/IQKeyboardManager" target="_blank"><img src="http://code4app.qiniudn.com/photo/524260bf6803fa7a41000001_1.gif" alt="图片失效" title="" width="200" />
***		

#<span id="5">RTImageAssets</span>

>本项目是一个 Xcode 插件，用来生成 @3x 的图片资源对应的 @2x 和 @1x 版本，只要拖拽高清图到 @3x 的位置上，然后按 Ctrl+Shift+A 即可自动生成两张低清的补全空位。当然你也可以从 @2x 的图生成 @3x 版本，如果你对图片质量要求不高的话。
链接地址：[https://github.com/rickytan/RTImageAssets](https://github.com/rickytan/RTImageAssets)					
示例：		
<a href="https://github.com/rickytan/RTImageAssets" target="_blank"><img src="http://7teblm.com1.z0.glb.clouddn.com/usage.gif" alt="图片失效" title="" width="600" />
***

<!--
#<span id=""></span>
>		
链接地址：[]()				
示例：		
<a href="http://code4app.qiniudn.com/photo/53023b81cb7e84f74e8b52c8_1.gif" target="_blank"><img src="" alt="图片失效" title="" width="200" />
***		

-->

声明：以上示例图片均来自Code4app，源码收集自github，版权均属于开发者。如本人不愿意，请联系我。
